package Pages.ScheduleTreatment;

import BaseTests.BaseTests;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class ScheduleTreatment extends BaseTests{

    public String TestUser = "Doctor";

    //---LOCATORS
    //---Apheresis Center
    //---Drop-Off
    @FindBy(xpath="//div[@id='tabPanel']/div[1]/div[5]/div[2]/div[1]//a")
    public SelenideElement AddDropOffContactButton;

    @FindBy(xpath="//span[contains (text(), 'Select a new Leukapheresis Drop-Off Contact Option')]")
    public SelenideElement NewDropOffContactWindow;

    @FindBy(css="[class='slds-small-size--1-of-2 slds-medium-size--1-of-2 slds-large-size--1-of-2 cSL_GridColumn']:nth-of-type(1) .slds-form-element__control #lookatme")
    public SelenideElement NewDropOffContactSearchField;

    @FindBy(css="[class='slds-small-size--1-of-2 slds-medium-size--1-of-2 slds-large-size--1-of-2 cSL_GridColumn']:nth-of-type(1) .slds-p-left--small")
    public SelenideElement DropOffContactSaveButton;

    @FindBy(xpath = "//div[@class='slds-lookup__menu']/ul[@class='slds-lookup__list']/li[2]")
    public SelenideElement DropOffFirstValue;

    //---Pick-Up
    @FindBy(xpath="//div[@id='tabPanel']/div[1]/div[5]/div[2]/div[2]//a")
    public SelenideElement AddPickUpContactButton;

    @FindBy(xpath="//span[contains (text(), 'Select a new Leukapheresis Pick-Up Contact Option')]")
    public SelenideElement NewPickUpContactWindow;

    @FindBy(css="[class='slds-small-size--1-of-2 slds-medium-size--1-of-2 slds-large-size--1-of-2 cSL_GridColumn']:nth-of-type(2) .slds-form-element__control #lookatme")
    public SelenideElement NewPickUpContactSearchField;

    @FindBy(css = "[class='slds-small-size--1-of-2 slds-medium-size--1-of-2 slds-large-size--1-of-2 cSL_GridColumn']:nth-of-type(2) .slds-lookup__list .slds-media:nth-of-type(2)")
    public SelenideElement PickUpFirstValue;

    @FindBy(css="[class='slds-small-size--1-of-2 slds-medium-size--1-of-2 slds-large-size--1-of-2 cSL_GridColumn']:nth-of-type(2) .slds-p-left--small")
    public SelenideElement PickUpContactSaveButton;

    //---Treatment Site
    //---Drop-Off
    @FindBy(css=".cCmp_SL_InfusionDropoffContact_View .sl-addContactButton")
    public SelenideElement AddSiteDropOffContactButton;

    @FindBy(xpath="//span[contains (text(), 'Select a new Infusion Drop-Off Contact Option')]")
    public SelenideElement NewSiteDropOffContactWindow;

    @FindBy(css="[class='slds-small-size--1-of-2 slds-medium-size--1-of-2 slds-large-size--1-of-2 cSL_GridColumn']:nth-of-type(1) .slds-lookup__search-input")
    public SelenideElement NewSiteDropOffContactSearchField;

    @FindBy(css="[class='slds-small-size--1-of-2 slds-medium-size--1-of-2 slds-large-size--1-of-2 cSL_GridColumn']:nth-of-type(1) .slds-p-left--small")
    public SelenideElement SiteDropOffContactSaveButton;

    @FindBy(css = ".slds-lookup__list .slds-media:nth-of-type(2)")
    public SelenideElement SiteDropOffFirstValue;

    //---Pick-Up
    @FindBy(css=".cCmp_SL_InfusionPickupContact_View .sl-addContactButton")
    public SelenideElement AddSitePickUpContactButton;

    @FindBy(xpath="//span[contains (text(), 'Select a new Treatment Site Pick-Up Contact Option')]")
    public SelenideElement NewSitePickUpContactWindow;

    @FindBy(css=".slds-input-has-icon--right #lookatme")
    public SelenideElement NewSitePickUpContactSearchField;

    @FindBy(css = "[class='slds-small-size--1-of-2 slds-medium-size--1-of-2 slds-large-size--1-of-2 cSL_GridColumn']:nth-of-type(2) .slds-lookup__list .slds-media:nth-of-type(2)")
    public SelenideElement SitePickUpFirstValue;

    @FindBy(css="[class='slds-small-size--1-of-2 slds-medium-size--1-of-2 slds-large-size--1-of-2 cSL_GridColumn']:nth-of-type(2) .slds-p-left--small")
    public SelenideElement SitePickUpContactSaveButton;

    //---Buttons
    @FindBy(xpath = "//*[contains(text(), 'Save & Next')]")
    public SelenideElement SaveAndNextButton;

    @FindBy(xpath = "//button[contains(text(), 'Open Scheduler')]")
    public SelenideElement OpenSchedulerButton;

    //---METHODS

    public ScheduleTreatment selectDropOffContact() throws InterruptedException {
        Thread.sleep(2000);
        $(AddDropOffContactButton).waitUntil(enabled,30000).click();
        $(NewDropOffContactWindow).waitUntil(visible,15000);
        mm.selectFromSearchField(NewDropOffContactSearchField, DropOffFirstValue, "Super Contact");
        $(DropOffContactSaveButton).waitUntil(enabled,15000).click();
        Thread.sleep(1500);
        return this;
    }

    public ScheduleTreatment selectPickUpContact() throws InterruptedException{
        $(AddPickUpContactButton).waitUntil(enabled,30000).click();
        $(NewPickUpContactWindow).waitUntil(visible,15000);
        mm.selectFromSearchField(NewPickUpContactSearchField, PickUpFirstValue, "Super Contact");
        $(PickUpContactSaveButton).waitUntil(enabled,15000).click();
        Thread.sleep(2000);
        return this;
    }

    public ScheduleTreatment selectSiteDropOffContact() throws InterruptedException{
        $(AddSiteDropOffContactButton).waitUntil(enabled,30000).click();
        $(NewSiteDropOffContactWindow).waitUntil(visible,15000);
        mm.selectFromSearchField(NewSiteDropOffContactSearchField, SiteDropOffFirstValue, "Super Contact");
        $(SiteDropOffContactSaveButton).waitUntil(enabled,15000).click();
        Thread.sleep(2000);
        return this;
    }

    public ScheduleTreatment selectSitePickUpContact() throws InterruptedException{
        $(AddSitePickUpContactButton).waitUntil(enabled, 30000).click();
        $(NewSitePickUpContactWindow).waitUntil(visible,15000);
        mm.selectFromSearchField(NewSitePickUpContactSearchField, SitePickUpFirstValue, "Super Contact");
        $(SitePickUpContactSaveButton).waitUntil(enabled,15000).click();
        Thread.sleep(2000);
        return this;
    }

    public ScheduleTreatment addContactsOnSchedulePage() throws InterruptedException {
        System.out.println("[INFO]: Add Contacts on Schedule Treatment Page.");
        selectDropOffContact();
        selectPickUpContact();
        $(SaveAndNextButton).waitUntil(enabled, 15000).click();
        selectSiteDropOffContact();
        selectSitePickUpContact();
        $(SaveAndNextButton).waitUntil(enabled, 15000).click();
        return this;
    }
}
