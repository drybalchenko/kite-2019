package Pages.ScheduleTreatment;

import BaseTests.BaseTests;
import org.openqa.selenium.support.FindBy;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

import java.util.ArrayList;
import java.util.List;

public class SchedulingHelper extends BaseTests{

    public String SchedulingDate;
    public String DeliveryDate;

    //---LOCATORS

    @FindBy(css="[type='search']")
    public SelenideElement TimeField;

    @FindBy(xpath="//div[@class='week']//div[@class='day future available'][1]")  //div[@class='week']   //div[@class='week']//div[@class='day future'][1]
    public SelenideElement FirstAvailableDay;

    @FindBy(xpath="//div[@class='calendar']")
    public SelenideElement Calendar;

    @FindBy(xpath="//span[@class='date-indicated']")
    public SelenideElement DateIndicator;

    @FindBy(xpath="//div[@id='scheduler']//div[@class='site-info']/div[2]/div[3]/span")
    public SelenideElement DeliveryDateIndicator;

    @FindBy(xpath="//ul[@role='presentation']/li[2]/span")
    public SelenideElement TimeSecondValue;

    @FindBy(css=".save-schedule")
    public SelenideElement SaveButton;

    public SchedulingHelper selectDateTime () throws InterruptedException {
        System.out.println("[INFO]: Select Date and Time.");
        List<String> browserTabs = new ArrayList<String>(WebDriverRunner.getWebDriver().getWindowHandles());
        WebDriverRunner.getWebDriver().switchTo().window(browserTabs.get(1));
        $(Calendar).waitUntil(visible, 120000);
        FirstAvailableDay.click();
        SchedulingDate = DateIndicator.getText();
        TimeField.click();
        Thread.sleep(2000);
        TimeSecondValue.click();
        DeliveryDate = DeliveryDateIndicator.getText();
        SaveButton.click();
        Thread.sleep(200);
        WebDriverRunner.getWebDriver().switchTo().window(browserTabs.get(0));
        return this;
    }
}
