package Pages.ConfirmOrder;

import BaseTests.BaseTests;
import org.openqa.selenium.support.FindBy;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class ConfirmOrder extends BaseTests{

    public String TotalSchedulingDate;
    public String TotalDeliveryDate;

    //---LOCATORS
    //---Apheresis Information
    @FindBy(xpath = "//div[4]/div[1]/div[2]/div[2]/div[@class='slds-grid']")
    public SelenideElement ApheresisDateField;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_CheckoutReview']/div[4]/div[1]/div[3]/div[2]/div[1]")
    public SelenideElement ApheresisAddressString1;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_CheckoutReview']/div[4]/div[1]/div[3]/div[2]/div[2]")
    public SelenideElement ApheresisAddressString2;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_CheckoutReview']/div[4]/div[1]/div[3]/div[2]/div[3]")
    public SelenideElement ApheresisAddressString3;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_CheckoutReview']/div[4]/div[1]/div[5]/div[1]/span")
    public SelenideElement ApheresisDropOffContact;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_CheckoutReview']/div[4]/div[1]/div[5]/div[2]/span")
    public SelenideElement ApheresisPickUpContact;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_CheckoutReview']/div[5]/div[1]/div[1]/div[2]//span")
    public SelenideElement ApheresisInstitution;

    //---Apheresis Information
    @FindBy(xpath = "//div[4]/div[2]/div[2]/div[2]/div[@class='slds-grid']")
    public SelenideElement TreatmentDateField;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_CheckoutReview']/div[4]/div[2]/div[3]/div[2]/div[1]")
    public SelenideElement TreatmentAddressString1;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_CheckoutReview']/div[4]/div[2]/div[3]/div[2]/div[2]")
    public SelenideElement TreatmentAddressString2;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_CheckoutReview']/div[4]/div[2]/div[3]/div[2]/div[3]")
    public SelenideElement TreatmentAddressString3;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_CheckoutReview']/div[4]/div[2]/div[5]/div[1]/span")
    public SelenideElement TreatmentDropOffContact;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_CheckoutReview']/div[4]/div[2]/div[5]/div[2]/span")
    public SelenideElement TreatmentPickUpContact;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_CheckoutReview']/div[5]/div[2]/div[1]/div[2]//span")
    public SelenideElement TreatingPhysician1;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_CheckoutReview']/div[5]/div[2]/div[2]/div[2]//span")
    public SelenideElement TreatingPhysician2;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_CheckoutReview']/div[5]/div[2]/div[4]/div[2]//span")
    public SelenideElement TreatingPhysician3;

    //---Buttons
    @FindBy(xpath = "//span[@class='slds-checkbox--faux']")
    public SelenideElement AgreeCheckbox;

    @FindBy(xpath = "//button[contains (text(), 'Confirm Order')]")
    public SelenideElement ConfirmOrderButton;

    //---Message Window
    @FindBy(xpath = "//div[@id='tabPanel']/div/section[1]//h2[@class='slds-text-heading--medium']/span")
    public SelenideElement SuccessMessage;

    @FindBy(xpath = "//button[@class='slds-button slds-button--neutral' and contains (text(), 'OK')]")
    public SelenideElement MessageOkButton;


    @FindBy(xpath = "//button[contains (text(), 'Confirm & Continue to Confirm Order')]")
    public SelenideElement ConfirmAndContinueButton;

    //---METHODS
    public ConfirmOrder openOrderData() {
        System.out.println("[INFO]: Open Order Data.");
        $(ConfirmAndContinueButton).waitUntil(enabled, 180000).click();
        return this;
    }

    public ConfirmOrder checkOrderData(){
        System.out.println("[INFO]: Check Order Data.");
        //---Apheresis Information
        $(ApheresisDateField).waitUntil(present, 50000);

        String[] newDate = shp.SchedulingDate.split(" ");
        String[] newDayNumber = newDate[0].split("");
        System.out.print("[INFO]: Date converted " + shp.SchedulingDate + " >>> ");
        if (newDayNumber.length==1){
            TotalSchedulingDate = "0" + newDate[0] + "-" + newDate[1].substring(0, newDate[1].length() - 1) + "-" + newDate[2] + " (PDT)";
        } else {
            TotalSchedulingDate = newDate[0] + "-" + newDate[1].substring(0, newDate[1].length() - 1) + "-" + newDate[2] + " (PDT)";
        }
        System.out.println(TotalSchedulingDate);
        $(ApheresisDateField).shouldHave(visible, text(TotalSchedulingDate));
        $(ApheresisAddressString1).shouldHave(visible, text("(TST) Seattle Cancer Care Alliance"));
        $(ApheresisAddressString2).shouldHave(visible, text("825 Eastlake Ave E SCCA APHERESIS"));
        $(ApheresisAddressString3).shouldHave(visible, text("Seattle, WA 98109"));
        $(ApheresisDropOffContact).shouldHave(visible, text("Super Contact"));
        $(ApheresisPickUpContact).shouldHave(visible, text("Super Contact"));
        $(ApheresisInstitution).shouldHave(visible, text(""));

        //---Treatment Information
        String[] newDeliveryDate = shp.DeliveryDate.split(" ");
        String[] newDeliveryDayNumber = newDeliveryDate[0].split("");
        System.out.print("[INFO]: Date converted " + shp.DeliveryDate + " >>> ");
        if (newDeliveryDayNumber.length==1){
            TotalDeliveryDate = "0" + newDeliveryDate[0] + "-" + newDeliveryDate[1].substring(0, newDeliveryDate[1].length() - 1) + "-" + newDeliveryDate[2] + " (PDT)";
        } else {
            TotalDeliveryDate = newDeliveryDate[0] + "-" + newDeliveryDate[1].substring(0, newDeliveryDate[1].length() - 1) + "-" + newDeliveryDate[2] + " (PDT)";
        }
        System.out.println(TotalDeliveryDate);
        $(TreatmentDateField).shouldHave(visible, text(TotalDeliveryDate));
        $(TreatmentAddressString1).shouldHave(visible, text("(TST) Seattle Cancer Care Alliance"));
        $(TreatmentAddressString2).shouldHave(visible, text("825 Eastlake Ave E SCCA APHERESIS"));
        $(TreatmentAddressString3).shouldHave(visible, text("Seattle, WA 98109"));
        $(TreatmentDropOffContact).shouldHave(visible, text("Super Contact"));
        $(TreatmentPickUpContact).shouldHave(visible, text("Super Contact"));
        $(TreatingPhysician1).shouldHave(visible, text(""));
        //$(TreatingPhysician2).shouldHave(visible, text("")); //NPI: 123456
        //$(TreatingPhysician3).shouldHave(visible, text("")); //test@test.com
        return this;
    }

    public ConfirmOrder confirmOrder(){
        System.out.println("[INFO]: Confirm Order.");
        //---Apheresis Information
        $(AgreeCheckbox)
        .waitUntil(enabled, 30000)
        .click();

        $(ConfirmOrderButton)
        .waitUntil(enabled, 30000)
        .click();

        $(SuccessMessage)
        .waitUntil(enabled, 30000);

        $(MessageOkButton)
        .waitUntil(enabled, 30000)
        .click();

        return this;
    }
}
