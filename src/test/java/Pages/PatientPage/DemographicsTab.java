package Pages.PatientPage;

import BaseTests.BaseTests;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class DemographicsTab extends BaseTests{

    public int EditedYear;
    public String EditedMonth;
    public String EditedDay;
    public String NonEditData;

    //---Patient Demographics Block
    //---Header

    @FindBy(id = "patientNameHeader")
    public WebElement NameHeader;

    @FindBy(id = "dobHeader")
    public WebElement BirthdayHeader;

    @FindBy(id = "patientIdHeader")
    public WebElement PatientIdHeader;

    //---Information Block

    @FindBy(css = "[data-aura-rendered-by] .cCmp_SL_PatientDemographics_View:nth-child(2) [class='slds-small-size--1-of-2 slds-medium-size--1-of-2 slds-large-size--1-of-2 cSL_GridColumn']:nth-of-type(1) #patientNameView")
    public SelenideElement NameInfoBlock;

    @FindBy(css = "[data-aura-rendered-by] .cCmp_SL_PatientDemographics_View:nth-child(3) [class='slds-small-size--1-of-2 slds-medium-size--1-of-2 slds-large-size--1-of-2 cSL_GridColumn']:nth-of-type(1) #patientNameView")
    public SelenideElement BirthdayInfoBlock;

    @FindBy(css = "[data-aura-rendered-by] .cCmp_SL_PatientDemographics_View:nth-child(3) [class='slds-small-size--1-of-2 slds-medium-size--1-of-2 slds-large-size--1-of-2 cSL_GridColumn']:nth-of-type(2) #patientNameView")
    public SelenideElement GenderInfoBlock;

    @FindBy(css = "#PhysicianNameView")
    public SelenideElement PhysicianNameInfoBlock;

    @FindBy(css = "#PhysicianEmailView")
    public SelenideElement PhysicianEmailInfoBlock;

    //---Buttons

    @FindBy(xpath = "//a[contains (text(), 'Edit')]")
    public SelenideElement EditButton;

    //---EDIT Patient Information

    @FindBy(xpath = "//input[@data-interactive-lib-uid='6']")
    public SelenideElement EditFirstNameField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='7']")
    public SelenideElement EditMiddleNameField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='8']")
    public SelenideElement EditLastNameField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='9']")
    public SelenideElement EditDobYearField;

    @FindBy(xpath = "//div[2]/div[2]/div[2]//select[@class='slds-select']")
    public SelenideElement EditMonthField;

    @FindBy(xpath = "//div[2]/div[2]/div[3]//select[@class='slds-select']")
    public SelenideElement EditDayField;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_PatientInfo']/div[2]/div[2]/div[4]//div[@class='slds-form-element__control']/div/span[1]")
    public SelenideElement EditGenderMaleButton;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_PatientInfo']/div[2]/div[2]/div[4]//div[@class='slds-form-element__control']/div/span[2]")
    public SelenideElement EditGenderFemaleButton;

    //input[@placeholder='Select a Physician']

    @FindBy(xpath = "//input[@placeholder='Select a Physician']")
    public SelenideElement EditThreatingPhisicianField;

    @FindBy(css = ".slds-pill__remove .slds-button__icon")
    public SelenideElement RemoveThreatingPhisicianSelection;

    @FindBy(xpath = "//li[@data-selindx='0']")
    public SelenideElement ThreatingFirstRecord;

    @FindBy(xpath = "//div[2]/div[4]/div//div[@class='slds-form-element__control']/div/span[1]/label[@class='slds-radio--button__label']/span[@class='slds-radio--faux']")
    public SelenideElement AddOwnHpYesButton;

    @FindBy(xpath = "//div[2]/div[4]/div//div[@class='slds-form-element__control']/div/span[2]/label[@class='slds-radio--button__label']/span[@class='slds-radio--faux']")
    public SelenideElement AddOwnHpNoButton;

    //---Buttons
    @FindBy(xpath = "//button[contains (text(), 'Submit')]")
    public SelenideElement SubmitButton;

    @FindBy(xpath = "//button[contains (text(), 'Cancel')]")
    public SelenideElement CancelButton;

    //---Cancel pop-up
    @FindBy(xpath = "//h5[@class='slds-text-heading--medium']")
    public SelenideElement CancelMessage;

    @FindBy(xpath = "//a[@class='slds-button slds-button--brand' and contains (text(), 'No, continue editing')]")
    public SelenideElement CancelNoButton;

    @FindBy(xpath = "//a[@class='slds-button slds-button--brand' and contains (text(), 'Yes, I would like to cancel')]")
    public SelenideElement CancelYesButton;

    //---METHODS

    public DemographicsTab checkDataOnPatientPage(){
        System.out.println("[INFO]: Check Data on Patient Page.");
        NonEditData = pf.LastName + pf.RN2;
        $(NameHeader).shouldHave(visible, text(pf.LastName + pf.RN2 + ", " + pf.FirstName + "  " + pf.MiddleName));
        $(BirthdayHeader).shouldHave(visible, text(pf.DAY +"-"+ pf.MONTH +"-"+ Integer.toString(pf.YEAR)));
        $(NameInfoBlock).shouldHave(visible, text(pf.LastName + pf.RN2 + ", " + pf.FirstName + " " + pf.MiddleName));
        $(BirthdayInfoBlock).shouldHave(visible, text(pf.DAY +"-"+ pf.MONTH +"-"+ Integer.toString(pf.YEAR)));
        $(GenderInfoBlock).shouldHave(visible, text(pf.Gender));
        $(PhysicianNameInfoBlock).shouldHave(visible, text("William, Basem"));
        //$(PhysicianEmailInfoBlock).shouldHave(visible, text(pf.PhysicianEmail));
        return this;
    }

    public DemographicsTab checkNonEditedDataOnPatientPage(){
        System.out.println("[INFO]: Check Data on Patient Page.");
        $(NameHeader).shouldHave(visible, text(NonEditData + ", " + pf.FirstName + "  " + pf.MiddleName));
        $(BirthdayHeader).shouldHave(visible, text(pf.DAY +"-"+ pf.MONTH +"-"+ Integer.toString(pf.YEAR)));
        $(NameInfoBlock).shouldHave(visible, text(NonEditData + ", " + pf.FirstName + " " + pf.MiddleName));
        $(BirthdayInfoBlock).shouldHave(visible, text(pf.DAY +"-"+ pf.MONTH +"-"+ Integer.toString(pf.YEAR)));
        $(GenderInfoBlock).shouldHave(visible, text(pf.Gender));
        $(PhysicianNameInfoBlock).shouldHave(visible, text("William, Basem"));
        //$(PhysicianEmailInfoBlock).shouldHave(visible, text(pf.PhysicianEmail));
        return this;
    }

    public DemographicsTab editDataOnPatientPage() throws InterruptedException{
        System.out.println("[INFO]: Edit Data on Patient Page.");
        $(EditButton).waitUntil(enabled, 30000).click();
        EditedYear = pf.randBetween(1900, 2015);
        EditedMonth = pf.getRandom(pf.Months);
        EditedDay = pf.getRandom(pf.Days);

        pf
        .RN2 = System.currentTimeMillis();

        $(EditFirstNameField).waitUntil(enabled, 15000);
        $(EditFirstNameField).setValue(pf.EditFirstName);
        $(EditMiddleNameField).setValue(pf.EditMiddleName);
        $(EditLastNameField).setValue(pf.EditLastName + pf.RN2);
        $(EditDobYearField).setValue(Integer.toString(EditedYear));

        mm
        .selectFromPicklistUniversal(EditMonthField, EditedMonth)
        .selectFromPicklistUniversal(EditDayField, EditedDay);

        $(EditGenderFemaleButton).waitUntil(enabled, 60000).click();
        $(RemoveThreatingPhisicianSelection).waitUntil(enabled, 60000).click();

        mm
        .selectFromSearchField(pf.ThreatingPhisicianField, ThreatingFirstRecord, pf.ThreatingPhysician);
        return this;
    }

    public DemographicsTab submitEditedDataOnPatientPage(){
        System.out.println("[INFO]: Submit Edited Data on Patient Page.");
        $(SubmitButton).waitUntil(enabled, 30000).click();
        return this;
    }

    public DemographicsTab submitPatientForm(){
        System.out.println("[INFO]: Submit Edited Patient data.");
        $(pf.TotalConfirmButton).waitUntil(enabled, 15000).click();
        $(NameHeader).waitUntil(visible, 15000);
        return this;
    }

    public DemographicsTab checkEditedDataOnPatientPage() throws InterruptedException {
        System.out.println("[INFO]: Check Edited data on Patient page.");
        Thread.sleep(5000);
        $(NameHeader).shouldHave(text(pf.EditLastName + pf.RN2 + ", " + pf.EditFirstName + "  " + pf.EditMiddleName));
        $(BirthdayHeader).shouldHave(text(EditedDay +"-"+ EditedMonth +"-"+ Integer.toString(EditedYear)));
        $(NameInfoBlock).shouldHave(text(pf.EditLastName + pf.RN2 + ", " + pf.EditFirstName + " " + pf.EditMiddleName));
        $(BirthdayInfoBlock).shouldHave(text(EditedDay +"-"+ EditedMonth +"-"+ Integer.toString(EditedYear)));
        $(GenderInfoBlock).shouldHave(text(pf.EditGender));
        $(PhysicianNameInfoBlock).shouldHave(text(pf.EditPhysicianName));
        //$(PhysicianEmailInfoBlock).shouldHave(text(pf.EditPhysicianEmail));
        return this;
    }

    public DemographicsTab cancelEditFormYes() {
        System.out.println("[INFO]: Click 'Cancel' button.");
        $(CancelButton).waitUntil(enabled, 30000).click();
        System.out.println("[INFO]: Check Message for 'Cancel'.");
        $(CancelMessage).shouldHave(text("Are you sure you want to cancel? Any changes made will be lost."));
        System.out.println("[INFO]: Close window by 'Yes' button.");
        $(CancelYesButton).waitUntil(enabled, 30000).click();
        $(EditButton).waitUntil(enabled, 30000);
        return this;
    }

    public DemographicsTab cancelEditFormNo(){
        System.out.println("[INFO]: Click 'Cancel' button.");
        $(CancelButton).waitUntil(enabled, 30000).click();
        System.out.println("[INFO]: Check Message for 'Cancel'.");
        $(CancelMessage).shouldHave(text("Are you sure you want to cancel? Any changes made will be lost."));
        System.out.println("[INFO]: Close window by 'No' button.");
        $(CancelNoButton).waitUntil(enabled, 30000).click();
        $(CancelButton).waitUntil(enabled, 30000);
        $(SubmitButton).click();
        return this;
    }
}
