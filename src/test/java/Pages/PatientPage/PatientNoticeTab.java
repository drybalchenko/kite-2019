package Pages.PatientPage;

import BaseTests.BaseTests;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class PatientNoticeTab extends BaseTests{
    //---LOCATORS

    @FindBy(xpath = "//*[contains(text(), 'Save & Exit')]")
    public SelenideElement SaveAndExitButton;

    @FindBy(xpath = "//*[contains(text(), 'Save & Next')]")
    public SelenideElement SaveAndNextButton;

    //---METHODS

    public PatientNoticeTab goToContactInformationTab(){
        System.out.println("[INFO]: Navigate to Patient Contact Information Form.");
        $(SaveAndNextButton).waitUntil(enabled, 20000).click();
        $(pcit.ContactMethodPhone).waitUntil(visible, 20000);
        return this;
    }
}
