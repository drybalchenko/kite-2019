package Pages.PatientPage;

import BaseTests.BaseTests;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class PatientTeamTab extends BaseTests{
    public String CoordinatorName = "TestHCP Full";

    //---LOCATORS

    @FindBy(xpath = "//button/lightning-icon")
    public SelenideElement AddPatientTeamMemberButton;

    @FindBy(xpath = "//a[contains (text(), 'add')]")
    public SelenideElement AddButton;

    //---Financial Coordinator window
    @FindBy(xpath = "//input[@placeholder='Search Contacts'] ")
    public SelenideElement SearchContactsField;

    @FindBy(xpath = "//div[@class='slds-lookup__menu']/ul[@class='slds-lookup__list']/li[2]")
    public SelenideElement CoordinatorFirstValue;

    //---Financial Coordinator window Buttons
    @FindBy(xpath = "//button[contains (text(), 'Close')]")
    public SelenideElement CloseButton;

    @FindBy(xpath = "//button[@class='slds-button slds-button--brand slds-p-left--small slsavenext' and contains (text(), 'Save')]")
    public WebElement SaveButton;

    //---BUTTONS
    @FindBy(xpath = "//button[contains (text(), 'Continue to Medical Information')]")
    public SelenideElement ContinueButton;

    //---METHODS

    public PatientTeamTab addNewTeamMember(){
        try {
            if ($(AddButton).isDisplayed()) {
                System.out.println("[INFO]: Add new Team Member.");
                $(AddButton).waitUntil(enabled, 15000).click();
                $(SearchContactsField).waitUntil(enabled, 15000);
                mm.selectFromSearchField(SearchContactsField, CoordinatorFirstValue, "Super Contact");
                $(SaveButton).waitUntil(enabled, 15000).click();
        }} catch(Exception e) {}

        return this;
    }

    public PatientTeamTab goToMedicalInfo(){
        $(ContinueButton).waitUntil(enabled,15000).click();
        $(mip.FirstDiagnosis).waitUntil(visible,15000);
        return this;
    }
}
