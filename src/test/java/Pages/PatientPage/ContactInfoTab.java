package Pages.PatientPage;

import BaseTests.BaseTests;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class ContactInfoTab extends BaseTests{
    public String Street = "Street";
    public String City = "City";
    public String Zip = "03162";
    public String Phone = "0123456789";
    public String Email = "TEST@test.com";
    //---Total Data
    public String TotalAddress = "City, CO, 03162";
    public String TotalPhone = "(012) 345-6789";
    public String PreferredContact = "Phone";
    public String PreferredLanguage = "English";

    //---EDITED Data---

    public String EditStreet = "EditedStreet";
    public String EditCity = "EditedCity";
    public String EditZip = "03190";
    public String EditPhone = "9876543210";
    public String EditEmail = "EDITED@test.com";
    //---Total Data
    public String TotalEditAddress = "EditedCity, LA, 03190";
    public String TotalEditPhone = "(987) 654-3210";
    public String EditPreferredContact = "Email";
    public String EditPreferredLanguage = "Spanish";

    public String ErrorMessagePhone = "Phone number is required if patient's preferred contact method is 'Phone'";
    public String ErrorMessageEmail = "Email is required if patient's preferred contact method is 'Email'";
    public String ErrorMessageInvalidEmail = "Please enter a valid email";

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_PatientInfo']/div[8]/div[1]/div/div[1]/div//input")
    public SelenideElement StreetField;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_PatientInfo']/div[8]/div[1]/div/div[2]/div[1]//input")
    public SelenideElement CityField;

    @FindBy(xpath = "//div[@id='tabPanel']/div/div[8]/div[1]/div/div[2]/div[2]//div[@class='slds-form-element__control']/div/select[@class='slds-select']")
    public SelenideElement StateField;

    @FindBy(xpath = "//option[@value='USA']")
    public SelenideElement StateValue;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_PatientInfo']/div[8]/div[1]/div/div[2]/div[3]//input")
    public SelenideElement ZipField;

    @FindBy(xpath = "//label[@for='prefContactId0']/span")   //div[8]/div[2]/div[1]//div[@class='slds-form-element__control']/div/span[1]/label[@class='slds-radio--button__label']
    public SelenideElement ContactMethodPhone;

    @FindBy(xpath = "//label[@for='prefContactId1']")
    public SelenideElement ContactMethodEmail;

    @FindBy(xpath = "//label[@for='prefContactId2']")
    public SelenideElement ContactMethodMail;

    @FindBy(xpath = "//label[@for='languageId0']")
    public SelenideElement LanguageEnglish;

    @FindBy(xpath = "//label[@for='languageId1']")
    public SelenideElement LanguageSpanish;

    @FindBy(xpath = "//label[@for='languageId2']")
    public SelenideElement LanguageOther;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_PatientInfo']/div[8]/div[3]/div[4]//input")
    public SelenideElement OtherLanguageField;

    @FindBy(css = ".cCmp_SL_MaskedInput .uiInput--default")
    public SelenideElement PhoneField;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_PatientInfo']/div[8]/div[2]/div[3]//input")
    public SelenideElement EmailField;

    //---Buttons

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_PatientInfo']/div[8]/div[4]/div//button[contains (text(), 'Save & Exit')]")
    public SelenideElement SaveAndExitContactInfo;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_PatientInfo']/div[8]/div[4]/div//button[contains (text(), 'Save & Next')]")
    public SelenideElement SaveAndNextContactInfo;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_PatientInfo']/div[7]/h1[@class='slds-section-title']/section//a")
    public SelenideElement EditContactInfo;

    //---Error Message

    @FindBy(xpath = "//span[@class='slds-form-element__help cSL_FormElementError']")
    public SelenideElement ContactInfomationErrorMessage;

    //@FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_PatientInfo']/div[8]/div[3]/div[2]//span")
    @FindBy(xpath = "//span[@class='slds-form-element__help cSL_FormElementError']")
    public SelenideElement EmailErrorMessage;

    //---TOTAL PAGE Patient Information

    @FindBy(xpath = "//span[@id='AddressView']")
    public SelenideElement StreetInfo;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_PatientInfo']/div[8]/div[1]/div/div[2]/div//span")
    public SelenideElement AddressInfo;

    @FindBy(xpath = "//span[@id='PrimePhoneView']")
    public SelenideElement PhoneInfo;

    @FindBy(xpath = "//span[@id='ContactEmailView']")
    public SelenideElement EmailInfo;

    @FindBy(xpath = "//span[@id='PrefContactView']")
    public SelenideElement ContactMethodInfo;

    @FindBy(xpath = "//span[@id='ContactPrefLangView']")
    public SelenideElement LanguageInfo;

    //---Button
    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_PatientInfo']/div[7]/h1[@class='slds-section-title']/section//a")
    public SelenideElement ContactInfoEditButton;

    //---
    @FindBy(css = ".cCmp_SL_StartedOrderContinue .slds-float--right")
    public SelenideElement ContactInfoContinueButton;


    //---METHODS

    public ContactInfoTab goToPatientTeamTab(){
        $(SaveAndNextContactInfo).waitUntil(enabled, 10000).click();
        $(ptt.AddButton).waitUntil(present, 15000);
        return this;
    }

    public ContactInfoTab navigateToContactInformation() throws InterruptedException {
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        System.out.println("[INFO]: Navigate to 'Contact Information Form'");
        $(pnt.SaveAndNextButton).waitUntil(enabled,15000).click();
        $(ContactMethodPhone).waitUntil(enabled,30000);
        return this;
    }

    public ContactInfoTab fillFieldsInContactInformation(){
        System.out.println("[INFO]: Fill fields in 'Contact Information Form'");
        $(StreetField).setValue(Street);
        $(CityField).setValue(City);
        mm.selectFromPicklistUniversal(StateField, "CO");
        $(ZipField).setValue(Zip);
        $(PhoneField).setValue(Phone);
        $(EmailField).setValue(Email);

        LanguageEnglish.click();
        ContactMethodPhone.click();
        return this;
    }
    public ContactInfoTab clickSaveAndNextButton(){
        $(SaveAndNextContactInfo).waitUntil(enabled, 30000).click();
        return this;
    }

    public ContactInfoTab clickSaveAndExitButton(){
        $(SaveAndExitContactInfo).waitUntil(enabled, 30000).click();
        return this;
    }

    public ContactInfoTab checkContactInformation(){
        System.out.println("[INFO]: Check Contact Information data");
        $(AddressInfo).shouldHave(text(TotalAddress));
        $(PhoneInfo).shouldHave(text(TotalPhone));
        $(EmailInfo).shouldHave(text(Email));
        $(ContactMethodInfo).shouldHave(text(PreferredContact));
        $(LanguageInfo).shouldHave(text(PreferredLanguage));
        return this;
    }

    public ContactInfoTab editContactInformation(){
        System.out.println("[INFO]: Edit Contact Information");
        $(EditContactInfo).waitUntil(enabled, 10000).click();
        $(StreetField).setValue(EditStreet);
        $(CityField).setValue(EditCity);
        mm.selectFromPicklistUniversal(StateField, "LA");
        $(ZipField).setValue(EditZip);
        $(PhoneField).setValue(EditPhone);
        $(EmailField).setValue(EditEmail);

        $(LanguageSpanish).click();
        $(ContactMethodEmail).click();
        //$(SaveAndNextContactInfo).click();
        return this;
    }

    public ContactInfoTab editContactInformation2(){
        System.out.println("[INFO]: Edit Contact Information");
        $(StreetField).setValue(EditStreet);
        $(CityField).setValue(EditCity);
        mm.selectFromPicklistUniversal(StateField, "LA");
        $(ZipField).setValue(EditZip);
        $(PhoneField).setValue(EditPhone);
        $(EmailField).setValue(EditEmail);

        $(LanguageSpanish).click();
        $(ContactMethodEmail).click();
        $(SaveAndNextContactInfo).click();
        return this;
    }

    public ContactInfoTab checkEditedContactInformation(){
        System.out.println("[INFO]: Check Edited Contact Information");
        $(AddressInfo).shouldHave(visible, text(TotalEditAddress));
        $(PhoneInfo).shouldHave(visible, text(TotalEditPhone));
        $(EmailInfo).shouldHave(visible, text(EditEmail));
        $(ContactMethodInfo).shouldHave(visible, text(EditPreferredContact));
        $(LanguageInfo).shouldHave(visible, text(EditPreferredLanguage));
        return this;
    }
}
