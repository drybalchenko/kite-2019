package Pages.PatientPage;

import BaseTests.BaseTests;
import org.openqa.selenium.support.FindBy;
import com.codeborne.selenide.SelenideElement;

import java.io.File;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Condition.*;

public class AcknowledgementTab extends BaseTests {

    String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());

    //---Locators
    @FindBy(linkText = "Upload Again")
    public SelenideElement UploadAgainLink;

    @FindBy(xpath = "//span[.='Acknowledgement and Notice Complete']")
    public SelenideElement NoticeCompleteMessage;

    @FindBy(xpath = "//div[4]/h1[@class='slds-section-title']/section//a")
    public SelenideElement UploadEditButton;

    @FindBy(xpath = "//div[@class='slds-tabs_scoped__content slds-show cCmp_SL_PrintandScan_Container']//span[2]")
    public SelenideElement DownloadDate;

    @FindBy(xpath = "//div[@class='slds-tabs_scoped__content slds-show cCmp_SL_PrintandScan_Container']//div[2]//span[2]")
    public SelenideElement UploadDate;

    //---Buttons
    @FindBy(css = ".cCmp_SL_PatientAuthorizationForm .slds-button--neutral.cCmp_SL_SaveComeBack")
    public SelenideElement SaveAndExitButton;

    @FindBy(css = ".cCmp_SL_PatientAuthorizationForm .slds-button--brand.slds-p-left--small")
    public SelenideElement SaveAndNextButton;

    @FindBy(css = "input[type = 'file']")
    public SelenideElement UploadField;

    //---Methods
    public Pages.PatientPage.AcknowledgementTab uploadFile(String testFile) throws InterruptedException {
        File file = new File(testFile);

        String path = file.getAbsolutePath();
        System.out.println("[INFO]: Upload file - " + path);
        Thread.sleep(3000);

        $(UploadField).waitUntil(enabled, 30000).sendKeys(path);
        Thread.sleep(3000);
        $(UploadAgainLink).waitUntil(enabled,120000);
        System.out.println("[INFO]: File uploaded successfully.");
        return this;
    }

    public Pages.PatientPage.AcknowledgementTab newUploadFile(String testFile) throws InterruptedException {
        File file = new File(testFile);

        String path = file.getAbsolutePath();
        System.out.println("[INFO]: Upload file - " + path);
        Thread.sleep(3000);

        $(UploadField).waitUntil(enabled, 30000).sendKeys(path);
        Thread.sleep(3000);
        return this;
    }

    public Pages.PatientPage.AcknowledgementTab uploadLargeFile(String testFile) throws InterruptedException {
        File file = new File(testFile);
        String path = file.getAbsolutePath();
        System.out.println("[INFO]: Upload file - " + path);
        Thread.sleep(3000);
        $(UploadField).waitUntil(enabled, 30000).sendKeys(path);
        return this;
    }

    public Pages.PatientPage.AcknowledgementTab checkUploadedFile() throws InterruptedException {
        openAttachmentsBlock();
        $(DownloadDate).waitUntil(visible, 100000 ).shouldHave(text("Downloaded " + currentDate));
        $(UploadDate).waitUntil(visible, 60000 ).shouldHave(text("Uploaded " + currentDate));
        return this;
    }

    public Pages.PatientPage.AcknowledgementTab checkUploadedLargeFile() throws InterruptedException {
        openAttachmentsBlock();
        $("input[type = 'file']").isEnabled();
        return this;
    }

    public Pages.PatientPage.AcknowledgementTab openAttachmentsBlock() throws InterruptedException {
        Thread.sleep(5000);
        try{
            if($(UploadEditButton).isDisplayed()) {
                $("Signature Pending").isDisplayed();
                $(UploadEditButton).waitUntil(enabled, 20000).click();
            }} catch (Exception e){};
        return this;
    }

    public Pages.PatientPage.AcknowledgementTab clickSaveAndExitButton(){
        $(SaveAndExitButton).waitUntil(enabled, 20000).click();
        return this;
    }

    public Pages.PatientPage.AcknowledgementTab clickSaveAndNextButton(){
        $(SaveAndNextButton).waitUntil(enabled, 30000).click();
        $(UploadEditButton).waitUntil(enabled, 30000);
        return this;
    }
}
