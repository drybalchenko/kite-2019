package Pages.Salesforce;

import BaseTests.BaseTests;
import com.codeborne.selenide.SelenideElement;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Collection;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class SalesfrcePO extends BaseTests{

    int number = 0;

    @FindBy(css = ".slds-icon-waffle")
    public SelenideElement AppLauncherIcon;

    @FindBy(xpath = "//input[@id='thePage:inputForm:continue']")
    public SelenideElement ContinueButton;

    @FindBy(xpath = "//input[@class='slds-input slds-text-color_default slds-p-left--none slds-size--1-of-1 input default input uiInput uiInputTextForAutocomplete uiInput--{remove}']")
    public SelenideElement SearchField;

    @FindBy(xpath = "//li[@class='lookup__item TYPEAHEAD_SCOPED slds-text-link--reset slds-grid slds-grid--vertical-align-center slds-truncate default uiAutocompleteOption forceSearchInputDesktopOption']")
    public SelenideElement SearchResultFirstValue;


    @FindBy(xpath = "//div[@class='leftContent slds-col slds-wrap slds-text-body--small slds-text-color--weak']")
    public SelenideElement PatientsBlock;

    @FindBy(xpath = "//div[@aria-expanded='true']//span[@class='title' and contains (text(), 'Related')]")
    public SelenideElement PatientRelatedTab;

    @FindBy(xpath = "//div[2]/article//span[@class='view-all-label']")
    public SelenideElement ViewAllSupportRequests;

    @FindBy(xpath = "//h1[@title='Support Request']")
    public SelenideElement SupportRequestPage;

    @FindBy(xpath = "//th[@class='slds-cell-edit cellContainer']//a[@class='slds-truncate outputLookupLink slds-truncate forceOutputLookup']")
    public SelenideElement SupportRequestNumber;

    @FindBy(xpath = "//th[@class='slds-cell-edit cellContainer']")
    public SelenideElement SupportRequestsList;

    @FindBy(xpath = "//div[@class='windowViewMode-maximized active lafPageHost']//span[contains (text(), 'Details')]")
    public SelenideElement SupportRequestDetailsTab;

    @FindBy(xpath = "//div[@class='recordTypeName slds-grow slds-truncate']")
    public SelenideElement SupportRequestRecordType;

    @FindBy(xpath = "//ul[@class='lookup__list  visible']")
    public SelenideElement SearchResultsList;


    //---Contacts Page
    @FindBy(xpath = "//a[@class='slds-button slds-button--reset downIcon slds-m-top_xxx-small slds-p-right_xxx-small' and @title='Select List View']")
    public SelenideElement OpenListViews;

    @FindBy(xpath = "//li[3]//span[contains (text(), 'All HCPs')]")
    public SelenideElement SelectAllHCP;

    @FindBy(css = ".slds-m-left_large [type]")
    public SelenideElement ContactSearchField;

    @FindBy(xpath = "//tbody/tr/td[@class='slds-cell-edit slds-cell-error errorColumn cellContainer']")
    public SelenideElement ContactLink;

    @FindBy(xpath = "//a[@title='Test User1234567890']")
    public SelenideElement TestContactLink;


    //--- METHODS
    public SalesfrcePO openPatientNew(){
        System.out.println("[INFO]: Open Patient page.");
        open("https://kitekonnect--full.lightning.force.com/lightning/o/Contact");
        $(OpenListViews).waitUntil(enabled, 30000).click();
        $(SelectAllHCP).waitUntil(enabled, 30000).click();
        $(ContactSearchField).waitUntil(enabled, 30000).sendKeys(pf.LastName+pf.RN2);
        $(ContactSearchField).sendKeys(Keys.ENTER);
        //$$(ContactLink).shouldHaveSize(0);

        try { if (number < 5){
            if($(By.xpath("//a[@title='Test " + pf.LastName+pf.RN2 + "']")).exists()) {
                $((By.xpath("//a[@title='Test " + pf.LastName+pf.RN2 + "']"))).waitUntil(enabled, 15000).click();
            } else {
                number++;
                System.out.println(number);
                Thread.sleep(30000);
                openPatientNew();
            }
        }
        }
        catch (Exception e) {
        }
        return this;
    }

    public SalesfrcePO openPatientNew2(){
        System.out.println("[INFO]: Open Patient page.");
        open("https://kitekonnect--full.lightning.force.com/lightning/o/Contact");
        $(OpenListViews).waitUntil(enabled, 30000).click();
        $(SelectAllHCP).waitUntil(enabled, 30000).click();
        $(ContactSearchField).waitUntil(enabled, 30000).sendKeys(pf.LastName+pf.RN2);
        $(ContactSearchField).sendKeys(Keys.ENTER);

        $(By.xpath("//a[@title='Test " + pf.LastName+pf.RN2 + "']")).waitUntil(enabled, 30000).click();
        return this;
    }

    public SalesfrcePO openPatient() throws InterruptedException {
        System.out.println("[INFO]: Open Patient page.");
        open("https://kitekonnect--full.lightning.force.com/lightning/o/Contact");
        Thread.sleep(10000);
        $(SearchField).click();
        $(SearchField).waitUntil(enabled, 60000).setValue(pf.LastName);
        Thread.sleep(30000);
        $(SearchField).click();
        $(SearchField).clear();
        $(SearchField).setValue(pf.LastName+pf.RN2);
        $(SearchField).click();
        Thread.sleep(6000);
        $$("uiOutputRichText").shouldHaveSize(0);
        $(SearchResultFirstValue).waitUntil(enabled, 60000).click();
        $(PatientRelatedTab).waitUntil(enabled, 60000);
        return this;
    }

    public SalesfrcePO openPatient2() throws InterruptedException {
        System.out.println("[INFO]: Open Patient page.");
        open("https://kitekonnect--full.lightning.force.com/lightning/page/home");
        Thread.sleep(6000);
        $(SearchField).waitUntil(enabled, 60000).setValue(""+pf.RN2);
        Thread.sleep(6000);
        $(SearchField).click();
        findPatientBySearchField();
        $(PatientRelatedTab).waitUntil(enabled, 60000);
        return this;
    }

    public SalesfrcePO findPatientBySearchField() throws InterruptedException {
        Thread.sleep(60000);
        try { if (number < 6){
            if(SearchResultFirstValue.exists()) {
                $(SearchResultFirstValue).waitUntil(enabled, 60000).click();
            } else {
                number++;
                System.out.println(number);
                openPatient2();
            }
            }
            }
            catch (Exception e) {
            }
        number=0;
        return this;
    }

    public SalesfrcePO checkSupportRequest(String recordType, int listSize){
        System.out.println("[INFO]: Open Support Requests.");
        $(PatientRelatedTab).click();
        $(ViewAllSupportRequests).waitUntil(enabled, 60000).click();

        $(SupportRequestNumber).waitUntil(enabled, 60000).click();
        $(SupportRequestDetailsTab).waitUntil(enabled, 60000).click();
        System.out.println("[INFO]: Check Support Requests record type.");
        $(SupportRequestRecordType).shouldHave(text(recordType));
        return this;
    }

    public void checkRequestInSalesforce(String recordType, int listSize) throws InterruptedException {
        System.out.println("[INFO]: Navigate to 'Salesforce' !!!!!!!!!!");
        open("https://kitekonnect--full.lightning.force.com/?un=kite@silverlinecrm.com.full&pw=$ummerTwo108");

        Thread.sleep(3000);
        try {
            if (spo.ContinueButton.exists()) {
                $(spo.ContinueButton).shouldBe(enabled).click(); }
        }
        catch(Exception e){
        }

        spo
        .openPatientNew()
        .checkSupportRequest(recordType, listSize);
    }
}
