package Pages.Salesforce;

import BaseTests.BaseTests;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static jdk.nashorn.internal.runtime.JSType.toInteger;

public class OrderManufacturingStatus extends BaseTests {

    public String [] Months = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

    public String patientInSalesforce= "https://kitekonnect--full.lightning.force.com/lightning/r/Order/8011g000000G2PlAAK/view?ws=%2Flightning%2Fr%2FPatient__c%2Fa0K1g000000gRT6EAM%2Fview";
    public String patientInApplication= "https://full-kitekonnect.cs96.force.com/s/patientdetails?patientId=735262185&caseId=a0H1g000004YfiXEAS";

    String timeStamp = new SimpleDateFormat("dd-MMM-yyyy").format(Calendar.getInstance().getTime());   //---"dd-MM-yyyy"

    //---LOCATORS
    @FindBy(xpath = "//div[@class='spinnerWrapper forceComponentSpinner']")
    public SelenideElement Spinner;

    //---In application
    @FindBy(xpath = "//ul[@class='cCmp_SL_OrderActivityTimeline']/li[1]//ul[@class='slds-list--horizontal slds-p-top--x-large']/div/div[2]//i")
    public SelenideElement ScheduleStatus;

    @FindBy(xpath = "//ul[@class='cCmp_SL_OrderActivityTimeline']/li[1]//ul[@class='slds-list--horizontal slds-p-top--x-large']/div/div[3]//i")
    public SelenideElement AphKitShippedStatus;

    @FindBy(xpath = "//ul[@class='cCmp_SL_OrderActivityTimeline']/li[1]//ul[@class='slds-list--horizontal slds-p-top--x-large']/div/div[4]//i")
    public SelenideElement AphMaterialStatus;

    @FindBy(xpath = "//ul[@class='cCmp_SL_OrderActivityTimeline']/li[3]//ul[@class='slds-list--horizontal slds-p-top--x-large']/div/div[2]//i")
    public SelenideElement ManufacturingStatus;

    @FindBy(xpath = "//ul[@class='cCmp_SL_OrderActivityTimeline']/li[3]//ul[@class='slds-list--horizontal slds-p-top--x-large']/div/div[3]//i")
    public SelenideElement ReleaseTestingStatus;

    @FindBy(xpath = "//ul[@class='cCmp_SL_OrderActivityTimeline']/li[3]//ul[@class='slds-list--horizontal slds-p-top--x-large']/div/div[3]//i")
    public SelenideElement ReadyToShipStatus;

    @FindBy(xpath = "//ul[@class='cCmp_SL_OrderActivityTimeline']/li[5]//ul[@class='slds-list--horizontal slds-p-top--x-large']/div/div[2]//i")
    public SelenideElement ShippedStatus;

    @FindBy(xpath = "//ul[@class='cCmp_SL_OrderActivityTimeline']/li[5]//ul[@class='slds-list--horizontal slds-p-top--x-large']/div/div[3]//i")
    public SelenideElement DeliveryStatus;

    //---In Salesforce
    @FindBy(xpath = "//article/div[1]/div/div[3]/div/div[@role='list']/div[1]/div[1]/div/div[2]/span/span") ////div/section[2]/div[@class='oneWorkspaceTabWrapper']/div[2]/div//div[@class='flexipagePage oneRecordHomeFlexipage']//div[@class='column region-main']/div[@class='flexipageComponent']/div/section[@role='tabpanel']/div/div/div[3]/div/div[3]/div/div[@role='list']/div[1]/div[1]/div/div[2]/span/span")//section[2]/div[@class='oneWorkspaceTabWrapper']/div[2]/div//div[@class='flexipagePage oneRecordHomeFlexipage']//div[@class='column region-main']/div[@class='flexipageComponent']/div/section[@role='tabpanel']/div/div/div[3]/div/div[3]/div/div[@role='list']/div[1]/div[1]
    public SelenideElement StatusField;

    @FindBy(xpath = "//button[@title='Edit Manufacturing Status']/lightning-primitive-icon")
    public SelenideElement EditStatusButton;

    @FindBy(xpath = "//button[@title='Edit Patient Name']/lightning-primitive-icon")
    public SelenideElement EditNameButton;

    @FindBy(xpath = "//article/div[2]/div/div[1]/div/div[3]/div/div[@role='list']/div[1]/div[1]//div[@class='uiPopupTrigger']//a[@role='button']") ////div/div[4]/div/div[1]/div/div[3]/div/div[@role='list']/div[1]/div[1]//div[@class='uiPopupTrigger']/div//a
    public SelenideElement OpenStatusList;

    //---Statuses
    @FindBy(xpath = "//li[@role='presentation']/a[@title='Draft Order']")
    public SelenideElement SelectDraftOrderStatus;

    @FindBy(xpath = "//li[@role='presentation']/a[@title='Leukapheresis - Scheduled']")
    public SelenideElement SelectLeukapheresisScheduledStatus;

    @FindBy(xpath = "//li[@role='presentation']/a[@title='Aph Kit Shipped']")
    public SelenideElement SelectAphKitShippedStatus;

    //@FindBy(xpath = "//li[@role='presentation']/a[@title='Draft Order']")
    //public SelenideElement SelectDraftOrderStatus;

    @FindBy(xpath = "//li[@role='presentation']/a[@title='Aph Material Received']")
    public SelenideElement SelectAphMaterialReceivedStatus;

    @FindBy(xpath = "//li[@role='presentation']/a[@title='Manufacturing']")
    public SelenideElement SelectManufacturingStatus;

    @FindBy(xpath = "//li[@role='presentation']/a[@title='Release Testing']")
    public SelenideElement SelectReleaseTestingStatus;

    @FindBy(xpath = "//li[@role='presentation']/a[@title='Ready to Ship']")
    public SelenideElement SelectReadyToShipStatus;

    @FindBy(xpath = "//li[@role='presentation']/a[@title='Final Product Shipped']")
    public SelenideElement SelectFinalProductShippedStatus;

    @FindBy(xpath = "//li[@role='presentation']/a[@title='Final Product Delivered']")
    public SelenideElement SelectFinalProductDeliveredStatus;

    //---Date Fields
    @FindBy(xpath = "//div[3]/div/div[@role='list']/div[3]/div[1]//input")
    public SelenideElement AphScheduledDateField;

    @FindBy(xpath = "//div[3]/div/div[@role='list']/div[3]/div[2]//input")
    public SelenideElement EstimatedFinalProductShipDateField;

    @FindBy(xpath = "//div[3]/div/div[@role='list']/div[4]/div[1]//input")
    public SelenideElement AphCompletedDateField;

    @FindBy(xpath = "//div[3]/div/div[@role='list']/div[4]/div[2]//input")
    public SelenideElement FinalProductShippedDateField;

    @FindBy(xpath = "//div[3]/div/div[@role='list']/div[5]/div[1]//input")
    public SelenideElement AphEstimatedReceiptDateField;

    @FindBy(xpath = "//div[3]/div/div[@role='list']/div[5]/div[2]//input")
    public SelenideElement FinalProductDeliveredDateField;

    @FindBy(xpath = "//div[3]/div/div[@role='list']/div[6]/div[1]//input")
    public SelenideElement AphReceiptDateField;

    //---Date Fields With Value
    @FindBy(xpath = "//section[2]/div[@class='oneWorkspaceTabWrapper']/div[2]/div//div[@class='flexipagePage oneRecordHomeFlexipage']//div[@class='column region-main']/div[@class='flexipageComponent']/div/section[@role='tabpanel']/div/div/div[3]/div/div[3]/div/div[@role='list']/div[3]/div[1]//div[2]")
    public SelenideElement AphScheduledDateValue;

    @FindBy(xpath = "//section[2]/div[@class='oneWorkspaceTabWrapper']/div[2]/div//div[@class='flexipagePage oneRecordHomeFlexipage']//div[@class='column region-main']/div[@class='flexipageComponent']/div/section[@role='tabpanel']/div/div/div[3]/div/div[3]/div/div[@role='list']/div[3]/div[2]//div[2]")
    public SelenideElement EstimatedFinalProductShipDateValue;

    @FindBy(xpath = "//section[2]/div[@class='oneWorkspaceTabWrapper']/div[2]/div//div[@class='flexipagePage oneRecordHomeFlexipage']//div[@class='column region-main']/div[@class='flexipageComponent']/div/section[@role='tabpanel']/div/div/div[3]/div/div[3]/div/div[@role='list']/div[4]/div[1]//div[2]")
    public SelenideElement AphCompletedDateValue;

    @FindBy(xpath = "//section[2]/div[@class='oneWorkspaceTabWrapper']/div[2]/div//div[@class='flexipagePage oneRecordHomeFlexipage']//div[@class='column region-main']/div[@class='flexipageComponent']/div/section[@role='tabpanel']/div/div/div[3]/div/div[3]/div/div[@role='list']/div[4]/div[2]//div[2]")
    public SelenideElement FinalProductShippedDateValue;

    @FindBy(xpath = "//section[2]/div[@class='oneWorkspaceTabWrapper']/div[2]/div//div[@class='flexipagePage oneRecordHomeFlexipage']//div[@class='column region-main']/div[@class='flexipageComponent']/div/section[@role='tabpanel']/div/div/div[3]/div/div[3]/div/div[@role='list']/div[5]/div[1]//div[2]")
    public SelenideElement AphEstimatedReceiptDateValue;

    @FindBy(xpath = "//section[2]/div[@class='oneWorkspaceTabWrapper']/div[2]/div//div[@class='flexipagePage oneRecordHomeFlexipage']//div[@class='column region-main']/div[@class='flexipageComponent']/div/section[@role='tabpanel']/div/div/div[3]/div/div[3]/div/div[@role='list']/div[5]/div[2]//div[2]")
    public SelenideElement FinalProductDeliveredDateValue;

    @FindBy(xpath = "//section[2]/div[@class='oneWorkspaceTabWrapper']/div[2]/div//div[@class='flexipagePage oneRecordHomeFlexipage']//div[@class='column region-main']/div[@class='flexipageComponent']/div/section[@role='tabpanel']/div/div/div[3]/div/div[3]/div/div[@role='list']/div[6]/div[1]//div[2]")
    public SelenideElement AphReceiptDateValue;

    //Buttons
    @FindBy(css = ".uiButton--brand.uiButton")
    public SelenideElement SaveButton;


    //--- METHODS
    public void selectStatus(SelenideElement statusInList, String status) throws InterruptedException {
        System.out.println("[INFO]: Login to Salesforce.");
        open ("https://kitekonnect--full.lightning.force.com/?un=kite@silverlinecrm.com.full&pw=$ummerTwo108");

        Thread.sleep(5000);
        try {
            if (spo.ContinueButton.exists()) {
                $(spo.ContinueButton).shouldBe(enabled).click(); }
        }
        catch(Exception e){
        }
        System.out.println("[INFO]: Open Patient Details.");
        open ("https://kitekonnect--full.lightning.force.com/lightning/r/Order/8011g000000G5a0AAC/view?ws=%2Flightning%2Fr%2FCase__c%2Fa0H1g000004YjWWEA0%2Fview");
        System.out.println("[INFO]: Select " +status+ " status.");
        $(EditNameButton).waitUntil(enabled, 60000).click();
        Thread.sleep(3000);
        $(OpenStatusList).waitUntil(exist, 30000).sendKeys(Keys.ENTER);

        Thread.sleep(5000);

        if($(statusInList).isDisplayed()){
            $(statusInList).waitUntil(enabled, 30000).click();
        } else {
            $(OpenStatusList).waitUntil(enabled, 30000).click();
            $(statusInList).waitUntil(enabled, 30000).click();
        }

        $(SaveButton).waitUntil(enabled, 30000).click();
        System.out.println("[INFO]: Check selected status.");
        (EditStatusButton).waitUntil(enabled, 60000);
        Thread.sleep(5000);
        $(StatusField).waitUntil(hasText(status), 30000);
        System.out.println("[INFO]: "+ $(StatusField).getText() +" is selected.");

    }

    public void clearDateFields() throws InterruptedException {
        System.out.println("[INFO]: Login to Salesforce.");
        open ("https://kitekonnect--full.lightning.force.com/?un=kite@silverlinecrm.com.full&pw=$ummerTwo108");
        //lp.loginToSalesforce2019();

        Thread.sleep(3000);
        try {
            if (spo.ContinueButton.exists()) {
                $(spo.ContinueButton).shouldBe(enabled).click(); }
        }
        catch(Exception e){
        }

        System.out.println("[INFO]: Open Patient Details.");
        open ("https://kitekonnect--full.lightning.force.com/lightning/r/Order/8011g000000G5a0AAC/view?ws=%2Flightning%2Fr%2FCase__c%2Fa0H1g000004YjWWEA0%2Fview");   //https://kitekonnect--full.lightning.force.com/lightning/r/Order/8011g000000G2PlAAK/view?ws=%2Flightning%2Fr%2FPatient__c%2Fa0K1g000000gRT6EAM%2Fview

        System.out.println("[INFO]: Clean all Date fields.");
        (EditNameButton).waitUntil(enabled, 60000).click();

        $(AphScheduledDateField).clear();
        $(EstimatedFinalProductShipDateField).clear();
        $(AphCompletedDateField).clear();
        $(FinalProductShippedDateField).clear();
        $(AphEstimatedReceiptDateField).clear();
        $(FinalProductDeliveredDateField).clear();
        $(AphReceiptDateField).clear();

        $(SaveButton).sendKeys(Keys.ENTER);
        //$(SaveButton).waitUntil(enabled, 30000).click();
        Thread.sleep(6000);
        //switchTo().alert().accept();
        //$(Spinner).waitUntil(hidden, 60000);
        /*
        open ("https://kitekonnect--full.lightning.force.com/lightning/r/Order/8011g000000G2PlAAK/view?ws=%2Flightning%2Fr%2FPatient__c%2Fa0K1g000000gRT6EAM%2Fview");
        $(EditStatusButton).waitUntil(enabled, 30000);

        $(AphScheduledDateValue).shouldBe(empty);
        $(EstimatedFinalProductShipDateValue).shouldBe(empty);
        $(AphCompletedDateValue).shouldBe(empty);
        $(FinalProductShippedDateValue).shouldBe(empty);
        $(AphEstimatedReceiptDateValue).shouldBe(empty);
        $(FinalProductDeliveredDateValue).shouldBe(empty);
        $(AphReceiptDateValue).shouldBe(empty);
        */
    }

    public void goToStatusesInApp() throws InterruptedException {
        System.out.println("[INFO]: Navigate to application.");
        open("https://full-kitekonnect.cs96.force.com");
        lp.loginToApplication2(lp.UserLogin, lp.UserPassword);
        System.out.println("[INFO]: Open Patient Details.");
        open("https://full-kitekonnect.cs96.force.com/s/patientdetails?patientId=842960519&caseId=a0H1g000004YjWWEA0");
        Thread.sleep(3000);
    }

    //---FOR STATUSES
    public void changeStatusToLeukapheresisScheduled() throws InterruptedException {
        selectStatus(SelectLeukapheresisScheduledStatus, "Leukapheresis - Scheduled");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        System.out.println(">>> Done <<<");
    }

    public void changeStatusToLeukapheresisCompleted() throws InterruptedException {
        selectStatus(SelectAphKitShippedStatus, "Leukapheresis - Completed");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        System.out.println(">>> Done <<<");
    }

    public void changeStatusToAphMaterialShipped() throws InterruptedException {
        selectStatus(SelectDraftOrderStatus, "Aph Material Shipped");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        System.out.println(">>> Done <<<");
    }

    public void changeStatusToAphMaterialReceived() throws InterruptedException {
        selectStatus(SelectAphMaterialReceivedStatus, "Aph Material Received");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        System.out.println(">>> Done <<<");
    }

    public void changeStatusToManufacturing() throws InterruptedException {
        selectStatus(SelectManufacturingStatus, "Manufacturing");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        System.out.println(">>> Done <<<");
    }

    public void changeStatusToReleaseTesting() throws InterruptedException {
        selectStatus(SelectReleaseTestingStatus, "Release Testing");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        System.out.println(">>> Done <<<");
    }

    public void changeStatusToReadyToShip() throws InterruptedException {
        selectStatus(SelectReadyToShipStatus, "Ready To Ship");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        System.out.println(">>> Done <<<");
    }

    public void changeStatusToFinalProductShipped() throws InterruptedException {
        selectStatus(SelectFinalProductShippedStatus, "Final Product Shipped");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        System.out.println(">>> Done <<<");
    }

    public void changeStatusToFinalProductDelivered() throws InterruptedException {
        selectStatus(SelectFinalProductDeliveredStatus, "Final Product Delivered");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        System.out.println(">>> Done <<<");
    }

    //---NEW TESTS
    //---FOR STATUSES
    public void changeStatusToLeukapheresisScheduled2() throws InterruptedException {
        selectStatus(SelectLeukapheresisScheduledStatus, "Leukapheresis - Scheduled");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("In Progress"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
    }

    public void changeStatusToAphKitShipped2() throws InterruptedException {
        selectStatus(SelectAphKitShippedStatus, "Aph Kit Shipped");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("In Progress"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
    }

    public void changeStatusToDraftOrder2() throws InterruptedException {
        selectStatus(SelectDraftOrderStatus, "Draft Order");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
    }

    public void changeStatusToAphMaterialReceived2() throws InterruptedException {
        selectStatus(SelectAphMaterialReceivedStatus, "Aph Material Received");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("In Progress"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
    }

    public void changeStatusToManufacturing2() throws InterruptedException {
        selectStatus(SelectManufacturingStatus, "Manufacturing");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("In Progress"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Pending"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
    }

    public void changeStatusToReleaseTesting2() throws InterruptedException {
        selectStatus(SelectReleaseTestingStatus, "Release Testing");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("In Progress"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("In Progress"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
    }

    public void changeStatusToReadyToShip2() throws InterruptedException {
        selectStatus(SelectReadyToShipStatus, "Ready To Ship");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
    }

    public void changeStatusToFinalProductShipped2() throws InterruptedException {
        String NewTotalDate;
        String date = getCurrentTimeStamp();

        selectStatus(SelectFinalProductShippedStatus, "Final Product Shipped");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));

        String[] newDate = date.split("-");
        String[] newDayNumber = newDate[0].split("");
        System.out.print("[INFO]: Date converted " + date + " >>> ");
        if (newDayNumber.length==1){
            int month = toInteger(newDate[1]);
            NewTotalDate = "0" + newDate[0] + "-" + Months[month-1] + "-" + newDate[2];
        } else {
            int month = toInteger(newDate[1]);
            NewTotalDate = newDate[0] + "-" + Months[month-1] + "-" + newDate[2];
        }

        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text(NewTotalDate));   //06-Feb-2019
        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));
    }

    public String getCurrentTimeStamp() {
        return new SimpleDateFormat("dd-MM-yyyy").format(new Date());
    }

    public void changeStatusToFinalProductDelivered2() throws InterruptedException {
        String NewTotalDate;
        String date = getCurrentTimeStamp();
        selectStatus(SelectFinalProductDeliveredStatus, "Final Product Delivered");
        goToStatusesInApp();
        System.out.println("[INFO]: Check data in Application.");
        $(ScheduleStatus).waitUntil(exist, 120000).shouldHave(text("TBD"));
        $(AphKitShippedStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(AphMaterialStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(ManufacturingStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(ReleaseTestingStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(ReadyToShipStatus).waitUntil(exist, 10000).shouldHave(text("Complete"));
        $(ShippedStatus).waitUntil(exist, 10000).shouldHave(text("TBD"));

        String[] newDate = date.split("-");
        String[] newDayNumber = newDate[0].split("");
        System.out.print("[INFO]: Date converted " + date + " >>> ");
        if (newDayNumber.length==1){
            int month = toInteger(newDate[1]);
            NewTotalDate = "0" + newDate[0] + "-" + Months[month-1] + "-" + newDate[2];
        } else {
            int month = toInteger(newDate[1]);
            NewTotalDate = newDate[0] + "-" + Months[month-1] + "-" + newDate[2];
        }

        $(DeliveryStatus).waitUntil(exist, 10000).shouldHave(text(NewTotalDate));
    }
}
