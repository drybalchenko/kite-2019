package Pages.MainPages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class SingInPage {

    @FindBy(xpath = "//a[@class='slds-button slds-button--brand ' and contains (text(), 'Sign In')]")
    public SelenideElement SingInButton;

    @FindBy(xpath = "//a[@class='slds-button slds-button--neutral' and contains (text(), 'Register')]")
    public SelenideElement RegisterButton;

    //---REGISTRATION form

    @FindBy(xpath = "//input[@placeholder='Search Institutions']")
    public SelenideElement InstitutionsField;

    //---Phisician

    @FindBy(xpath = "//button[@class='slds-button slds-button--neutral' and contains (text(), 'Yes') ]")
    public SelenideElement PhisicianYesButton;

    @FindBy(xpath = "//button[@class='slds-button slds-button--neutral' and contains (text(), 'No') ]")
    public SelenideElement PhisicianNoButton;

    //---Account Information

    @FindBy(xpath = "//input[@data-interactive-lib-uid='16']")
    public SelenideElement FistNameField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='17']")
    public SelenideElement LastNameField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='18']")
    public SelenideElement InstitutionEmailField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='19']")
    public SelenideElement ConfirmInstitutionEmailField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='20']")
    public SelenideElement BusinessPhoneField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='21']")
    public SelenideElement MobilePhoneField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='22']")
    public SelenideElement FaxField;

    @FindBy(xpath = "//div[@class='slds-grid slds-wrap slds-p-bottom--medium'][6]//button[@class='slds-button slds-button--brand' and contains (text(), 'Confirm & Continue')]")
    public SelenideElement ConfirmButton;

    //---BUTTONS

    @FindBy(xpath = "//a[contains (text(), 'Back to Login Page')]")
    public SelenideElement BackToLoginPageButton;
}
