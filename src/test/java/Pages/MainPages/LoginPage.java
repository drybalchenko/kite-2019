package Pages.MainPages;

import BaseTests.BaseTests;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class LoginPage extends BaseTests{

    public String SalesforceBaseUrl = "https://test.salesforce.com/";

    public String UserLogin = "fred@flintstone.com";
    public String UserPassword = "TestingP4";

    public String TestUserLogin = "dmitry.rybalchenko@silverlinecrm.com2018";
    public String TestUserPassword = "Tests@2018";

    public String UserLogin3 = "kite@silverlinecrm.com.full";
    public String UserPassword3 = "$ummerTwo108";

    public String TestLogin2 = "scca.hcp@kitepharma.com.full";
    public String TestPassword2 = "TestPassword@201819";

    public String MessageTextForUsername = "The username you entered is not in a valid format. If you do not remember your username, please contact your Kite Konnect™ Case Manager at 1-844-454-KITE.";
    public String MessageTextForPassword = "Your login attempt has failed. Make sure the username and password are correct.";

    @FindBy(xpath = "//a[@class='slds-button slds-button--brand ' and contains (text(), 'Sign In')]")
    public SelenideElement SingInButton;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='2']")
    public SelenideElement LoginField;

    @FindBy(xpath = "//input[@type='password']")
    public SelenideElement PasswordField;

    @FindBy(xpath = "//button[@class='slds-button slds-button--brand' and contains (text(), 'Sign In')]")
    public SelenideElement LoginButton;

    //---Messages
    @FindBy(xpath = "//div[@id='centerPanel']/div/div[2]/div/div[1]/div[1]//p")
    public SelenideElement MessageForInvalidLoginData;

    @FindBy(xpath = "//button[contains (text(), 'Close')]")
    public SelenideElement MessageCloseButton;

    //---Salesforce
    @FindBy(xpath = "//input[@id='username']")
    public SelenideElement SlLoginField;

    @FindBy(xpath = "//input[@id='password']")
    public SelenideElement SlPasswordField;

    @FindBy(xpath = "//input[@id='Login']")
    public SelenideElement SlLoginButton;

    @FindBy(xpath = "//a[@class='switch-to-lightning']")
    public SelenideElement SwitchToLightningLink;

    //---Methods

    public LoginPage loginToApplication (String login, String password) {
        $(SingInButton).waitUntil(enabled, 30000).click();
        $(LoginField).waitUntil(enabled, 30000).setValue(login);
        //$(LoginField).setValue(login);
        $(PasswordField).shouldBe(enabled).setValue(password);
        $(LoginButton).shouldBe(enabled).click();

        $(hp.EnrollNewPatientButton).waitUntil(enabled, 60000);
        return this;
    }

    public LoginPage loginToApplication2 (String login, String password) throws InterruptedException {

        $(SingInButton).waitUntil(enabled, 30000).click();
        $(LoginField).waitUntil(enabled, 15000).setValue(login);
        $(PasswordField).shouldBe(enabled).setValue(password);
        $(LoginButton).shouldBe(enabled).click();

        Thread.sleep(5000);
        return this;
    }

    public LoginPage loginWithIncorrectLogin(){
        open("https://full-kitekonnect.cs96.force.com/s/");
        System.out.println("[INFO]: Navigate to Login Page.");

        $(lp.SingInButton).waitUntil(enabled, 15000).click();
        $(lp.LoginField).waitUntil(enabled, 15000);

        System.out.println("[INFO]: Try to Login with incorrect Username.");
        $(LoginField).setValue("TestUser2018");
        $(PasswordField).shouldBe(enabled).setValue(lp.UserPassword);
        $(LoginButton).shouldBe(enabled).click();

        System.out.println("[INFO]: Check Error message for incorrect Username.");
        $(lp.MessageCloseButton).waitUntil(enabled, 15000);
        $(MessageForInvalidLoginData).shouldHave(visible, text(MessageTextForUsername));
        $(MessageCloseButton).click();
        return this;
    }

    public LoginPage loginWithIncorrectPassword(){
        System.out.println("[INFO]: Try to Login with incorrect Password.");
        open("https://full-kitekonnect.cs96.force.com/s/");
        $(lp.SingInButton).waitUntil(enabled, 15000).click();
        $(LoginField).waitUntil(enabled, 15000).setValue(lp.UserLogin);
        $(PasswordField).setValue("1234567890");
        $(LoginButton).click();

        System.out.println("[INFO]: Check Error message for incorrect Password.");
        $(lp.MessageCloseButton).waitUntil(enabled, 15000);
        $(MessageForInvalidLoginData).shouldHave(visible, text(MessageTextForPassword));
        $(MessageCloseButton).click();
        return this;
    }

    public LoginPage loginToSalesforce() throws InterruptedException {
        System.out.println("[INFO]: Login to Salesforce.");
        open("https://kitekonnect--full.lightning.force.com/?un=kite@silverlinecrm.com.full&pw=$ummerTwo108");

        Thread.sleep(5000);
        try {
            if (spo.ContinueButton.exists()) {
                $(spo.ContinueButton).shouldBe(enabled).click();
            }
        } catch (Exception e) {}
        return this;
    }

    //---

    public LoginPage loginToSalesforce2019 () {
        System.out.println("[INFO]: Login to Salesforce with Login and Password.");
        https://kitekonnect--full.lightning.force.com/?un=kite@silverlinecrm.com.full&pw=$ummerTwo108
        open("https://kitekonnect--full.lightning.force.com/login");
        $(SlLoginField).waitUntil(enabled, 30000).setValue("kite@silverlinecrm.com.full");
        $(SlPasswordField).waitUntil(enabled, 30000).setValue("$ummerTwo108");
        $(SlLoginButton).waitUntil(enabled, 30000).click();

        try{ if(SwitchToLightningLink.exists()){
            $(SwitchToLightningLink).waitUntil(enabled, 30000).click();
        }} catch (Exception e){}

        $(spo.AppLauncherIcon).waitUntil(enabled, 30000).click();
        return this;
    }
/*
    public LoginPage loginToSalesforceByLink() throws InterruptedException {
        System.out.println("[INFO]: Login to Salesforce by link.");
        open("https://files-libraryfolders-dev-ed.lightning.force.com/?un=files.libraryfolders@silverlinecrm.com&pw=Silverline2018");

        Thread.sleep(5000);
        try {
            if (spo.ContinueButton.exists()) {
                $(spo.ContinueButton).shouldBe(enabled).click();
            }
        } catch (Exception e) {}
        return this;
    }
    */
}
