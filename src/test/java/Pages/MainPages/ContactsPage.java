package Pages.MainPages;

import BaseTests.BaseTests;
import org.openqa.selenium.support.FindBy;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class ContactsPage extends BaseTests{

    long Num;

    @FindBy(xpath = "//b[contains (text(), 'Hospital Contacts')]")
    public SelenideElement HospitalContactsBlock;

    @FindBy(xpath = "//button[@class='slds-button slds-button--neutral' and contains (text(), 'Add New Contact')]")
    public SelenideElement AddNewContactButton;

    @FindBy(xpath = "//input[@class='slds-lookup__search-input slds-input']")
    public SelenideElement SearchContactsField;

    @FindBy(xpath = "//li[@data-selindx='0']")
    public SelenideElement FirstRecordInResults;

    @FindBy(xpath = "//button[@class='slds-button slds-button--neutral' and contains (text(), 'Add to Default Team')]")
    public SelenideElement AddToDefaultTeamButton;

    @FindBy(xpath = "//label[@for='physicianCheck']/span[@class='slds-checkbox--faux']")
    public SelenideElement IncludePhysiciansCheckbox;

    @FindBy(xpath = "//tr[@class='slds-hint-parent'][1]//th")
    public SelenideElement FirstContactName;

    @FindBy(xpath = "//tr[@class='slds-hint-parent'][2]//th")
    public SelenideElement SecondContactName;

    @FindBy(xpath = "//tr[@class='slds-hint-parent'][2]//td[1]")
    public SelenideElement SecondContactRole;

    @FindBy(xpath = "//tr[@class='slds-hint-parent'][2]//td[2]")
    public SelenideElement SecondContactPhone;

    @FindBy(xpath = "//tr[@class='slds-hint-parent'][2]//td[3]")
    public SelenideElement SecondContactEmail;

    @FindBy(xpath = "//tr[@class='slds-hint-parent'][2]//td[5]/div[@class='slds-truncate slds-align--absolute-center']/a")
    public SelenideElement DeleteButtonForSecondContact;

    @FindBy(xpath = "//div[@id='default-1']/div")
    public SelenideElement EditButtonForSecondContact;

    //--- create / edit contact window
    @FindBy(xpath = "//h2[@class='slds-text-heading--medium']/span[ contains( text(), 'Edit Contact Information')]")
    public SelenideElement EditContactWindow;

    @FindBy(xpath = "//h2[@class='slds-text-heading--medium']/span[ contains( text(), 'Add New Contact')]")
    public SelenideElement AddNewContactWindow;

    @FindBy(xpath = "//div[@class='slds-modal__content slds-p-horizontal--medium slds-p-top--medium']/div[2]/div[1]//input")
    public SelenideElement FirstNameField;

    @FindBy(xpath = "//div[@class='slds-modal__content slds-p-horizontal--medium slds-p-top--medium']/div[2]/div[2]//input")
    public SelenideElement LastNameField;

    @FindBy(xpath = "//div[@class='slds-modal__content slds-p-horizontal--medium slds-p-top--medium']/div[3]/div[1]//input")
    public SelenideElement EmailField;

    @FindBy(xpath = "//div[@class='slds-modal__content slds-p-horizontal--medium slds-p-top--medium']/div[3]/div[2]//input")
    public SelenideElement PhoneField;

    @FindBy(xpath = "//div[@class='slds-modal__content slds-p-horizontal--medium slds-p-top--medium']/div[4]/div[1]//input")
    public SelenideElement RoleField;

    @FindBy(xpath = "//div[@class='slds-grid']//li[@role='presentation'][2]")
    public SelenideElement CreateNewRole;

    @FindBy(css = ".slds-tabs--scoped_content .slds-p-left--small")
    public SelenideElement EditWindowSaveButton;

    @FindBy(css = ".slds-p-horizontal--x-small > [aria-labelledby] .slds-p-left--small")
    public SelenideElement AddNewWindowSaveButton;


    //---METHODS

    public ContactsPage addNewContact(){
        System.out.println("[INFO]: Add New Contact.");
        Num = System.currentTimeMillis();
        open("https://full-kitekonnect.cs96.force.com/s/portalcontacts");
        $(AddNewContactButton).waitUntil(enabled, 50000).click();
        $(AddNewContactWindow).waitUntil(visible, 40000);
        $(FirstNameField).setValue("New" + Num);
        $(LastNameField).setValue("User");
        $(EmailField).setValue("newuser@test.com");
        $(PhoneField).setValue("1233210000");
        $(RoleField).setValue("newRole");
        $(CreateNewRole).waitUntil(enabled, 15000).click();
        $(AddNewWindowSaveButton).click();
        return this;
    }

    public ContactsPage navigateToContactsPage(){
        System.out.println("[INFO]: Navigate to Contacts Page.");
        open("https://full-kitekonnect.cs96.force.com/s/portalcontacts");
        $(AddNewContactButton).waitUntil(enabled, 50000);
        $(HospitalContactsBlock).shouldBe(visible);
        return this;
    }

    public ContactsPage searchContact() throws InterruptedException {
        System.out.println("[INFO]: Search Contact by 'Search' field.");
        $(SearchContactsField).waitUntil(enabled, 50000);
        mm
        .selectFromSearchField(SearchContactsField, FirstRecordInResults , "New" + Num);
        $(AddToDefaultTeamButton).click();
        Thread.sleep(3000);
        return this;
    }

    public ContactsPage checkContactData(){
        System.out.println("[INFO]: Check Contact information.");
        $(SecondContactName).shouldHave(text("New" + Num + " User"));
        $(SecondContactRole).shouldHave(text("newRole"));
        $(SecondContactPhone).shouldHave(text("(123) 321-0000"));
        $(SecondContactEmail).shouldHave(text("newuser@test.com"));
        return this;
    }

    public ContactsPage editContactData(){
        System.out.println("[INFO]: Edit Contact information.");
        $(EditButtonForSecondContact).click();
        $(EditContactWindow).waitUntil(enabled, 60000);   //visible
        $(FirstNameField).clear();
        $(FirstNameField).setValue("Edited");
        $(LastNameField).clear();
        $(LastNameField).setValue("Name");
        $(EmailField).clear();
        $(EmailField).setValue("edited@email.com");
        $(PhoneField).clear();
        $(PhoneField).setValue("0009998877");
        $(".slds-pill__remove .slds-button__icon").waitUntil(enabled, 6000).click();
        $(RoleField).setValue("editedRole");
        $(CreateNewRole).waitUntil(enabled, 15000).click();
        $(EditWindowSaveButton).waitUntil(enabled, 10000).click();
        //$(EditContactWindow).waitWhile(disappear, 30000);
        return this;
    }

    public ContactsPage checkEditedContactData(){
        System.out.println("[INFO]: Check edited Contact information.");
        $(SecondContactName).shouldHave(text("Edited Name"));
        $(SecondContactRole).shouldHave(text("editedRole"));
        $(SecondContactPhone).shouldHave(text("(000) 999-8877"));
        $(SecondContactEmail).shouldHave(text("edited@email.com"));
        return this;
    }

    public ContactsPage removeContacts(){
        System.out.println("[INFO]: Remove all existing Contacts.");
        $(FirstContactName).waitUntil(visible, 20000);
        try {
            if (DeleteButtonForSecondContact.exists()) {
                $(DeleteButtonForSecondContact).shouldBe(enabled).click();
                removeContacts(); }
            }
        catch(Exception e){
        }
        return this;
    }
}
