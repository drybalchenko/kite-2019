package Pages.MainPages;

import BaseTests.BaseTests;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import java.util.Random;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class PatientForm extends BaseTests{

    public double RN = Math.random()*1000;
    public long RN2;
    public int YEAR;
    public String MONTH;
    public String DAY;
    public String [] Days = {"01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27"};
    public String [] Months = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};


    public String RequiredFieldErrorMessage = "This field is required";
    public String PatientExistErrorMessage = "This patient appears to already be enrolled in Kite Konnect™. Please contact your Kite Konnect™ Case Manager to confirm.";

    //---Patient data
    public String ExistingFirstName = "ExistTest";
    public String FirstName = "Test";
    public String MiddleName = "super";
    public String LastName = "User214.5665517926818";
    public String DobYear = "1990";
    public String ThreatingPhysician = "William Basem";

    //---Total Page
    public String FullPatientName = LastName + ", " + FirstName + " " + MiddleName;
    public String FullPatientNameForPatientPage = LastName + ", " + FirstName + RN2 + "  " + MiddleName;
    public String Gender = "Male";
    public String PhysicianName = "Doctor, Test";
    public String PhysicianEmail = "test@test.com";

    //---Edit Patient data
    public String NewFirstName = "Edit";
    public String EditFirstName = "Edit";
    public String EditMiddleName = "William";
    public String EditLastName = "Fox";
    public String EditDobYear = "1985";
    public String EditThreatingPhysician = "Test Doctor";

    //---Edit Total Page
    public String EditGender = "Female";
    public String EditPhysicianName = "William, Basem";
    public String EditPhysicianEmail = "test@test.com";

    @FindBy(xpath = "//*[contains(text(), 'Please Register Your Patient')]")
    public SelenideElement PatientForm;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='2']")
    public SelenideElement FirstNameField;

    @FindBy(xpath = "//div[@class='slds-grid slds-wrap slds-grid--pull-padded cCmp_SL_PatientDemographics_Edit'][1]/div[@class='slds-small-size--4-of-12 slds-medium-size--5-of-12 slds-large-size--2-of-6  slds-p-around--medium cSL_GridColumn'][1]//span[@class='slds-form-element__help cSL_FormElementError']")
    public SelenideElement ErrorForFirstNameField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='3']")
    public SelenideElement MiddleNameField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='4']")
    public SelenideElement LastNameField;

    @FindBy(xpath = "//div[@class='slds-grid slds-wrap slds-grid--pull-padded cCmp_SL_PatientDemographics_Edit'][1]/div[@class='slds-small-size--4-of-12 slds-medium-size--5-of-12 slds-large-size--2-of-6  slds-p-around--medium cSL_GridColumn'][2]//span[@class='slds-form-element__help cSL_FormElementError']")
    public SelenideElement ErrorForLastNameField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='5']")
    public SelenideElement DobYearField;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[3]/div[1]/div/span")
    public SelenideElement ErrorForDobYearField;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[3]/div[2]/div/div/div/select")
    public SelenideElement MonthField;

    @FindBy(xpath = "//option[@value='Jan']")
    public SelenideElement MonthValue;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[3]/div[2]/div/span")
    public SelenideElement ErrorForMonthField;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[3]/div[3]/div/div/div/select")
    public SelenideElement DayField;

    @FindBy(xpath = "//option[@value='01']")
    public SelenideElement DayValue;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[3]/div[3]/div/span")
    public SelenideElement ErrorForDayField;

    @FindBy(css = "[class='slds-small-size--3-of-12 slds-medium-size--3-of-12 slds-large-size--3-of-12  slds-p-around--medium cSL_GridColumn'] .slds-radio--button:nth-of-type(1) .slds-radio--button__label")
    public SelenideElement MaleGenderButton;

    @FindBy(css = ".cSL_Radio:nth-child(1) .slds-radio--button:nth-child(2)")
    public SelenideElement FemaleGenderButton;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[3]/div[4]/fieldset/span")
    public SelenideElement ErrorForGender;

    @FindBy(id = "lookatme")
    public SelenideElement ThreatingPhisicianField;

    @FindBy(xpath = "//li[@data-selindx='0']")
    public SelenideElement ThreatingFirstRecord;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[4]/div[1]/div/span")
    public SelenideElement ErrorForThreatingPhisicianField;

    @FindBy(xpath = "//div[@id='lookatme']/ul/li[@data-selindx='0']")
    public SelenideElement ThreatingPhisicianFirstRecord;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[5]/div//div[@class='slds-form-element__control']/div/span[1]/label[@class='slds-radio--button__label']")
    public SelenideElement AddOwnHpYesButton;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[5]/div//div[@class='slds-form-element__control']/div/span[2]/label[@class='slds-radio--button__label']")
    public SelenideElement AddOwnHpNoButton;

    @FindBy(xpath = "//div[@class='slds-kite__content']/div[5]/div[1]/fieldset/span")
    public SelenideElement ErrorForAddOwnHp;

    //---Buttons
    @FindBy(xpath = "//button[@class='slds-button slds-button--brand slds-p-left--small slsavenext kite-button__long' and contains(text(), 'Submit')]")
    public SelenideElement SubmitButton;

    @FindBy(xpath = "//button[@class='slds-button slds-button--neutral kite-button__long' and contains(text(), 'Cancel')]")
    public SelenideElement CancelButton;

    //---Cancel Message
    @FindBy(xpath = "//div[@class='slds-kite__content']/section[1]//div[@class='slds-modal__content slds-p-around--medium']")
    public SelenideElement CancelMessageWindow;

    @FindBy(xpath = "//button[@class='slds-button slds-button--neutral' and contains (text(), 'Cancel')]")
    public SelenideElement CancelButtonInCancelMessage;

    @FindBy(xpath = "//button[@class='slds-button slds-button--brand slsavenext' and contains (text(), 'Ok')]")
    public SelenideElement OkButtonInCancelMessage;

    //---

    @FindBy(xpath = "")
    public SelenideElement IdTypeField;

    @FindBy(xpath = "//input[@data-interactive-lib-uid='7']")
    public SelenideElement PatientIDField;

    //---TOTAL PAGE-----------------------------------------------------------------------------------------------------

    @FindBy(xpath = "//div[@class='slds-grid slds-p-vertical--medium'][1]//span[@id='patientNameView']")
    public SelenideElement TotalPatientName;

    @FindBy(xpath = "//div[@class='slds-grid slds-p-vertical--medium'][2]//span[@id='patientNameView']")
    public SelenideElement TotalPatientBirthday;

    @FindBy(xpath = "//div[@class='slds-modal__content slds-p-around--medium']/div[3]/div[2]//span[@id='patientNameView']")
    public SelenideElement TotalPatientGender;

    @FindBy(id = "PhysicianNameView")
    public SelenideElement TotalPhysicianName;

    @FindBy(id = "PhysicianEmailView")
    public SelenideElement TotalPhysicianEmail;

    //---Buttons
    @FindBy(xpath = "//button[@class='slds-button slds-button--neutral' and contains(text(), 'Back')]")
    public SelenideElement TotalBackButton;

    @FindBy(xpath = "//button[contains(text(), 'Confirm and Continue to Cell Order')]")
    public SelenideElement TotalConfirmButton;

    //----Error Message Patient already registered
    @FindBy(xpath = "//div/div[2]/div/div[@class='ui-widget']/div/div[@role='dialog']//p")   //div[@class='ui-widget']/div/div[1]/div[@role='alert']/h2
    public SelenideElement PatientExistError;


    //---METHODS
    public static String getRandom(String[] array) {
        int rnd = new Random().nextInt(array.length);
        return array[rnd];
    }

    public static int randBetween(int start, int end) {
        return start + (int)Math.round(Math.random() * (end - start));
    }

    public Pages.MainPages.PatientForm fillFieldsInPatienForm() throws InterruptedException {
        System.out.println("[INFO]: Fill all fields in 'Patient Form'.");
        RN2 = System.currentTimeMillis();
        YEAR = randBetween(1900, 2015);
        MONTH = getRandom(Months);
        DAY = getRandom(Days);

        $(FirstNameField).waitUntil(enabled, 30000).setValue(FirstName);
        $(MiddleNameField).setValue(MiddleName);
        $(LastNameField).setValue(LastName + RN2);
        $(DobYearField).setValue(Integer.toString(YEAR));

        mm
        .selectFromPicklistUniversal(MonthField, MONTH)
        .selectFromPicklistUniversal(DayField, DAY);

        $(MaleGenderButton).shouldBe(enabled).click();

        mm
        .selectFromSearchField(ThreatingPhisicianField, ThreatingFirstRecord, "William Basem");

        $(AddOwnHpNoButton).waitUntil(enabled, 15000).click();
        return this;
    }

    public Pages.MainPages.PatientForm savePatientForm(){
        System.out.println("[INFO]: Save 'Patient Form'.");
        $(SubmitButton).waitUntil(enabled, 15000).click();
        $(TotalPatientName).waitUntil(visible, 30000);
        return this;
    }

    public Pages.MainPages.PatientForm checkDataOnTotalPage(){
        System.out.println("[INFO]: Check data on total page.");

        $(pf.TotalPatientName).shouldHave(text(LastName + RN2 + ", " + FirstName + " " + MiddleName));
        $(pf.TotalPatientBirthday).shouldHave(text(DAY +"-"+ MONTH +"-"+ Integer.toString(YEAR)));
        $(pf.TotalPatientGender).shouldHave(text(pf.Gender));
        $(pf.TotalPhysicianName).shouldHave(text("William, Basem"));
        return this;
    }

    public Pages.MainPages.PatientForm submitPatientForm(){
        System.out.println("[INFO]: Submit 'Patient Form'.");
        $(pf.TotalConfirmButton).waitUntil(enabled, 30000).click();
        $(pdt.NameHeader).waitUntil(visible, 30000);
        return this;
    }
}
