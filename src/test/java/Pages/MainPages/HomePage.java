package Pages.MainPages;

import BaseTests.BaseTests;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class HomePage extends BaseTests{

    public String Institution = "Test Cancer Center";
    public String User = "Test HCP";

    //---Messages

    @FindBy(xpath = "//img[@src='/resource/1497545203000/NewLogo']")
    public SelenideElement KiteLogo;

    @FindBy(xpath = "//input[@id='lookatme']")
    public SelenideElement InstitutionSearchField;

    @FindBy(xpath = "//li[@data-selindx='0']")
    public SelenideElement InstitutionFirstRecord;

    //--- Home Page

    @FindBy(xpath = "//div[@class='slds-spinner_container cCmp_SL_PatientTable']")
    public SelenideElement SpinnerIcon;

    @FindBy(xpath = "//div[@id='patientNameHeader']//b")
    public SelenideElement SpinnerHide;

    @FindBy(xpath = "//div[@id='tabPanel']")
    public SelenideElement PatientsBlock;

    //----Search Patient

    @FindBy(xpath = "//input[@id='textsearch']")
    public SelenideElement SearchField;

    @FindBy(xpath = "//div[@class='patient-card slds-container--center slds-container--x-large slds-m-vertical--medium cCmp_SL_PatientOrderProgress cCmp_SL_ActivePatients'][1]")
    public SelenideElement FirstRecord;

    @FindBy(xpath = "//div[@id='patientNameHeader']")
    public SelenideElement PatientName;

    @FindBy(xpath = "//div[@id='dobHeader']")
    public SelenideElement PatientDob;

    @FindBy(xpath = "//div[@id='physicianNameHeader']")
    public SelenideElement PatientPhysician;

    //---
    @FindBy(xpath = "//div[@id='tabPanel']/div[2]/div[3]/div[2]/div[2]/div/p")
    public SelenideElement ApheresisDate;

    @FindBy(xpath = "//div[@id='tabPanel']/div[2]/div[3]/div[2]/div[3]/div/p")
    public SelenideElement ApheresisName;

    @FindBy(xpath = "//div[@id='tabPanel']/div[2]/div[3]/div[4]/div[2]/div/p")
    public SelenideElement FinalProductReadyDate;

    @FindBy(xpath = "//div[@id='tabPanel']/div[2]/div[3]/div[4]/div[3]/div/p")
    public SelenideElement FinalProductReadyName;

    //---Center

    @FindBy(xpath = "//span[contains(text(), 'Enroll New Patient')]")
    public SelenideElement EnrollNewPatientButton;

    //---Tabs

    @FindBy(xpath = "//a[contains (text(), 'Contacts')]")
    public SelenideElement ContactsTab;

    @FindBy(xpath = "//a[contains (text(), 'Patients')]")
    public SelenideElement PatientsTab;

    @FindBy(xpath = "//div[@class='slds-container--center slds-container--x-large']//a[contains (text(), 'Medical Information')]")
    public SelenideElement MedicalInformationTab;

    @FindBy(xpath = "//div[@class='slds-container--center slds-container--x-large']//a[contains (text(), 'Adverse Event')]")
    public SelenideElement AdverseEventTab;

    @FindBy(xpath = "//div[@class='slds-container--center slds-container--x-large']//a[contains (text(), 'Product Complaint')]")
    public SelenideElement ProductComplaintTab;

    @FindBy(xpath = "//div[@class='slds-container--center slds-container--x-large']//a[contains (text(), 'Request Assistance')]")
    public SelenideElement RequestAssistanceTab;

    @FindBy(xpath = "//h2[@class='slds-text-heading--medium' and contains (text(), 'Medical Information')]")
    public SelenideElement MedicalInfoWindow;

    @FindBy(xpath = "//div[@class='ui-widget']/section[1]//h3[1]")
    public SelenideElement MedicalInfoMessage1;

    @FindBy(xpath = "//div[@class='ui-widget']/section[1]//h3[2]")
    public SelenideElement MedicalInfoMessage2;

    @FindBy(xpath = "//h2[@class='slds-text-heading--medium' and contains (text(), 'Adverse Event')]")
    public SelenideElement AdverseEventWindow;

    @FindBy(xpath = "//div[@class='ui-widget']/section[2]//h3[1]")
    public SelenideElement AdverseEventMessage1;

    @FindBy(xpath = "//div[@class='ui-widget']/section[2]//h3[2]")
    public SelenideElement AdverseEventMessage2;

    @FindBy(xpath = "//h2[@class='slds-text-heading--medium' and contains (text(), 'Product Complaint')]")
    public SelenideElement ProductComplaintWindow;

    @FindBy(xpath = "//div[@class='ui-widget']/section[3]//h3[1]")
    public SelenideElement ProductComplaintMessage1;

    @FindBy(xpath = "//div[@class='ui-widget']/section[3]//h3[2]")
    public SelenideElement ProductComplaintMessage2;

    //--- Patient Info
    @FindBy(xpath = "//button[@class='slds-button slds-button--neutral cCmp_SL_RequestAssistance']")
    public SelenideElement RequestAssistanceButton;

    @FindBy(xpath = "//h2[@id='assistanceHeader' and contains (text(), 'Request Assistance')]")
    public SelenideElement RequestAssistanceWindow;

    @FindBy(xpath = "//div[@class='slds-grid slds-p-vertical--x-small cCmp_SL_PatientAssistance_View']")
    public SelenideElement SelectedRequestName;

    //---- Sign In button
    @FindBy(xpath = "//a[@href='./login' and contains (text(), 'Sign In')]")
    public SelenideElement RightSignInButton;


    //---Calendar Locators
    @FindBy (xpath = "//button[@class='slds-button slds-button_icon-container']")
    public SelenideElement OpenDateTypes;

    @FindBy (xpath = "//li[@role='presentation']//*[contains (text(), 'Leukapheresis Date')]")
    public SelenideElement LeukapheresisDateType;

    @FindBy (xpath = "//li[@role='presentation']//*[contains (text(), 'Estimated Final Product Ready Date')]")
    public SelenideElement EstimatedFinalProductReadyDateType;

    @FindBy (xpath = "//div[@class='slds-form-element ']//input")
    public SelenideElement SearchByDateField;

    @FindBy (xpath = "//input[@placeholder='Search by Estimated Final Product Ready Date...']")
    public SelenideElement EstimatedFinalProductReadyDateField;

    @FindBy (xpath = "//input[@placeholder='Search by Leukapheresis Date...']")
    public SelenideElement LeukapheresisDateField;

    @FindBy (css = ".slds-datepicker.slds-dropdown.slds-dropdown")
    public SelenideElement Calendar;

    @FindBy (xpath = "//select[@class='select uiInput uiInputSelect uiInput--default uiInput--select']")
    public SelenideElement YearField;

    @FindBy (xpath = "//option[@value='2025']")
    public SelenideElement SelectYear2025;

    @FindBy (xpath = "//div[@class='slds-datepicker__filter--month slds-grid slds-grid--align-spread slds-grow']")
    public SelenideElement MonthField;

    @FindBy (xpath = "//h2[@class='slds-align-middle' and @id='month']")
    public SelenideElement MonthName;

    @FindBy (xpath = "//div[@class='slds-align-middle'][1]/a[@class='slds-button slds-button--icon-container']")
    public SelenideElement PrevMonthButton;

    @FindBy (xpath = "//div[@class='slds-align-middle'][2]/a[@class='slds-button slds-button--icon-container']")
    public SelenideElement NextMonthButton;

    @FindBy (xpath = "//a[@class='slds-show--inline-block slds-p-bottom--x-small' and contains (text(), 'Today')]")
    public SelenideElement TodayButton;

    @FindBy (xpath = "//a[@class='slds-show--inline-block slds-p-bottom--x-small' and contains (text(), 'Clear')]")
    public SelenideElement ClearButton;

    @FindBy (xpath = "//a[@class='slds-show--inline-block slds-p-bottom--x-small' and contains (text(), 'Close')]")
    public SelenideElement CloseButton;

    @FindBy (xpath = "//span[@class='slds-day' and contains (text(), '23')]")
    public SelenideElement SelectDate23;

    @FindBy (xpath = "//input[@placeholder='Select Start Date']")
    public SelenideElement StartDateField;

    @FindBy (xpath = "//input[@placeholder='Select End Date']")
    public SelenideElement EndDateField;

    //---Methods
    public HomePage navigateToPatientForm() throws InterruptedException {
        System.out.println("[INFO]: Navigate to 'Patient Form'.");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        $(hp.EnrollNewPatientButton).waitUntil(enabled, 60000).click();

        Thread.sleep(5000);
        try{if(RightSignInButton.isDisplayed()){
            $(RightSignInButton).waitUntil(enabled, 60000).click();
            $(lp.LoginField).waitUntil(enabled, 30000).setValue("fred@flintstone.com");
            $(lp.PasswordField).shouldBe(enabled).setValue("TestingP3");
            $(lp.LoginButton).shouldBe(enabled).click();
            $(hp.EnrollNewPatientButton).waitUntil(enabled, 60000);
            System.out.println("[INFO]: In this test was LOGOUT moment.");
            navigateToPatientForm();
        }} catch (Exception e){}
        $(By.xpath("//input[@data-interactive-lib-uid='2']")).waitUntil(enabled, 240000);
        return this;
    }

    public HomePage navigateToHomePage() throws InterruptedException {
        System.out.println("[INFO]: Navigate to 'Home Page'.");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        Thread.sleep(5000);
        try{if(RightSignInButton.isDisplayed()){
            $(RightSignInButton).waitUntil(enabled, 120000).click();
            $(lp.LoginField).waitUntil(enabled, 30000).setValue(lp.TestLogin2);
            $(lp.PasswordField).shouldBe(enabled).setValue(lp.TestPassword2);
            $(lp.LoginButton).shouldBe(enabled).click();
            System.out.println("[INFO]: In this test was LOGOUT moment.");
            navigateToHomePage();
        }} catch (Exception e){}
        //$(SpinnerHide).waitUntil(enabled, 300000);
        $(hp.EnrollNewPatientButton).waitUntil(enabled, 120000);
        return this;
    }

    public HomePage findPatientBySearchField(){
        $(SpinnerHide).waitUntil(enabled, 120000); //--------------------!!!!!!!!!!!!!!!!!
        System.out.println("[INFO]: Find Patient by 'Search' field.");
        $(SearchField).waitUntil(enabled, 60000).setValue(""+pf.RN2);
        $$("patient-card slds-container--center slds-container--x-large slds-m-vertical--medium cCmp_SL_PatientOrderProgress cCmp_SL_ActivePatients").shouldHaveSize(0);
        return this;
    }

    public HomePage findPatientByName2(String name){
        System.out.println("[INFO]: Find Patient by 'Search' field.");
        $(SearchField).waitUntil(enabled, 20000).setValue(name);
        $$("patient-card slds-container--center slds-container--x-large slds-m-vertical--medium cCmp_SL_PatientOrderProgress cCmp_SL_ActivePatients").shouldHaveSize(0);
        return this;
    }

    public HomePage openFirstPatient(){
        System.out.println("[INFO]: Open Patient page.");
        $$("patient-card slds-container--center slds-container--x-large slds-m-vertical--medium cCmp_SL_PatientOrderProgress cCmp_SL_ActivePatients").shouldHaveSize(0);
        $(PatientName).click();
        $(pcit.ContactInfoContinueButton).waitUntil(enabled, 30000).click();
        return this;
    }

    public HomePage checkDataOfFirstContact(){
        System.out.println("[INFO]: Check data in Contact on 'Home' page.");
        $(PatientName).shouldHave(text(pf.LastName + pf.RN2 + ", Test super"));
        $(PatientDob).shouldHave(text(pf.DAY +"-"+ pf.MONTH +"-"+ Integer.toString(pf.YEAR)));
        $(PatientPhysician).shouldHave(text("William, Basem"));
        $(ApheresisDate).shouldHave(text(cop.TotalSchedulingDate.substring(0, cop.TotalSchedulingDate.length() - 6)));
        $(ApheresisName).shouldHave(text("(TST) Seattle Cancer Care Alliance"));
        $(FinalProductReadyDate).shouldHave(text(cop.TotalDeliveryDate.substring(0, cop.TotalSchedulingDate.length() - 6)));
        $(FinalProductReadyName).shouldHave(text("(TST) Seattle Cancer Care Alliance"));
        return this;
    }

    public HomePage openPatientInformation(){
        System.out.println("[INFO]: Open Patient information.");
        $(PatientName).waitUntil(enabled, 30000).click();
        $(RequestAssistanceButton).shouldBe(visible);
        return this;
    }

    public HomePage checkRequestInformation(String requestName){
        System.out.println("[INFO]: Check Request information.");
        $(RequestAssistanceButton).waitUntil(enabled, 30000).click();
        $(RequestAssistanceWindow).waitUntil(enabled, 30000);
        $(SelectedRequestName).shouldHave(text(requestName));
        return this;
    }

    //---Calendar Methods
    public HomePage selectMonth(String month){
        try {
            if (hp.MonthName.getText().equals(month)) {
            } else {
                $(hp.NextMonthButton).waitUntil(enabled, 30000).click();
                selectMonth(month);
            }
        } catch (Exception e){}
        return this;
    }

    public HomePage selectDate(String date){
        $(By.xpath("//span[@class='slds-day' and contains (text(), '"+ date +"')]")).waitUntil(enabled, 30000).click();
        return this;
    }

    public HomePage selectDate2(String date){
        $(By.xpath("//span[contains (text(), '"+ date +"')]")).waitUntil(enabled, 30000).doubleClick();
        return this;
    }

    public HomePage selectYear(String year){
    $(hp.YearField).waitUntil(enabled, 30000).click();
    $(By.xpath(("//option[@value='"+ year +"']"))).waitUntil(enabled, 30000).click();
        return this;
    }
}
