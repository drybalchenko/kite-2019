package Pages.MedicalInformation;

import BaseTests.BaseTests;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class MedicalInformation extends BaseTests{

    //---LOCATORS
    //---Assistance Tab
    @FindBy(xpath = "//label[@for='Benefit Investigation']/span[1]")
    public SelenideElement BenefitInvestigationAssistance;

    @FindBy(xpath = "//label[@for='Prior Authorization']/span[1]")
    public SelenideElement PriorAuthorizationAssistance;

    @FindBy(xpath = "//label[@for='Appeals Request']/span[1]")
    public SelenideElement AppealsRequestAssistance;

    @FindBy(xpath = "//input[@accept='pdf/png']")
    public SelenideElement AppealsRequestSelectFile;

    @FindBy(xpath = "//div[@id='tabPanel']/div/div[5]/div[5]/div[1]/div[4]/div/div/div[@role='dialog']//div[@class='slds-modal__content slds-p-around--small']")
    public SelenideElement AppealsRequestUploadedWindow;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_MedicalInformation']/div[5]/div[5]/div[1]/div[4]/div/div/div[@role='dialog']//button[@class='slds-button slds-button--neutral']")
    public SelenideElement UploadedWindowCloseButton;

    @FindBy(xpath = "//label[@for='Uninsured Patient']/span[1]")
    public SelenideElement UninsuredPatientAssistance;

    @FindBy(xpath = "//label[@for='Underinsured Patient']/span[1]")
    public SelenideElement UnderinsuredPatientAssistance;

    @FindBy(xpath = "//label[@for='Travel and Lodging']/span[1]")
    public SelenideElement TravelAndLodgingAssistance;

    @FindBy(xpath = "//label[@for='General Inquiry']/span[1]")
    public SelenideElement GeneralInquiryAssistance;

    //---Assistance with Attachment
    @FindBy(xpath = "//span[@class='slds-file-selector__button slds-button slds-button_neutral']")
    public SelenideElement AppealsAddAttachmentButton;

    //---Diagnosis Tab
    @FindBy(xpath = "//label[@for='IndicationRadio0']/span[@class='slds-radio--faux']")
    public SelenideElement FirstDiagnosis;

    @FindBy(xpath = "//label[@for='IndicationRadio1']/span[@class='slds-radio--faux']")
    public SelenideElement SecondDiagnosis;

    @FindBy(xpath = "//label[@for='IndicationRadio2']/span[@class='slds-radio--faux']")
    public SelenideElement ThirdDiagnosis;

    @FindBy(xpath = "//label[@for='IndicationRadio3']/span[@class='slds-radio--faux']")
    public SelenideElement FourthDiagnosis;

    @FindBy(xpath = "//label[@for='IndicationRadio4']/span[@class='slds-radio--faux']")
    public SelenideElement OtherDiagnosis;

    //---
    @FindBy(xpath = "//div[2]/div[2]/div/span")
    public SelenideElement SelectedDiagnosis;

    @FindBy(xpath = "//ul[@role='tablist']/li[2]/a[@role='tab']/span[@class='slds-tabs--path__stage']")
    public SelenideElement MedicalInfoTab;

    @FindBy(xpath = "//div[@class='cCmp_SL_MedicalInformation']/div[2]/div[3]/div//input[@type='text']")
    public SelenideElement AddDiagnosisField;

    //---Insurance Tab
    @FindBy(xpath = "//span[contains (text(), 'Upload Insurance Card')]")
    public SelenideElement UploadInsuranceCard;

    @FindBy(xpath = "//span[contains (text(), 'Manually Enter')]")
    public SelenideElement ManuallyEnter;

    @FindBy(xpath = "//input[@type='file")
    public SelenideElement UploadInsuranceField;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_MedicalInformation']/div[8]/div[3]/div/div[@role='dialog']//div[@class='slds-modal__content slds-p-around--small']")
    public SelenideElement PIUploadedWindow;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_MedicalInformation']/div[8]/div[3]/div/div[@role='dialog']//button[@class='slds-button slds-button--neutral']")
    public SelenideElement PIUploadedWindowCloseButton;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_MedicalInformation']/div[8]/div[1]/div//span[.='Please select a method to add insurance information']")
    public SelenideElement PIErrorMessage;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_MedicalInformation']/div[8]/div[1]/div//span[.='You must upload an insurance card or enter information manually to proceed.']")
    public SelenideElement PIUploadErrorMessage;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[1]/div//select")
    public SelenideElement PIType;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[1]/div//span[.='This field is required']")
    public SelenideElement PITypeError;

    @FindBy(xpath = "//input[@id='lookatme']")
    public SelenideElement PIName;

    @FindBy(xpath = "//div[@id='tabPanel']/div[@class='cCmp_SL_MedicalInformation']//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[2]/div//span[.='This field is required']")
    public SelenideElement PINameError;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[3]/div//input[@type='text']")
    public SelenideElement PIPhoneNumber;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[3]/div//span[.='This field is required']")
    public SelenideElement PIPhoneNumberError;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[5]/div[1]//input[@type='text']")
    public SelenideElement PISubscriberName;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[5]/div[1]//span[.='This field is required']")
    public SelenideElement PISubscriberNameError;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[5]/div[2]//input[@type='text']")
    public SelenideElement PIDob;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[5]/div[2]//span[.='Invalid year']")
    public SelenideElement PIDobError;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[5]/div[3]//select[@class='slds-select']")
    public SelenideElement PIMonth;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[5]/div[3]//span[.='Invalid month']")
    public SelenideElement PIMonthError;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[5]/div[4]//select[@class='slds-select']")
    public SelenideElement PIDay;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[5]/div[4]//span[.='Invalid day']")
    public SelenideElement PIDayError;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[5]/div[4]//span[.='DOB must be in the past']")
    public SelenideElement PIDayErrorForYear;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[6]/div//input[@type='text']")
    public SelenideElement PIRelationship;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[7]/div[1]//input[@type='text']")
    public SelenideElement PISubscriberId;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[7]/div[1]//span[.='This field is required']")
    public SelenideElement PISubscriberIdError;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[7]/div[2]//input[@type='text']")
    public SelenideElement PIGroupNumber;

    @FindBy(xpath = "//div[@class='cCmp_SL_InsuranceInput']/div[@class='']/div[8]/div//input[@type='text']")
    public SelenideElement PIEmployer;

    //---Buttons

    @FindBy(xpath = "//*[contains(text(), 'Save & Exit')]")
    public SelenideElement SaveAndExitButton;

    @FindBy(xpath = "//*[contains(text(), 'Save & Next')]")
    public SelenideElement SaveAndNextButton;

    @FindBy(xpath = "//*[contains(text(), 'Continue to Schedule Treatment')]")
    public SelenideElement ContinueToScheduleButton;

    //---METHODS

    public MedicalInformation selectAssistance(){
        $(PriorAuthorizationAssistance).waitUntil(enabled, 15000).click();
        return this;
    }

    public MedicalInformation openInsuranceForm(){
        System.out.println("[INFO]: Open Insurance Form.");
        $(ManuallyEnter).waitUntil(enabled, 60000).click();
        $(PIType).waitUntil(enabled, 60000);
        return this;
    }

    public MedicalInformation checkFieldsInInsuranceForm(){
        System.out.println("[INFO]: Check Fields In Insurance Form.");
        $(PIType).shouldBe(visible).shouldBe(enabled);
        $(PIName).shouldBe(visible).shouldBe(enabled);
        $(PIPhoneNumber).shouldBe(visible).shouldBe(enabled);
        $(PIDob).shouldBe(visible).shouldBe(enabled);
        $(PIMonth).shouldBe(visible).shouldBe(enabled);
        $(PIDay).shouldBe(visible);
        $(PIRelationship).shouldBe(visible).shouldBe(enabled);
        $(PISubscriberId).shouldBe(visible).shouldBe(enabled);
        $(PIGroupNumber).shouldBe(visible).shouldBe(enabled);
        $(PIEmployer).shouldBe(visible).shouldBe(enabled);
        return this;
    }

    public MedicalInformation checkErrorMessagesEmptyInsuranceForm(){
        System.out.println("[INFO]: Check Error Messages for required fields in Insurance Form.");
        $(PITypeError).waitUntil(visible, 60000);
        $(PINameError).should(exist).shouldBe(visible);
        $(PIPhoneNumberError).should(exist).shouldBe(visible);
        $(PISubscriberNameError).should(exist).shouldBe(visible);
        $(PIDobError).should(exist).shouldBe(visible);
        $(PIMonthError).should(exist).shouldBe(visible);
        $(PIDayError).should(exist).shouldBe(visible);
        $(PISubscriberIdError).should(exist).shouldBe(visible);
        return this;
    }

    public MedicalInformation fillRequiredFieldsInInsuranceForm() throws InterruptedException {
        System.out.println("[INFO]: Fill Required Fields In Insurance Form.");
        mm.selectFromPicklistUniversal(PIType, "Commercial");
        mm.selectFromSearchField(PIName, pf.ThreatingFirstRecord ,"Test Payer");
        Thread.sleep(3000);
        $(PIPhoneNumber).shouldHave(value("(888) 345-4567"));
        $(PISubscriberName).sendKeys("TestSubscriber");
        $(PIDob).sendKeys(pf.DobYear);
        mm.selectFromPicklistUniversal(PIMonth, pf.MONTH);
        mm.selectFromPicklistUniversal(PIDay, pf.DAY);
        $(PISubscriberId).sendKeys("0123456789");
        return this;
    }

    public MedicalInformation selectDiagnosis(SelenideElement diagnosis){
        System.out.println("[INFO]: Select Diagnosis.");
        $(diagnosis).waitUntil(enabled, 15000).click();
        return this;
    }

    public MedicalInformation diagnosisClickSaveAndNext(){
        System.out.println("[INFO]: Navigate to Assistance Tab.");
        mm.navigateToNextPage(mip.SaveAndNextButton, mip.BenefitInvestigationAssistance);
        return this;
    }

    public MedicalInformation diagnosisClickSaveAndExit(){
        System.out.println("[INFO]: Navigate to Assistance Tab.");
        mm.navigateToNextPage(mip.SaveAndExitButton, hp.SearchField);
        return this;
    }

    public MedicalInformation fillRequiredMedicalInfo() {
        System.out.println("[INFO]: Fill required fields on Medical Info Page.");
        selectDiagnosis(SecondDiagnosis);
        System.out.println("[INFO]: Navigate to Assistance Tab.");
        mm.navigateToNextPage(mip.SaveAndNextButton, mip.BenefitInvestigationAssistance);
        return this;
    }

    public MedicalInformation selectPatientAssistance(WebElement assistance) {
        System.out.println("[INFO]: Select Assistance.");
        $(assistance).waitUntil(enabled, 60000).click();
        return this;
    }

    public MedicalInformation checkSelectedDiagnosis(String diagnosis) {
        System.out.println("[INFO]: Check selected Diagnosis - '" + diagnosis + "'.");
        $(SelectedDiagnosis).waitUntil(visible, 60000).shouldHave(text(diagnosis));
        return this;
    }

    public MedicalInformation openMedicalInfo() {
        System.out.println("[INFO]: Open Medical Information Tab.");
        $(MedicalInfoTab).waitUntil(exist, 60000).click();
        return this;
    }

    public MedicalInformation addDiagnosis() {
        System.out.println("[INFO]: Add Diagnosis field.");
        $(AddDiagnosisField).waitUntil(enabled, 60000).sendKeys("Test");
        return this;
    }

    public MedicalInformation closeUploadInfoWindow(WebElement message, WebElement button) {
        System.out.println("[INFO]: File uploaded successfully.");
        System.out.println("[INFO]: Close Upload Window.");
        $(message).waitUntil(visible, 60000).shouldHave(text("Your file 'testPdfFile.pdf' successfully uploaded."));
        $(button).waitUntil(enabled, 60000).click();
        return this;
    }
    public MedicalInformation closeUploadInfoWindow2(WebElement message, WebElement button) {
        System.out.println("[INFO]: File uploaded successfully.");
        System.out.println("[INFO]: Close Upload Window.");
        $(AppealsRequestUploadedWindow).waitUntil(visible, 60000).shouldHave(text("Your file 'testPdfFile.pdf' successfully uploaded."));
        $(UploadedWindowCloseButton).waitUntil(enabled, 60000).click();
        return this;
    }
}
