package VisualTesting;

import com.applitools.eyes.selenium.Eyes;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class Applitool {

/*
    public static void getScreenshot(String pageName) {

        Eyes eyes = new Eyes();

        eyes.setApiKey("nuaxivuuwircL0H3y3FtOM0CGG0SN3WHQGdGhEDmdlQ110");

        try {
              //pageName
            eyes.open(getWebDriver(), "Kite", "Kite2019");

            eyes.checkWindow(pageName);

            eyes.close();

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            eyes.abortIfNotClosed();
        }
    }
*/

    static Eyes eyes = new Eyes();

    public static void EyesStart(String appName, String testName) {
        eyes.setApiKey("nuaxivuuwircL0H3y3FtOM0CGG0SN3WHQGdGhEDmdlQ110");
        eyes.open(getWebDriver(), appName, testName);
    }

    public static void getScreenshot(String pageName) {
            eyes.checkWindow(pageName);
    }

    public static void EyesClose() {
        try {
            eyes.close();
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally{
            eyes.abortIfNotClosed();
        }
    }
}
