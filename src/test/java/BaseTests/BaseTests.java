package BaseTests;

import Pages.MainPages.*;
import Pages.PatientPage.*;
import TestAPI.SalesforceSoap;
import Helpers.BrowsersHelper;
import MainMethods.MainMethods;
import VisualTesting.Applitool;
import Pages.ScheduleTreatment.*;
import Pages.Salesforce.SalesfrcePO;
import TestSuites.RegressionTestSuite;
import Pages.ConfirmOrder.ConfirmOrder;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import static com.codeborne.selenide.Selenide.*;
import Pages.Salesforce.OrderManufacturingStatus;
import Pages.MedicalInformation.MedicalInformation;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;

public class BaseTests {

    //public static WebDriver driver;
    public static LoginPage lp;
    public static MainMethods mm;
    public static HomePage hp;
    public static PatientForm pf;
    public static MedicalInformation mip;
    private static RegressionTestSuite rts;
    protected static DemographicsTab pdt;
    protected static ContactInfoTab pcit;
    protected static PatientTeamTab ptt;
    protected static PatientNoticeTab pnt;
    protected static ScheduleTreatment stp;
    protected static SchedulingHelper shp;
    protected static ConfirmOrder cop;
    protected static ContactsPage cp;
    protected static SalesfrcePO spo;
    protected static OrderManufacturingStatus oms;
    protected static AcknowledgementTab at;
    protected static Applitool ap;

    @BeforeAll
    public static void setup() {
        //Configuration.browser = "chrome";
        //setWebDriver(BrowsersHelper.getChromeLocalWebDriver());
        //setWebDriver(BrowsersHelper.getChromeRemoteWebDriver());
        //setWebDriver(BrowsersHelper.getFirefoxRemoteWebDriver());
        //setWebDriver(BrowsersHelper.getIE11RemoteWebDriver());
        //initializePages();
        //open("https://full-kitekonnect.cs96.force.com");
        //lp.loginToApplication(lp.TestLogin2, lp.TestPassword2);

        //Properties properties = new Properties();

        String br = System.getProperty("browser");
        if(br == null || "".equals(br)) br = "chrome";
        Configuration.timeout = 30000;
        WebDriver wd;
        if (br.toLowerCase().equals("chrome")){
            wd = BrowsersHelper.getChromeRemoteWebDriver();
        } else {
            if (br.toLowerCase().equals("firefox")){
                wd = BrowsersHelper.getFirefoxRemoteWebDriver();
            } else {
                wd = BrowsersHelper.getEdgeRemoteWebDriver();
            }
        }
        System.out.println(wd);
        WebDriverRunner.setWebDriver(wd);

        SalesforceSoap.main();
        initializePages();
    }

    public static void initializePages() {
        lp = page (LoginPage.class);
        mm = page (MainMethods.class);
        hp = page (HomePage.class);
        pf = page (PatientForm.class);
        pdt = page (DemographicsTab.class);
        pcit = page (ContactInfoTab.class);
        ptt = page (PatientTeamTab.class);
        pnt = page (PatientNoticeTab.class);
        mip = page (MedicalInformation.class);
        stp = page (ScheduleTreatment.class);
        shp = page (SchedulingHelper.class);
        cop = page (ConfirmOrder.class);
        cp = page (ContactsPage.class);
        spo = page (SalesfrcePO.class);
        rts = page (RegressionTestSuite.class);
        oms = page (OrderManufacturingStatus.class);
        at = page (AcknowledgementTab.class);
        ap = page (Applitool.class);
    }

    @AfterAll
    public static void end(){
        WebDriverRunner.getWebDriver().quit();
        SalesforceSoap.main();
    }
}
