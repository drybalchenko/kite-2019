package TestAPI;

import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.soap.enterprise.GetUserInfoResult;
import com.sforce.soap.enterprise.QueryResult;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

//--- MAIN Command- java -classpath .\force-wsc-44.0.0.jar;.\ST-4.1.jar;.\antlr-runtime-3.1.3.jar com.sforce.ws.tools.wsdlc .\enterprise.xml .\enterprise.jar

public class SalesforceSoap {
    private String query;

    EnterpriseConnection connection;
    public static  String authEndPoint = "https://test.salesforce.com/services/Soap/c/41";
    public static  String login = "kite@silverlinecrm.com.full";
    public static  String password = "$ummerTwo108";

    public static void main() {
        SalesforceSoap sample = new SalesforceSoap(authEndPoint, login, password);
        sample.deleteTestData();
    }

    public void deleteTestData() {
        // Make a login call
        if (login()) {
            // Do a describe global
            System.out.println("[INFO]: Delete test data - Related Cases.");
            deleteRelatedCases("Test User");
            deleteRelatedCases("Edit Fox");

            System.out.println("[INFO]: Delete test data - Patients.");
            deleteTestPatients("Test User");
            deleteTestPatients("Edit Fox");

            System.out.println("[INFO]: Delete test data - Related Orders.");
            deleteRelatedOrders("Test User");
            deleteRelatedOrders("Edit Fox");

            // Log out
            logout();
            System.out.println("[INFO]: Test data successfully deleted from Salesforce ✔.");
        }
    }

    // Constructor
    public SalesforceSoap(String authEndPoint, String login, String password) {

        this.authEndPoint = authEndPoint;
        this.login = login;
        this.password = password;
    }

    private boolean login() {
        boolean success = false;
        String username = this.login;
        String password = this.password;

        try {
            ConnectorConfig config = new ConnectorConfig();
            config.setUsername(username);
            config.setPassword(password);

            System.out.println("AuthEndPoint: " + authEndPoint);
            config.setAuthEndpoint(authEndPoint);

            connection = new EnterpriseConnection(config);
            printUserInfo(config);

            success = true;
        } catch (ConnectionException ce) {
            ce.printStackTrace();
        }
        return success;
    }

    private void printUserInfo(ConnectorConfig config) {
        try {
            GetUserInfoResult userInfo = connection.getUserInfo();
            System.out.println("\nLogging in ...\n");
            System.out.println("UserID: " + userInfo.getUserId());
            System.out.println("User Full Name: " + userInfo.getUserFullName());
            System.out.println("User Email: " + userInfo.getUserEmail());
            System.out.println("SessionID: " + config.getSessionId());
            System.out.println("Auth End Point: " + config.getAuthEndpoint());
            System.out.println("Service End Point: " + config.getServiceEndpoint());
            System.out.println();
        } catch (ConnectionException ce) {
            ce.printStackTrace();
        }
    }

    private void logout() {
        try {
            connection.logout();
            System.out.println("Logged out.");
        } catch (ConnectionException ce) {
            ce.printStackTrace();
        }
    }

    //---!!! !!! !!!
    private void deleteTestPatients(String patientName) {
        try {
            QueryResult q = connection.query("SELECT id FROM Patient__c WHERE Name like '"+ patientName +"%' LIMIT 200" ); //, Test User
            //List<String> supplierNames1 = new ArrayList<String>();
            String[] patients = new String[q.getRecords().length];
            for (int i = 0; i < q.getRecords().length; i++) {
                patients[i] = q.getRecords()[i].getId();
            }
            System.out.println(patients);
            connection.delete(patients);
            System.out.println("[NOTE]:"+ q.getRecords().length +" Test Patients have been removed.");

        } catch (ConnectionException ce) {
            ce.printStackTrace();
        }
    }

    private void deleteRelatedCases(String patientName) {
        try {
            QueryResult q = connection.query("SELECT id FROM Case__c WHERE Patient_Name__c like '"+ patientName +"%' LIMIT 200" ); //, Test User
            //List<String> supplierNames1 = new ArrayList<String>();
            String[] patients = new String[q.getRecords().length];
            for (int i = 0; i < q.getRecords().length; i++) {
                patients[i] = q.getRecords()[i].getId();
            }
            System.out.println(patients);
            connection.delete(patients);
            System.out.println("[NOTE]:"+ q.getRecords().length +" Cases have been removed.");

        } catch (ConnectionException ce) {
            ce.printStackTrace();
        }
    }

    private void deleteRelatedOrders(String patientName) {
        try {
            QueryResult q = connection.query("SELECT id FROM Order WHERE Patient_Name__c like '"+ patientName +"%' LIMIT 200" ); //, Test User
            //List<String> supplierNames1 = new ArrayList<String>();
            String[] patients = new String[q.getRecords().length];
            for (int i = 0; i < q.getRecords().length; i++) {
                patients[i] = q.getRecords()[i].getId();
            }
            System.out.println(patients);
            connection.delete(patients);
            System.out.println("[NOTE]:"+ q.getRecords().length +" Orders have been removed.");

        } catch (ConnectionException ce) {
            ce.printStackTrace();
        }
    }
}
