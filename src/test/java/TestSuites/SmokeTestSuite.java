package TestSuites;

import Helpers.BrowsersHelper;
import TestAPI.SalesforceSoap;
import Tests.*;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.openqa.selenium.WebDriver;

import java.io.FileInputStream;
import java.util.Properties;

import static BaseTests.BaseTests.initializePages;
import static BaseTests.BaseTests.lp;
import static com.codeborne.selenide.Selenide.open;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CheckLoginPage.class,
        CheckHomePageTabs.class,
        HappyPathTests.class,
        TestsForContactsPage.class
})

public class SmokeTestSuite {
    @BeforeClass
    public static void cleanTestDataBeforeTests() {
        SalesforceSoap.main();
/*
        Properties properties = new Properties();
        //properties.load(new FileInputStream("application.properties"));
        String br = System.getProperty("browser");
        if(br == "" || br == null) br = "Chrome";
        Configuration.timeout = 30000;
        WebDriver wd;
        if (br.toLowerCase().equals("Chrome")){
            wd = BrowsersHelper.getChromeRemoteWebDriver();
        } else {
            if (br.toLowerCase().equals("FireFox")){
                wd = BrowsersHelper.getFirefoxRemoteWebDriver();
            } else {
                wd = BrowsersHelper.getIE11RemoteWebDriver();
            }
        }
        System.out.println(wd);
        WebDriverRunner.setWebDriver(wd);

        initializePages();
        open("https://full-kitekonnect.cs96.force.com");
        lp.loginToApplication(lp.TestLogin2, lp.TestPassword2);
        */
    }

    @AfterClass
    public static void cleanTestDataAfterTests() {
        SalesforceSoap.main();
        //WebDriverRunner.getWebDriver().quit();
    }
}
