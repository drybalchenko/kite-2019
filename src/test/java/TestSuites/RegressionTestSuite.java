package TestSuites;

import TestAPI.SalesforceSoap;
import Tests.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
//import org.junit.platform.runner.JUnitPlatform;

//@RunWith(JUnitPlatform.class)
@Suite.SuiteClasses({
        HappyPathTests.class,
        CheckHomePageTabs.class,
        CheckRequiredFieldsPatientForm.class,
        CreateNewPatient.class,
        TestsForContactsPage.class,
        CheckLoginPage.class,
        CheckAssistanceRequests.class,
        CheckManufacturingStatuses.class,
        CheckAttachments.class,
        CheckDiagnosis.class,
        CheckInsuranceInfoForm.class
})

public class RegressionTestSuite{
    @BeforeClass
    public static void cleanTestDataBeforeTests() {
        SalesforceSoap.main();
    }

    @AfterClass
    public static void cleanTestDataAfterTests() {
        SalesforceSoap.main();
    }
}
