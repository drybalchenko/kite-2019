package TestSuites;

import TestAPI.SalesforceSoap;
import Tests.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CheckRequiredFieldsPatientForm.class,
        CreateNewPatient.class,
        CheckInsuranceInfoForm.class
})

public class TestSuite2 {
    @BeforeClass
    public static void cleanTestDataBeforeTests() {
        SalesforceSoap.main();
    }

    @AfterClass
    public static void cleanTestDataAfterTests() {
        SalesforceSoap.main();
    }
}
