package MainMethods;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

import java.util.concurrent.TimeUnit;

public class MainMethods {

    //---METHODS

    public MainMethods selectFromPicklistUniversal(WebElement field, String value){

        $(field)
                .click();

        $(By.xpath("//option[@value='" + value + "']"))
                .waitUntil(exist, 90000)
                .click();

        return this;
    }

    public MainMethods selectFromSearchField(WebElement field, WebElement firstValue, String value) throws InterruptedException {
        Thread.sleep(5000);
        $(field).waitUntil(enabled, 50000).clear();
        $(field).setValue(value);
        Thread.sleep(3000);
        $(firstValue).waitUntil(enabled, 60000).click();
        return this;
    }

    public MainMethods navigateToNextPage(WebElement button, WebElement elementOnPage){
        $(button).waitUntil(enabled, 25000).click();
        $(elementOnPage).waitUntil(present, 20000);
        return this;
    }
}
