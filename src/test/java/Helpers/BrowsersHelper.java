package Helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class BrowsersHelper {
    private static String remoteURL = "http://marina67:rDnzuyq1edYevNDxbgNL@hub.browserstack.com/wd/hub";

    public static WebDriver getIE11RemoteWebDriver () {
        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability("browser", "IE");
        capability.setCapability("version", "11");
        capability.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
        capability.setCapability("os", "Windows");
        capability.setCapability("os_version", "10");
        capability.setCapability("build", "Kite");
        capability.setCapability("acceptSslCerts", "true");
        capability.setCapability("browserstack.debug", "true");
        capability.setCapability("resolution", "1280x1024");
        capability.setCapability("browserstack.video", "false");
        RemoteWebDriver driver = null;
        try {
            driver = new RemoteWebDriver(
                    new URL(remoteURL),	capability);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.setFileDetector(new LocalFileDetector());
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getEdgeRemoteWebDriver () {
        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability("browser", "edge");
        capability.setCapability("version", "15");
        capability.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
        capability.setCapability("os", "Windows");
        capability.setCapability("os_version", "10");
        capability.setCapability("build", "Kite");
        capability.setCapability("acceptSslCerts", "true");
        capability.setCapability("browserstack.debug", "true");
        capability.setCapability("resolution", "1280x1024");
        capability.setCapability("browserstack.video", "false");
        RemoteWebDriver driver = null;
        try {
            driver = new RemoteWebDriver(
                    new URL(remoteURL),	capability);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.setFileDetector(new LocalFileDetector());
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getChromeRemoteWebDriver () {

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");

        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability("browser", "chrome");
        capability.setCapability("browser_version", "72.0");
        capability.setCapability("os", "Windows");
        capability.setCapability("os_version", "7");
        capability.setCapability("build", "Kite");
        capability.setCapability("acceptSslCerts", "true");
        capability.setCapability("browserstack.debug", "true");
        capability.setCapability("resolution", "1280x1024");
        capability.setCapability("browserstack.video", "false");
        capability.setCapability(ChromeOptions.CAPABILITY, options);
        RemoteWebDriver driver = null;
        try {
            driver = new RemoteWebDriver(
                    new URL(remoteURL),	capability);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.setFileDetector(new LocalFileDetector());
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getFirefoxRemoteWebDriver () {

        //FirefoxOptions options = new FirefoxOptions();
        //options.addArguments("--disable-extensions");

        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability("browser", "Firefox");
        capability.setCapability("browser_version", "58.0"); //58.0
        capability.setCapability("os", "Windows");
        capability.setCapability("os_version", "7");
        capability.setCapability("build", "Kite");
        capability.setCapability("acceptSslCerts", "true");
        capability.setCapability("browserstack.debug", "true");
        capability.setCapability("resolution", "1280x1024");
        capability.setCapability("browserstack.video", "false");
        //capability.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);
        RemoteWebDriver driver = null;
        try {
            driver = new RemoteWebDriver(
                    new URL(remoteURL),	capability);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.setFileDetector(new LocalFileDetector());
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getChromeLocalWebDriver () {
        System.setProperty("webdriver.chrome.driver", "D://Fish//chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getFirefoxLocalWebDriver () {
        System.setProperty("webdriver.gecko.driver", "D://Fish//geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getIE11LocalWebDriver () {
        System.setProperty("webdriver.ie.driver", "D://Fish//MicrosoftWebDriver.exe");
        WebDriver driver = new InternetExplorerDriver();
        driver.manage().window().maximize();
        return driver;
    }
}
