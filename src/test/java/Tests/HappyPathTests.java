package Tests;

import BaseTests.BaseTests;
import VisualTesting.Applitool;
import com.codeborne.selenide.Configuration;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class HappyPathTests extends BaseTests{

    static String br = System.getProperty("browser");

    @BeforeAll
    public static void login() {
        if(br == null || "".equals(br)) br = "Chrome";
        if (br.toLowerCase().equals("chrome")) {
            Applitool.EyesStart("Kite", "Kite2019");
        }

        open("https://full-kitekonnect.cs96.force.com");
        if (br.toLowerCase().equals("chrome")){
            Applitool.getScreenshot("'Sing In' page.(1)");
        }
        $(lp.SingInButton).waitUntil(enabled, 30000).click();
        $(lp.LoginField).waitUntil(enabled, 30000);
        if (br.toLowerCase().equals("chrome")){
            Applitool.getScreenshot("'Login' page.(2)");
        }
        $(lp.LoginField).setValue(lp.TestLogin2);
        $(lp.PasswordField).shouldBe(enabled).setValue(lp.TestPassword2);
        $(lp.LoginButton).shouldBe(enabled).click();
        $(hp.EnrollNewPatientButton).waitUntil(enabled, 60000);
        if (br.toLowerCase().equals("chrome")){
            Applitool.getScreenshot("'Home' page.(3)");
        }
    }

    @AfterAll
    public static void end(){
        if (br.toLowerCase().equals("chrome")) {
            Applitool.EyesClose();
        }
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Happy Path Scenario.")
    public void KITE01_HappyPathScenario() throws Exception {
        System.out.println("[INFO]: ******* KITE01 Happy Path Scenario - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
        .navigateToContactsPage()
        .removeContacts();

        if (br.toLowerCase().equals("chrome")){
            Applitool.getScreenshot("'Contacts' page.(4)");
        }

        hp
        .navigateToPatientForm();
        if (br.toLowerCase().equals("chrome")){
            Applitool.getScreenshot("'Patient' form.(5)");
        }

        pf
        .fillFieldsInPatienForm()
        .savePatientForm()
        .checkDataOnTotalPage();
        if (br.toLowerCase().equals("chrome")){
            Applitool.getScreenshot("Total page for Patient.(6)");
        }

        pf
        .submitPatientForm();
        if (br.toLowerCase().equals("chrome")){
            Applitool.getScreenshot("'Patient' page.(7)");
        }

        pdt
        .checkDataOnPatientPage();

        pnt
        .goToContactInformationTab();
        if (br.toLowerCase().equals("chrome")){
            Applitool.getScreenshot("'Contact Information'.(8)");
        }

        System.out.println("[INFO]: Navigate to Patient Team Tab.");

        mm
        .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);

        ptt
        .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
        .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);
        if (br.toLowerCase().equals("chrome")){
            Applitool.getScreenshot("Medical Info 'Diagnosis'.(9)");
        }

        mip
                .fillRequiredMedicalInfo();

        System.out.println("[INFO]: Navigate to Schedule Treatment Page.");
        mm
        .navigateToNextPage(mip.SaveAndNextButton, stp.AddDropOffContactButton);
        if (br.toLowerCase().equals("chrome")){
            Applitool.getScreenshot("'Schedule Treatment' page.(10)");
        }

        stp
        .addContactsOnSchedulePage();

        System.out.println("[INFO]: Open Scheduler.");
        $(stp.OpenSchedulerButton).waitUntil(enabled, 15000).click();

        shp
        .selectDateTime();

        cop
        .openOrderData()
        .checkOrderData()
        .confirmOrder();

        hp
        .navigateToHomePage()
        .findPatientBySearchField()
        .checkDataOfFirstContact();
        System.out.println("[INFO]: ******* KITE01 Happy Path Scenario - PASSED <<<<<<<");
    }

}
