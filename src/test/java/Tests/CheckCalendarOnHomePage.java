package Tests;

import BaseTests.BaseTests;
import com.codeborne.selenide.Condition;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class CheckCalendarOnHomePage extends BaseTests {

    @BeforeAll
    public static void login() {
        open("https://full-kitekonnect.cs96.force.com");
        lp.loginToApplication(lp.TestLogin2, lp.TestPassword2);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Calendar: Check Date types.")
    public void KITE75_CheckDateTypes(){
        System.out.println("[INFO]: ******* KITE Check Date types. - START >>>>>>>");
        System.out.println("[INFO]: Open 'Home' page.");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        System.out.println("[INFO]: Check 'Estimated Final Product Ready' Date Type.");
        $(hp.OpenDateTypes).waitUntil(Condition.enabled, 60000).click();
        $(hp.EstimatedFinalProductReadyDateType).waitUntil(visible, 60000);
        $(hp.EstimatedFinalProductReadyDateType).waitUntil(enabled, 60000).click();
        $(hp.EstimatedFinalProductReadyDateField).shouldBe(visible);
        System.out.println("[INFO]: Check 'Leukapheresis' Date Type.");
        $(hp.OpenDateTypes).waitUntil(Condition.enabled, 60000).click();
        $(hp.LeukapheresisDateType).waitUntil(Condition.enabled, 60000).click();
        $(hp.LeukapheresisDateField).shouldBe(visible);
        System.out.println("[INFO]: ******* KITE Check Date types. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Calendar: End Date after Start Date (Same Month).")
    public void KITE76_EndDateAfterStartDate(){
        System.out.println("[INFO]: ******* KITE End Date after Start Date. - START >>>>>>>");
        System.out.println("[INFO]: Open 'Home' page.");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        System.out.println("[INFO]: Select 'Leukapheresis' Date Type.");
        $(hp.OpenDateTypes).waitUntil(Condition.enabled, 60000).click();
        $(hp.LeukapheresisDateType).waitUntil(Condition.enabled, 60000).click();
        $(hp.LeukapheresisDateField).shouldBe(visible);

        System.out.println("[INFO]: Open Calendar.");
        $(hp.SearchByDateField).waitUntil(enabled, 30000).click();
        $(hp.Calendar).waitUntil(visible, 30000);

        System.out.println("[INFO]: Select Period (from 23-Jan-2025 to 26-Jan-2025).");
        hp.selectMonth("January");
        hp.selectYear("2025");
        hp.selectDate("23");
        hp.selectDate("26");

        System.out.println("[INFO]: Check Period.");
        $(hp.LeukapheresisDateField).shouldHave(value("23-Jan-2025 to 26-Jan-2025"));
        System.out.println("[INFO]: ******* KITE End Date after Start Date. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Calendar: Period One Day.")
    public void KITE77_SameDay(){
        System.out.println("[INFO]: ******* KITE Check period 1 day. - START >>>>>>>");
        System.out.println("[INFO]: Open 'Home' page.");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        System.out.println("[INFO]: Select 'Leukapheresis' Date Type.");
        $(hp.OpenDateTypes).waitUntil(Condition.enabled, 60000).click();
        $(hp.LeukapheresisDateType).waitUntil(Condition.enabled, 60000).click();
        $(hp.LeukapheresisDateField).shouldBe(visible);

        System.out.println("[INFO]: Open Calendar.");
        $(hp.SearchByDateField).waitUntil(enabled, 30000).click();
        $(hp.Calendar).waitUntil(visible, 30000);

        System.out.println("[INFO]: Select Period (from 11-May-2025 to 11-May-2025).");
        hp.selectMonth("May");
        hp.selectYear("2025");
        hp.selectDate("11");
        hp.selectDate("11");

        System.out.println("[INFO]: Check Period.");
        System.out.println($(hp.LeukapheresisDateField).getText());
        $(hp.LeukapheresisDateField).shouldHave(value("11-May-2025 to 11-May-2025"));
        System.out.println("[INFO]: ******* KITE Check period 1 day. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Calendar: End Date after Start Date (Different months).")
    public void KITE78_PeriodFromDifferentMonths(){
        System.out.println("[INFO]: ******* KITE Period From Different Months. - START >>>>>>>");
        System.out.println("[INFO]: Open 'Home' page.");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        System.out.println("[INFO]: Select 'Leukapheresis' Date Type.");
        $(hp.OpenDateTypes).waitUntil(Condition.enabled, 60000).click();
        $(hp.LeukapheresisDateType).waitUntil(Condition.enabled, 60000).click();
        $(hp.LeukapheresisDateField).shouldBe(visible);

        System.out.println("[INFO]: Open Calendar.");
        $(hp.SearchByDateField).waitUntil(enabled, 30000).click();
        $(hp.Calendar).waitUntil(visible, 30000);

        System.out.println("[INFO]: Select Period (from 15-Jan-2025 to 19-Mar-2025).");
        hp.selectMonth("January");
        hp.selectYear("2025");
        hp.selectDate("15");


        hp.selectMonth("March");
        hp.selectYear("2025");
        hp.selectDate("19");

        System.out.println("[INFO]: Check Period.");
        $(hp.LeukapheresisDateField).shouldHave(hasValue("15-Jan-2025 to 19-Mar-2025"));
        System.out.println("[INFO]: ******* KITE Period From Different Months. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Calendar: End Date after Start Date (Different years).")
    public void KITE79_PeriodFromDifferentYears(){
        System.out.println("[INFO]: ******* KITE Period From Different Years. - START >>>>>>>");
        System.out.println("[INFO]: Open 'Home' page.");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        System.out.println("[INFO]: Select 'Leukapheresis' Date Type.");
        $(hp.OpenDateTypes).waitUntil(Condition.enabled, 60000).click();
        $(hp.LeukapheresisDateType).waitUntil(Condition.enabled, 60000).click();
        $(hp.LeukapheresisDateField).shouldBe(visible);

        System.out.println("[INFO]: Open Calendar.");
        $(hp.SearchByDateField).waitUntil(enabled, 30000).click();
        $(hp.Calendar).waitUntil(visible, 30000);

        System.out.println("[INFO]: Select Period (from 8-Dec-2024 to 16-Oct-2026).");
        hp.selectMonth("December");
        hp.selectYear("2024");
        hp.selectDate("8");


        hp.selectMonth("October");
        hp.selectYear("2026");
        hp.selectDate("16");

        System.out.println("[INFO]: Check Period.");
        $(hp.LeukapheresisDateField).shouldHave(hasValue("8-Dec-2024 to 16-Oct-2026"));
        System.out.println("[INFO]: ******* KITE Period From Different Years. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Calendar: End Date before Start Date.")
    public void KITE80_EndDateBeforeStartDate(){
        System.out.println("[INFO]: ******* KITE End Date before Start Date. - START >>>>>>>");
        System.out.println("[INFO]: Open 'Home' page.");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        System.out.println("[INFO]: Select 'Leukapheresis' Date Type.");
        $(hp.OpenDateTypes).waitUntil(Condition.enabled, 60000).click();
        $(hp.LeukapheresisDateType).waitUntil(Condition.enabled, 60000).click();
        $(hp.LeukapheresisDateField).shouldBe(visible);

        System.out.println("[INFO]: Open Calendar.");
        $(hp.SearchByDateField).waitUntil(enabled, 30000).click();
        $(hp.Calendar).waitUntil(visible, 30000);

        System.out.println("[INFO]: Select invalid Period.");
        hp.selectMonth("October");
        hp.selectYear("2026");
        hp.selectDate("16");
        $(hp.StartDateField).shouldHave(value("16-Oct-2026"));
        $(hp.EndDateField).shouldHave(value("Select End Date"));

        System.out.println("[INFO]: End Date in previous year.");
        hp.selectMonth("December");
        hp.selectYear("2025");
        hp.selectDate("8");
        $(hp.StartDateField).shouldHave(value("16-Oct-2026"));
        $(hp.EndDateField).shouldHave(value("Select End Date"));

        System.out.println("[INFO]: End Date in previous month.");
        hp.selectMonth("September");
        hp.selectYear("2026");
        hp.selectDate("25");
        $(hp.StartDateField).shouldHave(value("16-Oct-2026"));
        $(hp.EndDateField).shouldHave(value("Select End Date"));

        System.out.println("[INFO]: End Date - previous day.");
        hp.selectMonth("October");
        hp.selectYear("2026");
        hp.selectDate("15");
        $(hp.StartDateField).shouldHave(value("16-Oct-2026"));
        $(hp.EndDateField).shouldHave(value("Select End Date"));
        System.out.println("[INFO]: ******* KITE End Date before Start Date. - PASSED <<<<<<<");
    }
}
