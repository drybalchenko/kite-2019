package Tests;

import BaseTests.BaseTests;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selenide.open;

public class TestsForContactsPage extends BaseTests{

    @BeforeAll
    public static void login() {
        open("https://full-kitekonnect.cs96.force.com");
        lp.loginToApplication(lp.TestLogin2, lp.TestPassword2);
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Create New Contact.")
    public void KITE18_CreateNewContact() throws Exception {
        System.out.println("[INFO]: ******* KITE18 Create New Contact - START >>>>>>>");
        cp
        .navigateToContactsPage()
        .removeContacts()
        .addNewContact()
        .navigateToContactsPage()
        .searchContact()
        .navigateToContactsPage()
        .checkContactData()
        .editContactData()
        .checkEditedContactData()
        .removeContacts();
        System.out.println("[INFO]: ******* KITE18 Create New Contact - PASSED <<<<<<<");
    }
}
