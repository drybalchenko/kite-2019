package Tests;

import BaseTests.BaseTests;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selenide.open;

public class CheckManufacturingStatuses extends BaseTests {

    @BeforeAll
    public static void login() {
        open("https://full-kitekonnect.cs96.force.com");
        lp.loginToApplication(lp.TestLogin2, lp.TestPassword2);
    }

    //@Test
    public void KITE26_CheckManufacturingStatuses() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE26 Check Manufacturing Statuses - START >>>>>>>");
        oms.clearDateFields();
        oms.changeStatusToLeukapheresisScheduled();
        oms.changeStatusToLeukapheresisCompleted();
        oms.changeStatusToAphMaterialShipped();
        oms.changeStatusToAphMaterialReceived();
        oms.changeStatusToManufacturing();
        oms.changeStatusToReleaseTesting();
        oms.changeStatusToReadyToShip();
        oms.changeStatusToFinalProductShipped();
        oms.changeStatusToFinalProductDelivered();
        System.out.println("[INFO]: ******* KITE26 Check Manufacturing Statuses - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Check status 'Draft Order'.")
    public void KITE37_CheckStatusDraftOrder() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE37 Check status 'Draft Order' - START >>>>>>>");
        oms.clearDateFields();
        oms.changeStatusToDraftOrder2();
        System.out.println("[INFO]: ******* KITE37 Check status 'Draft Order' - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Check status 'Leukapheresis Scheduled'.")
    public void KITE38_CheckStatusLeukapheresisScheduled() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE38 Check status 'Leukapheresis Scheduled' - START >>>>>>>");
        oms.clearDateFields();
        oms.changeStatusToLeukapheresisScheduled2();
        System.out.println("[INFO]: ******* KITE38 Check status 'Leukapheresis Scheduled' - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Check status 'Aph Kit Shipped'")
    public void KITE39_CheckStatusAphKitShipped() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE39 Check status 'Aph Kit Shipped' - START >>>>>>>");
        oms.clearDateFields();
        oms.changeStatusToAphKitShipped2();
        System.out.println("[INFO]: ******* KITE39 Check status 'Aph Kit Shipped' - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Check status 'Aph Material Received'.")
    public void KITE40_CheckStatusAphMaterialReceived() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE40 Check status 'Aph Material Received' - START >>>>>>>");
        oms.clearDateFields();
        oms.changeStatusToAphMaterialReceived2();
        System.out.println("[INFO]: ******* KITE40 Check status 'Aph Material Received' - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Check status 'Manufacturing'.")
    public void KITE41_CheckStatusManufacturing() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE41 Check status 'Manufacturing' - START >>>>>>>");
        oms.clearDateFields();
        oms.changeStatusToManufacturing2();
        System.out.println("[INFO]: ******* KITE41 Check status 'Manufacturing' - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Check status 'Release Testing'.")
    public void KITE42_CheckStatusReleaseTesting() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE42 Check status 'Release Testing' - START >>>>>>>");
        oms.clearDateFields();
        oms.changeStatusToReleaseTesting2();
        System.out.println("[INFO]: ******* KITE42 Check status 'Release Testing' - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Check status 'Ready To Ship'.")
    public void KITE43_CheckStatusReadyToShip() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE43 Check status 'Ready To Ship' - START >>>>>>>");
        oms.clearDateFields();
        oms.changeStatusToReadyToShip2();
        System.out.println("[INFO]: ******* KITE43 Check status 'Ready To Ship' - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Check status 'Final Product Shipped'.")
    public void KITE44_CheckStatusFinalProductShipped() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE44 Check status 'Final Product Shipped' - START >>>>>>>");
        oms.clearDateFields();
        oms.changeStatusToFinalProductShipped2();
        System.out.println("[INFO]: ******* KITE44 Check status 'Final Product Shipped' - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Check status 'Final Product Delivered'.")
    public void KITE45_CheckStatusFinalProductDelivered() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE45 Check status 'Final Product Delivered' - START >>>>>>>");
        oms.clearDateFields();
        oms.changeStatusToFinalProductDelivered2();
        System.out.println("[INFO]: ******* KITE45 Check status 'Final Product Delivered' - PASSED <<<<<<<");
    }

}
