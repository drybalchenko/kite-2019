package Tests;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import BaseTests.BaseTests;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class CreateNewPatient extends BaseTests{

    @BeforeAll
    public static void login() {
        open("https://full-kitekonnect.cs96.force.com");
        lp.loginToApplication(lp.TestLogin2, lp.TestPassword2);
    }

    //@Test
    public void KITE17_CreateNewPatient() throws Exception {
        System.out.println("[INFO]: ******* KITE17 Create new Patient - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pdt
                .checkDataOnPatientPage()
                .editDataOnPatientPage()
                .submitEditedDataOnPatientPage()
                .submitPatientForm()
                .checkEditedDataOnPatientPage();

        pnt
                .goToContactInformationTab();

        pcit
                .fillFieldsInContactInformation()
                .checkContactInformation()
                .clickSaveAndNextButton()
                .editContactInformation()
                .checkEditedContactInformation();

        System.out.println("[INFO]: ******* KITE17 Create new Patient - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Edit Data On Patient Page.")
    public void KITE17_EditDataOnPatientPage() throws Exception {
        System.out.println("[INFO]: ******* KITE17 Edit Data On Patient Page - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        hp
        .navigateToPatientForm();

        pf
        .fillFieldsInPatienForm()
        .savePatientForm()
        .checkDataOnTotalPage()
        .submitPatientForm();

        pdt
        .checkDataOnPatientPage()
        .editDataOnPatientPage()
        .submitEditedDataOnPatientPage()
        .submitPatientForm()
        .checkEditedDataOnPatientPage();
        System.out.println("[INFO]: ******* KITE17  Edit Data On Patient Page - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Add and Edit Patient Contact Information by 'SaveButton'.")
    public void KITE27_AddEditPatientContactInfo_SaveButton() throws Exception {
        System.out.println("[INFO]: ******* KITE27 Add and Edit Patient Contact Information by 'SaveButton' - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        hp
        .navigateToPatientForm();

        pf
        .fillFieldsInPatienForm()
        .savePatientForm()
        .checkDataOnTotalPage()
        .submitPatientForm();

        pnt
        .goToContactInformationTab();

        pcit
        .fillFieldsInContactInformation()
        .clickSaveAndNextButton()
        .checkContactInformation()
        .editContactInformation()
        .clickSaveAndNextButton()
        .checkEditedContactInformation();

        System.out.println("[INFO]: ******* KITE27 Add and Edit Patient Contact Information by 'SaveButton' - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Create Patient That Already Exist.")
    public void KITE28_CreatePatientThatAlreadyExist() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE28 Create Patient That Already Exist - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        hp
        .navigateToPatientForm();

        System.out.println("[INFO]: Try to create Patient that already exist.");
        $(pf.FirstNameField).setValue(pf.ExistingFirstName);
        //$(pf.MiddleNameField).setValue(pf.MiddleName);
        $(pf.LastNameField).setValue(pf.LastName);
        $(pf.DobYearField).setValue(pf.DobYear);
        mm
                .selectFromPicklistUniversal(pf.MonthField, "Jan")
                .selectFromPicklistUniversal(pf.DayField, "01");

        $(pf.MaleGenderButton).waitUntil(enabled, 15000).click();

        mm
        .selectFromSearchField(pf.ThreatingPhisicianField, pf.ThreatingFirstRecord, pf.ThreatingPhysician);

        $(pf.AddOwnHpNoButton).waitUntil(enabled, 15000).click();
        pf.savePatientForm();

        $(pf.TotalConfirmButton).waitUntil(enabled, 15000).click();

        System.out.println("[INFO]: Check Error message for Patient that already exist.");
        $(pf.PatientExistError).waitUntil(visible, 20000).shouldHave(text(pf.PatientExistErrorMessage));

        System.out.println("[INFO]: ******* KITE28 Create Patient That Already Exist - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Edit Data On Patient Page Cancel 'YES'.")
    public void KITE29_EditDataOnPatientPageCancelYes() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE29 Edit Data On Patient Page Cancel 'YES'. - START >>>>>>>");

        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pdt
                .checkDataOnPatientPage()
                .editDataOnPatientPage()
                .cancelEditFormYes()
                .checkNonEditedDataOnPatientPage();

        System.out.println("[INFO]: ******* KITE29 Edit Data On Patient Page Cancel 'YES'. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Edit Data On Patient Page Cancel 'NO'.")
    public void KITE30_EditDataOnPatientPageCancelNo() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE30 Edit Data On Patient Page Cancel 'NO'. - START >>>>>>>");

        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pdt
                .checkDataOnPatientPage()
                .editDataOnPatientPage()
                .cancelEditFormNo()
                .submitPatientForm()
                .checkEditedDataOnPatientPage();

        System.out.println("[INFO]: ******* KITE30 Edit Data On Patient Page Cancel 'NO'. - PASSED <<<<<<<");
    }
}
