package Tests;

import BaseTests.BaseTests;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selenide.open;

public class CheckLoginPage extends BaseTests{

    @BeforeAll
    public static void login() {
        open("https://full-kitekonnect.cs96.force.com");
        lp.loginToApplication(lp.TestLogin2, lp.TestPassword2);
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Login With Incorrect Credentials.")
    public void KITE19_LoginWithIncorrectCredentials(){
        System.out.println("[INFO]: ******* KITE19 Login With Incorrect Credentials - START >>>>>>>");
        lp.loginWithIncorrectLogin();
        lp.loginWithIncorrectPassword();
        System.out.println("[INFO]: ******* KITE19 Login With Incorrect Credentials - PASSED <<<<<<<");
    }
}
