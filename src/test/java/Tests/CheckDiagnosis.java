package Tests;

import BaseTests.BaseTests;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static com.codeborne.selenide.Selenide.open;

public class CheckDiagnosis extends BaseTests {

    @BeforeAll
    public static void login() {
        open("https://full-kitekonnect.cs96.force.com");
        lp.loginToApplication(lp.TestLogin2, lp.TestPassword2);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("First Diagnosis (Diffuse Large B Cell Lymphoma (DLBCL), Not Otherwise Specified) by 'Save&Next'.")
    public void KITE46_FirstDiagnosis_SaveAndNext() throws Exception {
        System.out.println("[INFO]: ******* KITE46 First Diagnosis by 'Save&Next'. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();

        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton); //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .selectDiagnosis(mip.FirstDiagnosis)
                .diagnosisClickSaveAndNext()
                .checkSelectedDiagnosis("Diffuse Large B Cell Lymphoma (DLBCL), Not Otherwise Specified");

        System.out.println("[INFO]: ******* KITE46 First Diagnosis by 'Save&Next'. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Second Diagnosis (DLBCL Arising From Follicular Lymphoma) by 'Save&Next'.")
    public void KITE47_SecondDiagnosis_SaveAndNext() throws Exception {
        System.out.println("[INFO]: ******* KITE47 Second Diagnosis by 'Save&Next'. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();

        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton); //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .selectDiagnosis(mip.SecondDiagnosis)
                .diagnosisClickSaveAndNext()
                .checkSelectedDiagnosis("DLBCL Arising From Follicular Lymphoma");

        System.out.println("[INFO]: ******* KITE47 Second Diagnosis by 'Save&Next'. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Third Diagnosis (Primary Mediastinal B Cell Lymphoma (PMBCL)) by 'Save&Next'.")
    public void KITE48_ThirdDiagnosis_SaveAndNext() throws Exception {
        System.out.println("[INFO]: ******* KITE48 Third Diagnosis by 'Save&Next'. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();

        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton); //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .selectDiagnosis(mip.ThirdDiagnosis)
                .diagnosisClickSaveAndNext()
                .checkSelectedDiagnosis("Primary Mediastinal B Cell Lymphoma (PMBCL)");

        System.out.println("[INFO]: ******* KITE48 Third Diagnosis by 'Save&Next'. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Fourth Diagnosis (High Grade B Cell Lymphoma) by 'Save&Next'.")
    public void KITE49_FourthDiagnosis_SaveAndNext() throws Exception {
        System.out.println("[INFO]: ******* KITE49 Fourth Diagnosis by 'Save&Next'. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();

        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton); //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .selectDiagnosis(mip.FourthDiagnosis)
                .diagnosisClickSaveAndNext()
                .checkSelectedDiagnosis("High Grade B Cell Lymphoma");

        System.out.println("[INFO]: ******* KITE49 Fourth Diagnosis by 'Save&Next'. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Other Diagnosis (Other) by 'Save&Next'.")
    public void KITE50_OtherDiagnosis_SaveAndNext() throws Exception {
        System.out.println("[INFO]: ******* KITE50 Other Diagnosis by 'Save&Next'. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();

        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton); //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .selectDiagnosis(mip.OtherDiagnosis)
                .addDiagnosis()
                .diagnosisClickSaveAndNext()
                .checkSelectedDiagnosis("Other");

        System.out.println("[INFO]: ******* KITE50 Other Diagnosis by 'Save&Next'. - PASSED <<<<<<<");
    }

    //------------------------------------------------------------------------------------------------------------------

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("First Diagnosis (Diffuse Large B Cell Lymphoma (DLBCL), Not Otherwise Specified) by 'Save&Exit'.")
    public void KITE51_FirstDiagnosis_SaveAndExit() throws Exception {
        System.out.println("[INFO]: ******* KITE51 First Diagnosis by 'Save&Exit'. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();

        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton); //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .selectDiagnosis(mip.FirstDiagnosis)
                .diagnosisClickSaveAndExit();

        hp
                .navigateToHomePage()
                .findPatientBySearchField()
                .openFirstPatient();

        mip
                //.openMedicalInfo()
                .diagnosisClickSaveAndNext()
                .checkSelectedDiagnosis("Diffuse Large B Cell Lymphoma (DLBCL), Not Otherwise Specified");

        System.out.println("[INFO]: ******* KITE51 First Diagnosis by 'Save&Exit'. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Second Diagnosis (DLBCL Arising From Follicular Lymphoma) by 'Save&Exit'.")
    public void KITE52_SecondDiagnosis_SaveAndExit() throws Exception {
        System.out.println("[INFO]: ******* KITE52 Second Diagnosis by 'Save&Exit'. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();

        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton); //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .selectDiagnosis(mip.SecondDiagnosis)
                .diagnosisClickSaveAndExit();

        hp
                .navigateToHomePage()
                .findPatientBySearchField()
                .openFirstPatient();

        mip
                //.openMedicalInfo()
                .diagnosisClickSaveAndNext()
                .checkSelectedDiagnosis("DLBCL Arising From Follicular Lymphoma");

        System.out.println("[INFO]: ******* KITE52 Second Diagnosis by 'Save&Exit'. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Third Diagnosis (Primary Mediastinal B Cell Lymphoma (PMBCL)) by 'Save&Exit'.")
    public void KITE53_ThirdDiagnosis_SaveAndExit() throws Exception {
        System.out.println("[INFO]: ******* KITE53 Third Diagnosis by 'Save&Exit'. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();

        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton); //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .selectDiagnosis(mip.ThirdDiagnosis)
                .diagnosisClickSaveAndExit();

        hp
                .navigateToHomePage()
                .findPatientBySearchField()
                .openFirstPatient();

        mip
                //.openMedicalInfo()
                .diagnosisClickSaveAndNext()
                .checkSelectedDiagnosis("Primary Mediastinal B Cell Lymphoma (PMBCL)");

        System.out.println("[INFO]: ******* KITE53 Third Diagnosis by 'Save&Exit'. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Fourth Diagnosis (High Grade B Cell Lymphoma) by 'Save&Exit'.")
    public void KITE54_FourthDiagnosis_SaveAndExit() throws Exception {
        System.out.println("[INFO]: ******* KITE54 Fourth Diagnosis by 'Save&Exit'. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();

        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton); //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .selectDiagnosis(mip.FourthDiagnosis)
                .diagnosisClickSaveAndExit();

        hp
                .navigateToHomePage()
                .findPatientBySearchField()
                .openFirstPatient();

        mip
                //.openMedicalInfo()
                .diagnosisClickSaveAndNext()
                .checkSelectedDiagnosis("High Grade B Cell Lymphoma");

        System.out.println("[INFO]: ******* KITE54 Fourth Diagnosis by 'Save&Exit'. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Other Diagnosis (Other) by 'Save&Next'.")
    public void KITE55_OtherDiagnosis_SaveAndExit() throws Exception {
        System.out.println("[INFO]: ******* KITE55 Other Diagnosis by 'Save&Next'. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();

        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton); //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .selectDiagnosis(mip.OtherDiagnosis)
                .addDiagnosis()
                .diagnosisClickSaveAndExit();

        hp
                .navigateToHomePage()
                .findPatientBySearchField()
                .openFirstPatient();

        mip
                //.openMedicalInfo()
                .diagnosisClickSaveAndNext()
                .checkSelectedDiagnosis("Other");

        System.out.println("[INFO]: ******* KITE55 Other Diagnosis by 'Save&Next'. - PASSED <<<<<<<");
    }
}
