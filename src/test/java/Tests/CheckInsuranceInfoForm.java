package Tests;

import BaseTests.BaseTests;
import com.codeborne.selenide.Condition;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class CheckInsuranceInfoForm extends BaseTests {

    @BeforeAll
    public static void login() {
        open("https://full-kitekonnect.cs96.force.com");
        lp.loginToApplication(lp.TestLogin2, lp.TestPassword2);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Empty Insurance Info Form.")
    public void KITE62_EmptyInsuranceInfoForm() throws Exception {
        System.out.println("[INFO]: ******* KITE62 Empty Insurance Info Form. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);   //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.BenefitInvestigationAssistance)
                .diagnosisClickSaveAndNext();

        System.out.println("[INFO]: Save Patient Assistance selection.");

        mip
                .openInsuranceForm()
                .checkFieldsInInsuranceForm();
        $(mip.ContinueToScheduleButton).waitUntil(Condition.enabled, 20000).click();

        mip
                .checkErrorMessagesEmptyInsuranceForm();

        $(mip.ManuallyEnter).waitUntil(Condition.enabled, 20000).click();
        $(mip.PIName).waitUntil(Condition.hidden, 20000);
        System.out.println("[INFO]: ******* KITE62 Empty Insurance Info Form. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Fill all required fields in Insurance Info Form.")
    public void KITE63_FillInsuranceInfoForm() throws Exception {
        System.out.println("[INFO]: ******* KITE63 Fill all required fields in Insurance Info Form. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.BenefitInvestigationAssistance)
                .diagnosisClickSaveAndNext();

        System.out.println("[INFO]: Save Patient Assistance selection.");

        mip
                .openInsuranceForm()
                .fillRequiredFieldsInInsuranceForm();

        mm
                .navigateToNextPage(mip.mip.ContinueToScheduleButton, stp.AddDropOffContactButton);

        System.out.println("[INFO]: ******* KITE63 Fill all required fields in Insurance Info Form. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Insurance Info Form Without Type.")
    public void KITE64_InsuranceInfoForm_WithoutType() throws Exception {
        System.out.println("[INFO]: ******* KITE64 Insurance Info Form Without Type. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.BenefitInvestigationAssistance)
                .diagnosisClickSaveAndNext();

        System.out.println("[INFO]: Save Patient Assistance selection.");

        mip
                .openInsuranceForm();

        System.out.println("[INFO]: Fill Required Fields In Insurance Form without Type.");
        mm
                .selectFromSearchField(mip.PIName, pf.ThreatingFirstRecord ,"Test Payer");

        Thread.sleep(3000);
        $(mip.PISubscriberName).sendKeys("TestSubscriber");
        $(mip.PIDob).sendKeys(pf.DobYear);

        mm
                .selectFromPicklistUniversal(mip.PIMonth, pf.MONTH)
                .selectFromPicklistUniversal(mip.PIDay, pf.DAY);

        $(mip.PISubscriberId).sendKeys("0123456789");
        $(mip.ContinueToScheduleButton).waitUntil(Condition.enabled, 20000).click();

        System.out.println("[INFO]: Check Error Message for Type field.");
        $(mip.PITypeError).waitUntil(exist, 20000).shouldBe(visible);

        $(mip.PINameError).shouldBe(hidden);
        $(mip.PIPhoneNumberError).shouldBe(hidden);
        $(mip.PISubscriberNameError).shouldBe(hidden);
        $(mip.PIDobError).shouldBe(hidden);
        $(mip.PIMonthError).shouldBe(hidden);
        $(mip.PIDayError).shouldBe(hidden);
        $(mip.PISubscriberIdError).shouldBe(hidden);
        System.out.println("[INFO]: ******* KITE64 Insurance Info Form Without Type. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Insurance Info Form Without Name.")
    public void KITE65_InsuranceInfoForm_WithoutName() throws Exception {
        System.out.println("[INFO]: ******* KITE65 Insurance Info Form Without Name. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.BenefitInvestigationAssistance)
                .diagnosisClickSaveAndNext();

        System.out.println("[INFO]: Save Patient Assistance selection.");

        mip
                .openInsuranceForm();

        System.out.println("[INFO]: Fill Required Fields In Insurance Form without Name.");
        mm
                .selectFromPicklistUniversal(mip.PIType, "Commercial");

        Thread.sleep(3000);
        $(mip.PISubscriberName).sendKeys("TestSubscriber");
        $(mip.PIDob).sendKeys(pf.DobYear);

        mm
                .selectFromPicklistUniversal(mip.PIMonth, pf.MONTH)
                .selectFromPicklistUniversal(mip.PIDay, pf.DAY);

        $(mip.PISubscriberId).sendKeys("0123456789");
        $(mip.ContinueToScheduleButton).waitUntil(Condition.enabled, 20000).click();

        System.out.println("[INFO]: Check Error Message for Type field.");
        $(mip.PINameError).waitUntil(exist, 20000).shouldBe(visible);

        $(mip.PITypeError).shouldBe(hidden);
        $(mip.PIPhoneNumberError).shouldBe(visible);
        $(mip.PISubscriberNameError).shouldBe(hidden);
        $(mip.PIDobError).shouldBe(hidden);
        $(mip.PIMonthError).shouldBe(hidden);
        $(mip.PIDayError).shouldBe(hidden);
        $(mip.PISubscriberIdError).shouldBe(hidden);
        System.out.println("[INFO]: ******* KITE65 Insurance Info Form Without Name. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Insurance Info Form Without Subscriber Name.")
    public void KITE66_InsuranceInfoForm_WithoutSubscriberName() throws Exception {
        System.out.println("[INFO]: ******* KITE66 Insurance Info Form Without Subscriber Name. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.BenefitInvestigationAssistance)
                .diagnosisClickSaveAndNext();

        System.out.println("[INFO]: Save Patient Assistance selection.");

        mip
                .openInsuranceForm();

        System.out.println("[INFO]: Fill Required Fields In Insurance Form without Subscriber Name.");
        mm
                .selectFromPicklistUniversal(mip.PIType, "Commercial")
                .selectFromSearchField(mip.PIName, pf.ThreatingFirstRecord ,"Test Payer");

        Thread.sleep(3000);
        $(mip.PIDob).sendKeys(pf.DobYear);

        mm
                .selectFromPicklistUniversal(mip.PIMonth, pf.MONTH)
                .selectFromPicklistUniversal(mip.PIDay, pf.DAY);

        $(mip.PISubscriberId).sendKeys("0123456789");
        $(mip.ContinueToScheduleButton).waitUntil(Condition.enabled, 20000).click();

        System.out.println("[INFO]: Check Error Message for Type field.");
        $(mip.PISubscriberNameError).waitUntil(exist, 20000).shouldBe(visible);

        $(mip.PITypeError).shouldBe(hidden);
        $(mip.PINameError).shouldBe(hidden);
        $(mip.PIPhoneNumberError).shouldBe(hidden);
        $(mip.PIDobError).shouldBe(hidden);
        $(mip.PIMonthError).shouldBe(hidden);
        $(mip.PIDayError).shouldBe(hidden);
        $(mip.PISubscriberIdError).shouldBe(hidden);
        System.out.println("[INFO]: ******* KITE66 Insurance Info Form Without Subscriber Name. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Insurance Info Form Without DOB.")
    public void KITE67_InsuranceInfoForm_WithoutDob() throws Exception {
        System.out.println("[INFO]: ******* KITE67 Insurance Info Form Without DOB. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.BenefitInvestigationAssistance)
                .diagnosisClickSaveAndNext();

        System.out.println("[INFO]: Save Patient Assistance selection.");

        mip
                .openInsuranceForm();

        System.out.println("[INFO]: Fill Required Fields In Insurance Form without DOB.");
        mm
                .selectFromPicklistUniversal(mip.PIType, "Commercial")
                .selectFromSearchField(mip.PIName, pf.ThreatingFirstRecord ,"Test Payer");

        Thread.sleep(3000);
        $(mip.PISubscriberName).sendKeys("TestSubscriber");

        mm
                .selectFromPicklistUniversal(mip.PIMonth, pf.MONTH);

        $(mip.PISubscriberId).sendKeys("0123456789");
        $(mip.ContinueToScheduleButton).waitUntil(Condition.enabled, 20000).click();

        System.out.println("[INFO]: Check Error Message for DOB.");
        $(mip.PIDayErrorForYear).waitUntil(exist, 60000).shouldBe(visible);

        //$(mip.PIDayError).waitUntil(exist, 60000).shouldBe(visible);
        //$(mip.PIDobError).shouldBe(visible);

        $(mip.PITypeError).shouldBe(hidden);
        $(mip.PINameError).shouldBe(hidden);
        $(mip.PIPhoneNumberError).shouldBe(hidden);
        $(mip.PISubscriberNameError).shouldBe(hidden);
        $(mip.PIMonthError).shouldBe(hidden);
        //$(mip.PIDayErrorForYear).shouldBe(visible);
        $(mip.PISubscriberIdError).shouldBe(hidden);
        System.out.println("[INFO]: ******* KITE67 Insurance Info Form Without DOB. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Insurance Info Form Without Month.")
    public void KITE68_InsuranceInfoForm_WithoutMonth() throws Exception {
        System.out.println("[INFO]: ******* KITE68 Insurance Info Form Without Month. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.BenefitInvestigationAssistance)
                .diagnosisClickSaveAndNext();

        System.out.println("[INFO]: Save Patient Assistance selection.");

        mip
                .openInsuranceForm();

        System.out.println("[INFO]: Fill Required Fields In Insurance Form without Without Month.");
        mm
                .selectFromPicklistUniversal(mip.PIType, "Commercial")
                .selectFromSearchField(mip.PIName, pf.ThreatingFirstRecord ,"Test Payer");

        Thread.sleep(3000);
        $(mip.PISubscriberName).sendKeys("TestSubscriber");
        $(mip.PIDob).sendKeys(pf.DobYear);
        $(mip.PISubscriberId).sendKeys("0123456789");
        $(mip.ContinueToScheduleButton).waitUntil(Condition.enabled, 20000).click();

        System.out.println("[INFO]: Check Error Message for Type field.");
        $(mip.PIMonthError).waitUntil(exist, 20000).shouldBe(visible);

        $(mip.PITypeError).shouldBe(hidden);
        $(mip.PINameError).shouldBe(hidden);
        $(mip.PIPhoneNumberError).shouldBe(hidden);
        $(mip.PISubscriberNameError).shouldBe(hidden);
        $(mip.PIDobError).shouldBe(hidden);
        $(mip.PIDayError).shouldBe(visible);
        $(mip.PISubscriberIdError).shouldBe(hidden);
        System.out.println("[INFO]: ******* KITE68 Insurance Info Form Without Month. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Insurance Info Form Without Day.")
    public void KITE69_InsuranceInfoForm_WithoutDay() throws Exception {
        System.out.println("[INFO]: ******* KITE69 Insurance Info Form Without Day. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.BenefitInvestigationAssistance)
                .diagnosisClickSaveAndNext();

        System.out.println("[INFO]: Save Patient Assistance selection.");

        mip
                .openInsuranceForm();

        System.out.println("[INFO]: Fill Required Fields In Insurance Form without Day.");
        mm
                .selectFromPicklistUniversal(mip.PIType, "Commercial")
                .selectFromSearchField(mip.PIName, pf.ThreatingFirstRecord ,"Test Payer");

        Thread.sleep(3000);
        $(mip.PISubscriberName).sendKeys("TestSubscriber");
        $(mip.PIDob).sendKeys(pf.DobYear);

        mm
                .selectFromPicklistUniversal(mip.PIMonth, pf.MONTH);

        $(mip.PISubscriberId).sendKeys("0123456789");
        $(mip.ContinueToScheduleButton).waitUntil(Condition.enabled, 20000).click();

        System.out.println("[INFO]: Check Error Message for Type field.");
        $(mip.PIDayError).waitUntil(exist, 20000).shouldBe(visible);

        $(mip.PITypeError).shouldBe(hidden);
        $(mip.PINameError).shouldBe(hidden);
        $(mip.PIPhoneNumberError).shouldBe(hidden);
        $(mip.PISubscriberNameError).shouldBe(hidden);
        $(mip.PIDobError).shouldBe(hidden);
        $(mip.PIMonthError).shouldBe(hidden);
        $(mip.PISubscriberIdError).shouldBe(hidden);
        System.out.println("[INFO]: ******* KITE69 Insurance Info Form Without Day. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Insurance Info Form Without ID.")
    public void KITE70_InsuranceInfoForm_WithoutID() throws Exception {
        System.out.println("[INFO]: ******* KITE70 Insurance Info Form Without ID. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.BenefitInvestigationAssistance)
                .diagnosisClickSaveAndNext();

        System.out.println("[INFO]: Save Patient Assistance selection.");

        mip
                .openInsuranceForm();

        System.out.println("[INFO]: Fill Required Fields In Insurance Form without ID.");
        mm
                .selectFromPicklistUniversal(mip.PIType, "Commercial")
                .selectFromSearchField(mip.PIName, pf.ThreatingFirstRecord ,"Test Payer");

        Thread.sleep(3000);
        $(mip.PISubscriberName).sendKeys("TestSubscriber");
        $(mip.PIDob).sendKeys(pf.DobYear);

        mm
                .selectFromPicklistUniversal(mip.PIMonth, pf.MONTH)
                .selectFromPicklistUniversal(mip.PIDay, pf.DAY);;

        $(mip.ContinueToScheduleButton).waitUntil(Condition.enabled, 20000).click();

        System.out.println("[INFO]: Check Error Message for Type field.");
        $(mip.PISubscriberIdError).waitUntil(exist, 20000).shouldBe(visible);

        $(mip.PITypeError).shouldBe(hidden);
        $(mip.PINameError).shouldBe(hidden);
        $(mip.PIPhoneNumberError).shouldBe(hidden);
        $(mip.PISubscriberNameError).shouldBe(hidden);
        $(mip.PIDobError).shouldBe(hidden);
        $(mip.PIMonthError).shouldBe(hidden);
        System.out.println("[INFO]: ******* KITE70 Insurance Info Form Without ID. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Upload Insurance Card.")
    public void KITE73_UploadInsuranceCard() throws Exception {
        System.out.println("[INFO]: ******* KITE73 Upload Insurance Card. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.BenefitInvestigationAssistance)
                .diagnosisClickSaveAndNext();

        System.out.println("[INFO]: Save Patient Assistance selection.");

        System.out.println("[INFO]: Check Error message for Patient Insurance Info.");
        $(mip.ContinueToScheduleButton).waitUntil(enabled, 30000).click();
        Thread.sleep(3000);
        $(mip.PIErrorMessage).shouldBe(visible);
        $(mip.UploadInsuranceCard).waitUntil(enabled, 30000).click();
        $(mip.ContinueToScheduleButton).waitUntil(enabled, 30000).click();
        Thread.sleep(3000);
        $(mip.PIUploadErrorMessage).shouldBe(visible);

        at
                .newUploadFile("src/test/resources/testPdfFile.pdf");

        mip
                .closeUploadInfoWindow(mip.PIUploadedWindow, mip.PIUploadedWindowCloseButton);

        System.out.println("[INFO]: Navigate to Schedule Treatment Page.");
        mm
                .navigateToNextPage(mip.ContinueToScheduleButton, stp.AddDropOffContactButton);

        System.out.println("[INFO]: ******* KITE73 Upload Insurance Card. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Upload Insurance Card by 'Save&Exit'.")
    public void KITE74_UploadInsuranceCardSaveAndExit() throws Exception {
        System.out.println("[INFO]: ******* KITE74 Upload Insurance Card by 'Save&Exit'. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.BenefitInvestigationAssistance)
                .diagnosisClickSaveAndNext();

        System.out.println("[INFO]: Save Patient Assistance selection.");

        System.out.println("[INFO]: Check Error message for Patient Insurance Info.");
        $(mip.UploadInsuranceCard).waitUntil(enabled, 30000).click();

        at
                .newUploadFile("src/test/resources/testPdfFile.pdf");

        mip
                .closeUploadInfoWindow(mip.PIUploadedWindow, mip.PIUploadedWindowCloseButton);

        System.out.println("[INFO]: Click 'Save&Exit' button.");
        $(mip.SaveAndExitButton).waitUntil(enabled, 30000).click();

        hp
                .findPatientBySearchField()
                .openFirstPatient();

        System.out.println("[INFO]: Open Patient information.");
        $(hp.PatientName).waitUntil(enabled, 30000).click();

        System.out.println("[INFO]: Navigate to Schedule Treatment Page.");
        mm
                .navigateToNextPage(mip.ContinueToScheduleButton, stp.AddDropOffContactButton);

        System.out.println("[INFO]: ******* KITE74 Upload Insurance Card by 'Save&Exit'. - PASSED <<<<<<<");
    }
}
