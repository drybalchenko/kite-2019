package Tests;

import BaseTests.BaseTests;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.switchTo;

public class CheckAttachments extends BaseTests {

    @BeforeAll
    public static void login() {
        open("https://full-kitekonnect.cs96.force.com");
        lp.loginToApplication(lp.TestLogin2, lp.TestPassword2);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Add attachment.")
    public void KITE31_AddAttachment() throws Exception {
        System.out.println("[INFO]: ******* KITE31 Add attachment - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        at
                .uploadFile("src/test/resources/testPdfFile.pdf");

        hp
                .navigateToHomePage()
                .findPatientBySearchField()
                .openFirstPatient();

        at
                .checkUploadedFile();

        System.out.println("[INFO]: ******* KITE31 Add attachment - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Add attachment by 'Save & Next' button.")
    public void KITE32_AddAttachment_SaveAndNext() throws Exception {
        System.out.println("[INFO]: ******* KITE32 Add attachment by 'Save & Next' button. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        at
                .uploadFile("src/test/resources/testPdfFile.pdf")
                .clickSaveAndNextButton();

        hp
                .navigateToHomePage()
                .findPatientBySearchField()
                .openFirstPatient();

        at
                .checkUploadedFile();

        System.out.println("[INFO]: ******* KITE32 Add attachment by 'Save & Next' button. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Add attachment by 'Save & Exit' button.")
    public void KITE33_AddAttachment_SaveAndExit() throws Exception {
        System.out.println("[INFO]: ******* KITE33 Add attachment by 'Save & Exit' button. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        at
                .uploadFile("src/test/resources/testPdfFile.pdf")
                .clickSaveAndExitButton();

        hp
                .navigateToHomePage()
                .findPatientBySearchField()
                .openFirstPatient();

        at
                .checkUploadedFile();

        System.out.println("[INFO]: ******* KITE33 Add attachment by 'Save & Exit' button. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Add attachment Large File (9 Mb) by 'Save & Next' button.")
    public void KITE34_AddAttachment_LargeFile_SaveAndNext() throws Exception {
        System.out.println("[INFO]: ******* KITE34 Add attachment Large File (9 Mb) by 'Save & Next' button. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        at
                .uploadLargeFile("src/test/resources/largePdfFile.pdf");

                switchTo().alert().getText().equals("Alert text : Alert : File size cannot exceed 4500000 bytes." +
                " Selected file size: 9836770");
                switchTo().alert().accept();

        at
                .clickSaveAndNextButton()
                .checkUploadedLargeFile();

        System.out.println("[INFO]: ******* KITE34 Add attachment Large File (9 Mb) by 'Save & Next' button. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Add attachment Large File (9 Mb) by 'Save & Exit' button.")
    public void KITE35_AddAttachment_LargeFile_SaveAndExit() throws Exception {
        System.out.println("[INFO]: ******* KITE35 Add attachment Large File (9 Mb) by 'Save & Exit' button. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        at
                .uploadLargeFile("src/test/resources/largePdfFile.pdf");

                switchTo().alert().getText().equals("Alert text : Alert : File size cannot exceed 4500000 bytes." +
                " Selected file size: 9836770");
                switchTo().alert().accept();

        at
                .clickSaveAndExitButton();

        hp
                .navigateToHomePage()
                .findPatientBySearchField()
                .openFirstPatient();

        at
                .checkUploadedLargeFile();

        System.out.println("[INFO]: ******* KITE35 Add attachment Large File (9 Mb) by 'Save & Exit' button. - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Add attachment Large File (9 Mb).")
    public void KITE36_AddAttachment_LargeFile() throws Exception {
        System.out.println("[INFO]: ******* KITE35 Add attachment Large File (9 Mb). - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        at
                .uploadLargeFile("src/test/resources/largePdfFile.pdf");

                switchTo().alert().getText().equals("Alert text : Alert : File size cannot exceed 4500000 bytes." +
                " Selected file size: 9836770");
                switchTo().alert().accept();

        hp
                .navigateToHomePage()
                .findPatientBySearchField()
                .openFirstPatient();

        at
                .checkUploadedLargeFile();

        System.out.println("[INFO]: ******* KITE35 Add attachment Large File (9 Mb). - PASSED <<<<<<<");
    }
}
