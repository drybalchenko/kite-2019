package Tests;

import BaseTests.BaseTests;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class CheckHomePageTabs extends BaseTests{

    @BeforeAll
    public static void login() {
        open("https://full-kitekonnect.cs96.force.com");
        lp.loginToApplication(lp.TestLogin2, lp.TestPassword2);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Check Medical Information Message.")
    public void KITE02_CheckMedicalInfoMessage() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE02 Check Medical Information Message - START >>>>>>>");
        hp.navigateToHomePage();
        System.out.println("[INFO]: Open 'Medical Information' Tab.");
        $(hp.MedicalInformationTab).waitUntil(enabled, 20000).click();
        $(hp.MedicalInfoWindow).waitUntil(visible, 30000);

        System.out.println("[INFO]: Check 'Medical Information' Message.");
        $(hp.MedicalInfoMessage1).shouldHave(text("To request Medical Information, please contact us:"));
        $(hp.MedicalInfoMessage2).shouldHave(text("1-844-454-KITE  or  Visit Kite Pharma Support"));
        System.out.println("[INFO]: ******* KITE02 Check Medical Information Message - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Check Adverse Event Message.")
    public void KITE03_CheckAdverseEventMessage() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE03 Check Adverse Event Message - START >>>>>>>");
        hp.navigateToHomePage();
        System.out.println("[INFO]: Open 'Adverse Event' Tab.");
        $(hp.AdverseEventTab).waitUntil(enabled, 20000).click();
        $(hp.AdverseEventWindow).waitUntil(visible, 30000);

        System.out.println("[INFO]: Check 'Adverse Event' Message.");
        $(hp.AdverseEventMessage1).shouldHave(text("To report a suspected Adverse Event, please contact us:"));
        $(hp.AdverseEventMessage2).shouldHave(text("1-844-454-KITE"));
        System.out.println("[INFO]: ******* KITE03 Check Adverse Event Message - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Check Product Complaint Message.")
    public void KITE04_CheckProductComplaintMessage() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE04 Check Product Complaint Message - START >>>>>>>");
        hp.navigateToHomePage();
        System.out.println("[INFO]: Open 'Product Complaint' Tab.");
        $(hp.ProductComplaintTab).waitUntil(enabled, 20000).click();
        $(hp.ProductComplaintWindow).waitUntil(visible, 30000);

        System.out.println("[INFO]: Check 'Product Complaint' Message.");
        $(hp.ProductComplaintMessage1).shouldHave(text("To report a Product Complaint, please contact us:"));
        $(hp.ProductComplaintMessage2).shouldHave(text("1-844-454-KITE"));
        System.out.println("[INFO]: ******* KITE04 Check Product Complaint Message - PASSED <<<<<<<");
    }
}
