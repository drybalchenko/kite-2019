package Tests;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import BaseTests.BaseTests;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class CheckRequiredFieldsPatientForm extends BaseTests{

    @BeforeAll
    public static void login() {
        open("https://full-kitekonnect.cs96.force.com");
        lp.loginToApplication(lp.TestLogin2, lp.TestPassword2);
    }

    //--- Required Fields on Patient Form

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Save Empty Patient Form.")
    public void KITE05_EmptyPatientForm() throws Exception {
        System.out.println("[INFO]: ******* KITE05 Save Empty Patient Form - START >>>>>>>");
        //open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        hp
        .navigateToPatientForm();

        System.out.println("[INFO]: Submit empty Patient Form.");
        $(pf.SubmitButton).waitUntil(enabled, 15000).click();

        System.out.println("[INFO]: Check Error messages for required fields.");
        $(pf.ErrorForFirstNameField).shouldHave(text(pf.RequiredFieldErrorMessage));
        $(pf.ErrorForLastNameField).shouldHave(text(pf.RequiredFieldErrorMessage));
        $(pf.ErrorForDobYearField).shouldHave(text("Invalid year"));
        $(pf.ErrorForMonthField).shouldHave(text("Invalid month"));
        $(pf.ErrorForDayField).shouldHave(text("Invalid day"));
        $(pf.ErrorForGender).shouldHave(text(pf.RequiredFieldErrorMessage));
        $(pf.ErrorForThreatingPhisicianField).shouldHave(text(pf.RequiredFieldErrorMessage));
        $(pf.ErrorForAddOwnHp).shouldHave(text(pf.RequiredFieldErrorMessage));

        System.out.println("[INFO]: ******* KITE05 Save Empty Patient Form - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Save Patient Without First Name.")
    public void KITE06_NewPatientWithoutFirstName() throws Exception {
        System.out.println("[INFO]: ******* KITE06 Save Patient Without First Name - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        hp
        .navigateToPatientForm();

        System.out.println("[INFO]: Fill required fields in Patient Form (without Firs Name)");
        $(pf.LastNameField).setValue(pf.LastName);
        $(pf.DobYearField).setValue(pf.DobYear);

        mm
        .selectFromPicklistUniversal(pf.MonthField, "May")
        .selectFromPicklistUniversal(pf.DayField, "05");

        $(pf.MaleGenderButton).click();

        mm
        .selectFromSearchField(pf.ThreatingPhisicianField, pf.ThreatingFirstRecord, pf.ThreatingPhysician);

        $(pf.AddOwnHpNoButton).click();

        System.out.println("[INFO]: Submit Patient Form (without Firs Name)");
        $(pf.SubmitButton).waitUntil(enabled, 15000).click();

        System.out.println("[INFO]: Check Error Message for required field - 'First Name'");
        $(pf.ErrorForFirstNameField).shouldHave(text(pf.RequiredFieldErrorMessage));

        System.out.println("[INFO]: ******* KITE06 Save Patient Without First Name - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Save Patient Without Last Name.")
    public void KITE07_NewPatientWithoutLastName() throws Exception {
        System.out.println("[INFO]: ******* KITE07 Save Patient Without Last Name - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        hp
        .navigateToPatientForm();

        System.out.println("[INFO]: Fill required fields in Patient Form (without Last Name)");
        $(pf.FirstNameField).setValue(pf.FirstName);
        $(pf.DobYearField).setValue(pf.DobYear);
        mm
        .selectFromPicklistUniversal(pf.MonthField, "Jan")
        .selectFromPicklistUniversal(pf.DayField, "01");

        $(pf.MaleGenderButton).click();

        mm
        .selectFromSearchField(pf.ThreatingPhisicianField, pf.ThreatingFirstRecord, pf.ThreatingPhysician);

        $(pf.AddOwnHpNoButton).click();

        System.out.println("[INFO]: Submit Patient Form (without Last Name)");
        $(pf.SubmitButton).waitUntil(enabled, 15000).click();

        System.out.println("[INFO]: Check Error Message for required field - 'Last Name'");
        $(pf.ErrorForLastNameField).shouldHave(text(pf.RequiredFieldErrorMessage));

        System.out.println("[INFO]: ******* KITE07 Save Patient Without Last Name - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Save Patient Without Date Of Birthday (Year/Day.")
    public void KITE08_NewPatientWithoutYear() throws Exception {
        System.out.println("[INFO]: ******* KITE08 Save Patient Without Date Of Birthday (Year/Day) - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        hp
        .navigateToPatientForm();

        System.out.println("[INFO]: Fill required fields in Patient Form (without Year and Day)");
        $(pf.FirstNameField).setValue(pf.FirstName);
        $(pf.LastNameField).setValue(pf.LastName);

        mm
        .selectFromPicklistUniversal(pf.MonthField, "Jan");

        $(pf.MaleGenderButton).click();

        mm
        .selectFromSearchField(pf.ThreatingPhisicianField, pf.ThreatingFirstRecord, pf.ThreatingPhysician);

        $(pf.AddOwnHpNoButton).click();

        System.out.println("[INFO]: Submit Patient Form (without Year and Day)");
        $(pf.SubmitButton).waitUntil(enabled, 15000).click();

        System.out.println("[INFO]: Check Error Messages for required fields - 'Year' and 'Day'");
        $(pf.ErrorForDobYearField).shouldHave(text("Invalid year"));
        $(pf.ErrorForDayField).shouldHave(text("Invalid day"));

        System.out.println("[INFO]: ******* KITE08 Save Patient Without Date Of Birthday (Year/Day) - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Save Patient Without Date Of Birthday (Month/Day).")
    public void KITE09_NewPatientWithoutMonth() throws Exception {
        System.out.println("[INFO]: ******* KITE09 Save Patient Without Date Of Birthday (Month/Day) - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        hp
        .navigateToPatientForm();

        System.out.println("[INFO]: Fill required fields in Patient Form (without Month and Day)");
        $(pf.FirstNameField).setValue(pf.FirstName);
        $(pf.LastNameField).setValue(pf.LastName);
        $(pf.DobYearField).setValue(pf.DobYear);
        $(pf.MaleGenderButton).click();

        mm
        .selectFromSearchField(pf.ThreatingPhisicianField, pf.ThreatingFirstRecord, pf.ThreatingPhysician);

        $(pf.AddOwnHpNoButton).click();

        System.out.println("[INFO]: Submit Patient Form (without Month and Day)");
        $(pf.SubmitButton).waitUntil(enabled, 15000).click();

        System.out.println("[INFO]: Check Error Messages for required fields - 'Month' and 'Day'");
        $(pf.ErrorForMonthField).shouldHave(text("Invalid month"));
        $(pf.ErrorForDayField).shouldHave(text("Invalid day"));
        System.out.println("[INFO]: ******* KITE09 Save Patient Without Date Of Birthday (Month/Day) - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Save Patient Without Date Of Birthday (Day).")
    public void KITE10_NewPatientWithoutDay() throws Exception {
        System.out.println("[INFO]: ******* KITE10 Save Patient Without Date Of Birthday (Day) - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        hp
        .navigateToPatientForm();

        System.out.println("[INFO]: Fill required field in Patient Form (without Day)");
        $(pf.FirstNameField).setValue(pf.FirstName);
        $(pf.LastNameField).setValue(pf.LastName);
        $(pf.DobYearField).setValue(pf.DobYear);

        mm
        .selectFromPicklistUniversal(pf.MonthField, "Jan");

        $(pf.MaleGenderButton).click();

        mm
        .selectFromSearchField(pf.ThreatingPhisicianField, pf.ThreatingFirstRecord, pf.ThreatingPhysician);

        $(pf.AddOwnHpNoButton).click();

        System.out.println("[INFO]: Submit Patient Form (without Day)");
        $(pf.SubmitButton).waitUntil(enabled, 15000).click();

        System.out.println("[INFO]: Check Error Message for required field - 'Day'");
        $(pf.ErrorForDayField).shouldHave(text("Invalid day"));

        System.out.println("[INFO]: ******* KITE10 Save Patient Without Date Of Birthday (Day) - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Save Patient Without Gender.")
    public void KITE11_NewPatientWithoutGender() throws Exception {
        System.out.println("[INFO]: ******* KITE11 Save Patient Without Gender - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        hp
        .navigateToPatientForm();

        System.out.println("[INFO]: Fill required fields in Patient Form (without Gender)");
        $(pf.FirstNameField).setValue(pf.FirstName);
        $(pf.LastNameField).setValue(pf.LastName);
        $(pf.DobYearField).setValue(pf.DobYear);

        mm
        .selectFromPicklistUniversal(pf.MonthField, "Jan")
        .selectFromPicklistUniversal(pf.DayField, "01")
        .selectFromSearchField(pf.ThreatingPhisicianField, pf.ThreatingFirstRecord, pf.ThreatingPhysician);

        $(pf.AddOwnHpNoButton).click();

        System.out.println("[INFO]: Submit Patient Form (without Gender)");
        $(pf.SubmitButton).waitUntil(enabled, 15000).click();

        System.out.println("[INFO]: Check Error Message for required field - 'Gender'");
        $(pf.ErrorForGender).shouldHave(text(pf.RequiredFieldErrorMessage));

        System.out.println("[INFO]: ******* KITE11 Save Patient Without Gender - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Save Patient Without Treating Physician.")
    public void KITE12_NewPatientWithoutTreatingPhysician() throws Exception {
        System.out.println("[INFO]: ******* KITE12 Save Patient Without Treating Physician - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        hp
        .navigateToPatientForm();

        System.out.println("[INFO]: Fill required fields in Patient Form (without Threating Phisician)");
        $(pf.FirstNameField).setValue(pf.FirstName);
        $(pf.LastNameField).setValue(pf.LastName);
        $(pf.DobYearField).setValue(pf.DobYear);

        mm
        .selectFromPicklistUniversal(pf.MonthField, "Jan")
        .selectFromPicklistUniversal(pf.DayField, "01");

        pf.MaleGenderButton.click();
        pf.AddOwnHpNoButton.click();

        System.out.println("[INFO]: Submit Patient Form (without Threating Phisician)");
        $(pf.SubmitButton).waitUntil(enabled, 15000).click();

        System.out.println("[INFO]: Check Error Message for required field - 'Threating Phisician'");
        $(pf.ErrorForThreatingPhisicianField).shouldHave(text(pf.RequiredFieldErrorMessage));

        System.out.println("[INFO]: ******* KITE12 Save Patient Without Treating Physician - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Save Patient Without Hospital Patient ID.")
    public void KITE13_NewPatientWithoutHospitalPatientId() throws Exception {
        System.out.println("[INFO]: ******* KITE13 Save Patient Without Hospital Patient ID - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");

        hp
        .navigateToPatientForm();

        System.out.println("[INFO]: Fill required fields in Patient Form (without Patient ID)");
        $(pf.FirstNameField).setValue(pf.FirstName);
        $(pf.LastNameField).setValue(pf.LastName);
        $(pf.DobYearField).setValue(pf.DobYear);

        mm
        .selectFromPicklistUniversal(pf.MonthField, "Jan")
        .selectFromPicklistUniversal(pf.DayField, "01");

        $(pf.MaleGenderButton).click();

        mm
        .selectFromSearchField(pf.ThreatingPhisicianField, pf.ThreatingFirstRecord, pf.ThreatingPhysician);

        System.out.println("[INFO]: Submit Patient Form (without Patient ID)");
        $(pf.SubmitButton).waitUntil(enabled, 15000).click();

        System.out.println("[INFO]: Check Error Message for required field - 'Patient ID'");
        $(pf.ErrorForAddOwnHp).shouldHave(text(pf.RequiredFieldErrorMessage));

        System.out.println("[INFO]: ******* KITE13 Save Patient Without Hospital Patient ID - PASSED <<<<<<<");
    }

    //--- Required Fields on Patient Contact Information Form

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Save Contact Information without required 'Phone'.")
    public void KITE14_ContactInfoWithoutRequiredPhone() throws Exception {
        System.out.println("[INFO]: ******* KITE14 Save Contact Information without required 'Phone' - START >>>>>>>");

        pcit
        .navigateToContactInformation()
        .ContactMethodPhone.click();

        System.out.println("[INFO]: Save form without required field - 'Phone'");
        $(pcit.SaveAndNextContactInfo).waitUntil(enabled, 15000).click();

        System.out.println("[INFO]: Check Error message for required field - 'Phone'");
        $(pcit.ContactInfomationErrorMessage).shouldHave(text(pcit.ErrorMessagePhone));

        System.out.println("[INFO]: ******* KITE14 Save Contact Information without required 'Phone' - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Save Contact Information without required 'Email'.")
    public void KITE15_ContactInfoWithoutRequiredEmail() throws Exception {
        System.out.println("[INFO]: ******* KITE15 Save Contact Information without required 'Email' - START >>>>>>>");

        pcit
        .navigateToContactInformation()
        .ContactMethodEmail.click();

        System.out.println("[INFO]: Save form without required field - 'Email'");
        $(pcit.SaveAndNextContactInfo).waitUntil(enabled, 15000).click();

        System.out.println("[INFO]: Check Error message for required field - 'Email'");
        $(pcit.ContactInfomationErrorMessage).shouldHave(text(pcit.ErrorMessageEmail));

        System.out.println("[INFO]: ******* KITE15 Save Contact Information without required 'Email' - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    @DisplayName("Save Contact Information with invalid 'Email'.")
    public void KITE16_ContactInfoInvalidEmail() throws Exception {
        System.out.println("[INFO]: ******* KITE16 Save Contact Information with invalid 'Email' - START >>>>>>>");

        pcit
        .navigateToContactInformation()
        .ContactMethodEmail.click();

        System.out.println("[INFO]: Fill 'Email' field with an invalid value.");
        $(pcit.EmailField).setValue("emailText");
        $(pcit.SaveAndNextContactInfo).waitUntil(enabled, 15000).click();

        System.out.println("[INFO]: Check Error message for Invalid Email");
        $(pcit.EmailErrorMessage).shouldHave(text(pcit.ErrorMessageInvalidEmail));

        System.out.println("[INFO]: ******* KITE16 Save Contact Information with invalid 'Email' - PASSED <<<<<<<");
    }
}
