package Tests;

import BaseTests.BaseTests;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class CheckAssistanceRequests extends BaseTests {

    @BeforeAll
    public static void login() {
        open("https://full-kitekonnect.cs96.force.com");
        lp.loginToApplication(lp.TestLogin2, lp.TestPassword2);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Create new Patient with Appeals Request support request.")
    public void KITE71_PatientWithAppealsRequest() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE71 Create new Patient with Appeals Request support request - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        //mm.navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddButton);
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.AppealsRequestAssistance);
        at
                .newUploadFile("src/test/resources/testPdfFile.pdf");

        mip
                .closeUploadInfoWindow(mip.AppealsRequestUploadedWindow, mip.UploadedWindowCloseButton);

        System.out.println("[INFO]: Save Patient Assistance selection.");
        $(mip.SaveAndNextButton).click();

        hp
                .navigateToHomePage()
                .findPatientBySearchField()
                .openPatientInformation()
                .checkRequestInformation("Appeals Request");
        /*
        System.out.println("[INFO]: Navigate to 'Salesforce'");
        open("https://kitekonnect--full.lightning.force.com/?un=kite@silverlinecrm.com.full&pw=$ummerTwo108");

        Thread.sleep(3000);
        try {
            if (spo.ContinueButton.exists()) {
                $(spo.ContinueButton).shouldBe(Condition.enabled).click(); }
        }
        catch(Exception e){
        }

        spo
                .openPatient2()
                .checkSupportRequest("Appeals Request", 3 );
        */
        System.out.println("[INFO]: ******* KITE71 Create new Patient with Appeals Request support request - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Create new Patient with Benefit Investigation support request.")
    public void KITE20_PatientWithBenefitInvestigation() throws Exception {
        System.out.println("[INFO]: ******* KITE20 Create new Patient with Benefit Investigation support request - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
        .navigateToPatientForm();

        pf
        .fillFieldsInPatienForm()
        .savePatientForm()
        .checkDataOnTotalPage()
        .submitPatientForm();

        pnt
        .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
        .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);   //AddButton

        ptt
        .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
        .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
        .fillRequiredMedicalInfo()
        .selectPatientAssistance(mip.BenefitInvestigationAssistance);

        System.out.println("[INFO]: Save Patient Assistance selection.");
        $(mip.SaveAndNextButton).click();

        hp
        .navigateToHomePage()
        .findPatientBySearchField()
        .openPatientInformation()
        .checkRequestInformation("Benefit Investigation");
/*
        System.out.println("[INFO]: Navigate to 'Salesforce' !!!!!!!!!!");
        open("https://kitekonnect--full.lightning.force.com/?un=kite@silverlinecrm.com.full&pw=$ummerTwo108");

        Thread.sleep(3000);
        try {
            if (spo.ContinueButton.exists()) {
                $(spo.ContinueButton).shouldBe(enabled).click(); }
        }
        catch(Exception e){
        }

        spo
        .openPatient2()
        .checkSupportRequest("Benefit Investigation", 3);
*/
        System.out.println("[INFO]: ******* KITE20 Create new Patient with Benefit Investigation support request - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Create new Patient with Prior Authorization support request.")
    public void KITE21_PatientWithPriorAuthorization() throws Exception {
        System.out.println("[INFO]: ******* KITE21 Create new Patient with Prior Authorization support request - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                //.checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);   //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.PriorAuthorizationAssistance);

        System.out.println("[INFO]: Save Patient Assistance selection.");
        $(mip.SaveAndNextButton).click();

        hp
        .navigateToHomePage()
        .findPatientBySearchField()
        .openPatientInformation()
        .checkRequestInformation("Prior Authorization");
/*
        System.out.println("[INFO]: Navigate to 'Salesforce' !!!!!!!!!!");
        open("https://kitekonnect--full.lightning.force.com/?un=kite@silverlinecrm.com.full&pw=$ummerTwo108");

        Thread.sleep(3000);
        try {
            if (spo.ContinueButton.exists()) {
                $(spo.ContinueButton).shouldBe(enabled).click(); }
        }
        catch(Exception e){
        }

        spo
                .openPatient2()
                .checkSupportRequest("Prior Authorization", 3);
*/
        System.out.println("[INFO]: ******* KITE21 Create new Patient with Prior Authorization support request - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Create new Patient with General Inquiry support request.")
    public void KITE22_PatientWithGeneralInquiry() throws Exception {
        System.out.println("[INFO]: ******* KITE22 Create new Patient with General Inquiry support request - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                //.checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);   //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.GeneralInquiryAssistance);

        System.out.println("[INFO]: Save Patient Assistance selection.");
        $(mip.SaveAndNextButton).click();

        hp
                .navigateToHomePage()
                .findPatientBySearchField()
                .openPatientInformation()
                .checkRequestInformation("General Inquiry");
/*
        System.out.println("[INFO]: Navigate to 'Salesforce' !!!!!!!!!!");
        open("https://kitekonnect--full.lightning.force.com/?un=kite@silverlinecrm.com.full&pw=$ummerTwo108");

        Thread.sleep(3000);
        try {
            if (spo.ContinueButton.exists()) {
                $(spo.ContinueButton).shouldBe(enabled).click(); }
        }
        catch(Exception e){
        }

        spo
                .openPatient2()
                .checkSupportRequest("General Inquiry", 3);
*/
        System.out.println("[INFO]: ******* KITE22 Create new Patient with General Inquiry support request - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Create new Patient with Travel And Lodging support request.")
    public void KITE23_PatientWithTravelAndLodging() throws Exception {
        System.out.println("[INFO]: ******* KITE23 Create new Patient with Travel And Lodging support request - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                //.checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton); //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.TravelAndLodgingAssistance);

        System.out.println("[INFO]: Save Patient Assistance selection.");
        $(mip.SaveAndNextButton).click();

        hp
                .navigateToHomePage()
                .findPatientBySearchField()
                .openPatientInformation()
                .checkRequestInformation("Travel and Lodging");
/*
        System.out.println("[INFO]: Navigate to 'Salesforce' !!!!!!!!!!");
        open("https://kitekonnect--full.lightning.force.com/?un=kite@silverlinecrm.com.full&pw=$ummerTwo108");

        Thread.sleep(3000);
        try {
            if (spo.ContinueButton.exists()) {
                $(spo.ContinueButton).shouldBe(enabled).click(); }
        }
        catch(Exception e){
        }

        spo
                .openPatient2()
                .checkSupportRequest("Patient Travel Assistance", 4);
*/
        System.out.println("[INFO]: ******* KITE23 Create new Patient with Travel And Lodging support request - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Create new Patient with Underinsured Patient support request.")
    public void KITE24_PatientWithUnderinsuredPatient() throws Exception {
        System.out.println("[INFO]: ******* KITE24 Create new Patient with Underinsured Patient support request - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                //.checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton); //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.UnderinsuredPatientAssistance);

        System.out.println("[INFO]: Save Patient Assistance selection.");
        $(mip.SaveAndNextButton).click();

        hp
                .navigateToHomePage()
                .findPatientBySearchField()
                .openPatientInformation()
                .checkRequestInformation("Underinsured Patient");
/*
        System.out.println("[INFO]: Navigate to 'Salesforce' !!!!!!!!!!");
        open("https://kitekonnect--full.lightning.force.com/?un=kite@silverlinecrm.com.full&pw=$ummerTwo108");

        Thread.sleep(3000);
        try {
            if (spo.ContinueButton.exists()) {
                $(spo.ContinueButton).shouldBe(enabled).click(); }
        }
        catch(Exception e){
        }

        spo
                .openPatient2()
                .checkSupportRequest("Underinsured Patient", 3);
*/
        System.out.println("[INFO]: ******* KITE24 Create new Patient with Underinsured Patient support request - PASSED <<<<<<<");
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Create new Patient with Uninsured Patient support request.")
    public void KITE25_PatientWithUninsuredPatient() throws Exception {
        System.out.println("[INFO]: ******* KITE25 Create new Patient with Uninsured Patient support request - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                //.checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);   //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.UninsuredPatientAssistance);

        System.out.println("[INFO]: Save Patient Assistance selection.");
        $(mip.SaveAndNextButton).click();

        hp
                .navigateToHomePage()
                .findPatientBySearchField()
                .openPatientInformation()
                .checkRequestInformation("Uninsured Patient");

        //spo.checkRequestInSalesforce("Uninsured Patient", 3);
        /*
                System.out.println("[INFO]: Navigate to 'Salesforce' !!!!!!!!!!");
        open("https://kitekonnect--full.lightning.force.com/?un=kite@silverlinecrm.com.full&pw=$ummerTwo108");

        Thread.sleep(3000);
        try {
            if (spo.ContinueButton.exists()) {
                $(spo.ContinueButton).shouldBe(enabled).click(); }
        }
        catch(Exception e){
        }

        spo
                .openPatient2()
                .checkSupportRequest("Uninsured Patient", 3);
*/
        System.out.println("[INFO]: ******* KITE25 Create new Patient with Uninsured Patient support request - PASSED <<<<<<<");
    }

    //------------------------------------------------------------------------------------------------------------------

    //@Test
    public void KITE56_BenefitInvestigationSaveAndExit() throws Exception {
        System.out.println("[INFO]: ******* KITE56 Benefit Investigation support request by 'Save&Exit' button. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);   //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.BenefitInvestigationAssistance);

        System.out.println("[INFO]: Save Patient Assistance selection.");
        $(mip.SaveAndExitButton).click();

        hp
                .findPatientBySearchField()
                .openPatientInformation()
                .checkRequestInformation("Benefit Investigation");

        System.out.println("[INFO]: ******* KITE56 Benefit Investigation support request by 'Save&Exit' button. - PASSED <<<<<<<");
    }

    //@Test
    public void KITE57_PriorAuthorizationSaveAndExit() throws Exception {
        System.out.println("[INFO]: ******* KITE57 Prior Authorization support request by 'Save&Exit' button.  - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);   //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.PriorAuthorizationAssistance);

        System.out.println("[INFO]: Save Patient Assistance selection.");
        $(mip.SaveAndExitButton).click();

        hp
                .findPatientBySearchField()
                .openPatientInformation()
                .checkRequestInformation("Prior Authorization");

        System.out.println("[INFO]: ******* KITE57 Prior Authorization support request by 'Save&Exit' button.  - PASSED <<<<<<<");
    }

    //@Test
    public void KITE58_GeneralInquirySaveAndExit() throws Exception {
        System.out.println("[INFO]: ******* KITE58 General Inquiry support request by 'Save&Exit' button.  - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);   //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.GeneralInquiryAssistance);

        System.out.println("[INFO]: Save Patient Assistance selection.");
        $(mip.SaveAndExitButton).click();

        hp
                .findPatientBySearchField()
                .openPatientInformation()
                .checkRequestInformation("General Inquiry");

        System.out.println("[INFO]: ******* KITE58 General Inquiry support request by 'Save&Exit' button.  - PASSED <<<<<<<");
    }

    //@Test
    public void KITE59_TravelAndLodgingSaveAndExit() throws Exception {
        System.out.println("[INFO]: ******* KITE59 Travel And Lodging support request by 'Save&Exit' button.  - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton); //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.TravelAndLodgingAssistance);

        System.out.println("[INFO]: Save Patient Assistance selection.");
        $(mip.SaveAndExitButton).click();

        hp
                .findPatientBySearchField()
                .openPatientInformation()
                .checkRequestInformation("Travel and Lodging");

        System.out.println("[INFO]: ******* KITE59 Travel And Lodging support request by 'Save&Exit' button.  - PASSED <<<<<<<");
    }

    //@Test
    public void KITE60_UnderinsuredPatientSaveAndExit() throws Exception {
        System.out.println("[INFO]: ******* KITE60 Underinsured Patient support request by 'Save&Exit' button.  - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton); //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.UnderinsuredPatientAssistance);

        System.out.println("[INFO]: Save Patient Assistance selection.");
        $(mip.SaveAndExitButton).click();

        hp
                .findPatientBySearchField()
                .openPatientInformation()
                .checkRequestInformation("Underinsured Patient");

        System.out.println("[INFO]: ******* KITE60 Underinsured Patient support request by 'Save&Exit' button.  - PASSED <<<<<<<");
    }

    //@Test
    public void KITE61_UninsuredPatientSaveAndExit() throws Exception {
        System.out.println("[INFO]: ******* KITE61 Uninsured Patient support request by 'Save&Exit' button.  - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);   //AddButton

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.UninsuredPatientAssistance);

        System.out.println("[INFO]: Save Patient Assistance selection.");
        $(mip.SaveAndExitButton).click();

        hp
                .findPatientBySearchField()
                .openPatientInformation()
                .checkRequestInformation("Uninsured Patient");

        System.out.println("[INFO]: ******* KITE61 Uninsured Patient support request by 'Save&Exit' button.  - PASSED <<<<<<<");
    }

   //@Test
    public void KITE72_AppealsRequestSaveAndExit() throws InterruptedException {
        System.out.println("[INFO]: ******* KITE72 Appeals Request support request by 'Save&Exit' button. - START >>>>>>>");
        open("https://full-kitekonnect.cs96.force.com/s/portalhome");
        cp
                .navigateToContactsPage()
                .removeContacts();
        hp
                .navigateToPatientForm();

        pf
                .fillFieldsInPatienForm()
                .savePatientForm()
                .checkDataOnTotalPage()
                .submitPatientForm();

        pnt
                .goToContactInformationTab();

        System.out.println("[INFO]: Navigate to Patient Team Tab.");
        //mm.navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddButton);
        mm
                .navigateToNextPage(pcit.SaveAndNextContactInfo, ptt.AddPatientTeamMemberButton);

        ptt
                .addNewTeamMember();

        System.out.println("[INFO]: Navigate to Medical Info Page.");
        mm
                .navigateToNextPage(ptt.ContinueButton, mip.FirstDiagnosis);

        mip
                .fillRequiredMedicalInfo()
                .selectPatientAssistance(mip.AppealsRequestAssistance);
        at
                .newUploadFile("src/test/resources/testPdfFile.pdf");

        mip
                .closeUploadInfoWindow(mip.AppealsRequestUploadedWindow, mip.UploadedWindowCloseButton);

        System.out.println("[INFO]: Save Patient Assistance selection.");
        $(mip.SaveAndExitButton).click();

        hp
                .findPatientBySearchField()
                .openPatientInformation()
                .checkRequestInformation("Appeals Request");
/*
        System.out.println("[INFO]: Navigate to 'Salesforce'");
        open("https://kitekonnect--full.lightning.force.com/?un=kite@silverlinecrm.com.full&pw=$ummerTwo108");

        Thread.sleep(3000);
        try {
            if (spo.ContinueButton.exists()) {
                $(spo.ContinueButton).shouldBe(enabled).click(); }
        }
        catch(Exception e){
        }

        spo
                .openPatient2()
                .checkSupportRequest("Appeals Request", 3 );
*/
        System.out.println("[INFO]: ******* KITE72 Appeals Request support request by 'Save&Exit' button. - PASSED <<<<<<<");
    }
}
